<?php

namespace Tests\Unit\src\recipebook\src\Domain\Model\Ingredient;


use Illuminate\Foundation\Testing\WithFaker;
use RecipeBook\Domain\Common\ValueObject\Uuid;
use RecipeBook\Domain\Model\Ingredient\Ingredient;
use RecipeBook\Domain\Model\Unit\Unit;
use Tests\TestCase;

class IngredientTest extends TestCase
{
    use WithFaker;

    public function testOK()
    {
        $ingredient = Ingredient::create(
            Uuid::random(),
            $this->faker->name,
            $this->faker->text,
            Unit::create(
                Uuid::random(),
                $this->faker->name,
                $this->faker->text
            )
        );

        $this->assertInstanceOf(Uuid::class, $ingredient->id());
        $this->assertInstanceOf(Unit::class, $ingredient->defaultUnit());
        $this->assertIsArray($ingredient->toArray());
        $this->assertIsArray($ingredient->toArrayMinified());
    }
}
