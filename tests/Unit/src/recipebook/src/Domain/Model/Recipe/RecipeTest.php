<?php

namespace Tests\Unit\src\recipebook\src\Domain\Model\Recipe;


use Doctrine\Common\Collections\ArrayCollection;
use Illuminate\Foundation\Testing\WithFaker;
use RecipeBook\Domain\Common\ValueObject\Uuid;
use RecipeBook\Domain\Model\Category\Category;
use RecipeBook\Domain\Model\Recipe\Recipe;
use RecipeBook\Domain\Model\Thermomix\ThermomixModel\ThermomixModel;
use RecipeBook\Domain\Model\User\User;
use Tests\TestCase;

class RecipeTest extends TestCase
{
    use WithFaker;

    public function testOk()
    {
        $user = User::create(
            Uuid::random(),
            $this->faker->name,
            $this->faker->email,
            $this->faker->password,
            true
        );
        $category = Category::create(Uuid::random(),
            $this->faker->name
        );
        $thermomixModel = ThermomixModel::create(Uuid::random(),
            $this->faker->name,
            $this->faker->text
        );
        $recipe = Recipe::create(
            Uuid::random(),
            $user,
            $this->faker->name,
            $this->faker->text,
            10,
            20,
            2,
            true,
            new ArrayCollection([$category]),
            new ArrayCollection([$thermomixModel])
        );

        $this->assertSame($user, $recipe->user());
        $this->assertIsArray($recipe->user()->toArray());

        $category2 = Category::create(Uuid::random(), $this->faker->name);
        $recipe->addCategory($category2);
        $this->assertIsArray($recipe->categories()->toArray());

        $thermomixModel2 = ThermomixModel::create(Uuid::random(), $this->faker->name, $this->faker->text);
        $recipe->addThermomixModel($thermomixModel2);

        $this->assertIsArray($recipe->toArray());
        $this->assertIsArray($recipe->toArrayMinified());

        $recipe->removeCategory($category);
        $recipe->removeAllCategories();

        $recipe->removeThermomixModel($thermomixModel);
        $recipe->removeAllThermomixModels();

        $this->assertEmpty($recipe->categories());
        $this->assertEmpty($recipe->thermomixModels());

        $user2 = User::create(
            Uuid::random(),
            $this->faker->name,
            $this->faker->email,
            $this->faker->password,
            true
        );
        $recipe->batchUpdate([
            'user' => $user2,
            'name' => $this->faker->name,
            'description' => $this->faker->text,
            'preparationTime' => 20,
            'cookingTime' => 30,
            'serves' => 4,
            'isThermomix' => true
        ]);

        $this->assertSame($recipe->user(), $user2);
    }
}
