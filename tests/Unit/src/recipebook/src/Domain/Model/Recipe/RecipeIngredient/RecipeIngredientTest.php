<?php


namespace Tests\Unit\src\recipebook\src\Domain\Model\Recipe\RecipeIngredient;


use Doctrine\Common\Collections\ArrayCollection;
use Illuminate\Foundation\Testing\WithFaker;
use RecipeBook\Domain\Common\ValueObject\Uuid;
use RecipeBook\Domain\Model\Category\Category;
use RecipeBook\Domain\Model\Ingredient\Ingredient;
use RecipeBook\Domain\Model\Recipe\Recipe;
use RecipeBook\Domain\Model\Recipe\RecipeIngredient\RecipeIngredient;
use RecipeBook\Domain\Model\Thermomix\ThermomixModel\ThermomixModel;
use RecipeBook\Domain\Model\Unit\Unit;
use RecipeBook\Domain\Model\User\User;
use Tests\TestCase;

class RecipeIngredientTest extends TestCase
{
    use WithFaker;

    public function testOk()
    {
        $user = User::create(
            Uuid::random(),
            $this->faker->name,
            $this->faker->email,
            $this->faker->password,
            true
        );
        $category = Category::create(Uuid::random(),
            $this->faker->name
        );
        $thermomixModel = ThermomixModel::create(Uuid::random(),
            $this->faker->name,
            $this->faker->text
        );
        $recipe = Recipe::create(
            Uuid::random(),
            $user,
            $this->faker->name,
            $this->faker->text,
            10,
            20,
            2,
            true,
            new ArrayCollection([$category]),
            new ArrayCollection([$thermomixModel])
        );
        $unit = Unit::create(
            Uuid::random(),
            $this->faker->name,
            $this->faker->text
        );
        $ingredient = Ingredient::create(
            Uuid::random(),
            $this->faker->name,
            $this->faker->text,
            $unit
        );
        $recipeIngredient = RecipeIngredient::create(
            Uuid::random(),
            $recipe,
            $ingredient,
            50,
            $unit
        );

        $this->assertSame($recipe, $recipeIngredient->recipe());
        $this->assertSame($ingredient, $recipeIngredient->ingredient());
        $this->assertSame($unit, $recipeIngredient->unit());
        $this->assertIsArray($recipeIngredient->toArray());
        $this->assertIsArray($recipeIngredient->toArrayMinified());

        $recipeIngredient->batchUpdate([
            'recipe' => $recipe,
            'ingredient' => $ingredient,
            'value' => 100,
            'unit' => $unit
        ]);

        $this->assertSame('100', $recipeIngredient->value());
    }
}
