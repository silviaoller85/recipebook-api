<?php

namespace Tests\Unit\src\recipebook\src\Domain\Model\Category;


use Illuminate\Foundation\Testing\WithFaker;
use RecipeBook\Domain\Common\ValueObject\Uuid;
use RecipeBook\Domain\Model\Category\Category;
use Tests\TestCase;

class CategoryTest extends TestCase
{
    use WithFaker;

    public function testOk()
    {
        $category = Category::create(Uuid::random(), $this->faker->name);

        $this->assertInstanceOf(Uuid::class, $category->id());
        $this->assertIsArray($category->toArray());
        $this->assertIsArray($category->toArrayMinified());
    }
}
