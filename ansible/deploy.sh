#!/bin/sh

set -e

date=`date +%Y%m%d%H%M%S`

while getopts "hpme:v:r:f:" OPTION
do
  case $OPTION in
    e)
      environment=$OPTARG
      ;;
    r)
      rollback=$OPTARG
      ;;
    ?)
      usage | more
      exit
      ;;
  esac
done

if [ -z "${environment}" ]; then
  usage | more
  exit 1
fi

if [ ! -z "${rollback}" ]; then
  ANSIBLE_HOST_KEY_CHECKING=false ansible-playbook -i ansible/hosts ansible/rollback.yml --extra-vars="version=${rollback} env=${environment}"
  exit 0
fi

version=$date

tar --exclude='*.DS_Store' --exclude='*Thumbs.db' --exclude='public/storage' -zcf ./ansible/files/app.tgz app bootstrap config database public resources routes tests .env.example artisan composer.json composer.phar phpunit.xml src composer.lock

echo "Executing ansible ..."

ANSIBLE_HOST_KEY_CHECKING=false ansible-playbook -i ansible/hosts ansible/deploy.yml --extra-vars="version=${version} env=${environment}"
