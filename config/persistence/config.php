<?php

use LaravelDoctrine\ORM\Facades\EntityManager;
use RecipeBook\Domain\Model\Category\Category;
use RecipeBook\Domain\Model\Category\CategoryRepositoryGatewayInterface;
use RecipeBook\Domain\Model\Ingredient\Ingredient;
use RecipeBook\Domain\Model\Ingredient\IngredientRepositoryGatewayInterface;
use RecipeBook\Domain\Model\Recipe\Recipe;
use RecipeBook\Domain\Model\Recipe\RecipeIngredient\RecipeIngredient;
use RecipeBook\Domain\Model\Recipe\RecipeIngredient\RecipeIngredientRepositoryGatewayInterface;
use RecipeBook\Domain\Model\Recipe\RecipePhoto\RecipePhoto;
use RecipeBook\Domain\Model\Recipe\RecipePhoto\RecipePhotoRepositoryGatewayInterface;
use RecipeBook\Domain\Model\Recipe\RecipeRepositoryGatewayInterface;
use RecipeBook\Domain\Model\Recipe\RecipeStep\RecipeStep;
use RecipeBook\Domain\Model\Recipe\RecipeStep\RecipeStepRepositoryGatewayInterface;
use RecipeBook\Domain\Model\Recipe\RecipeStep\RecipeStepThermomixAction;
use RecipeBook\Domain\Model\Recipe\RecipeStep\RecipeStepThermomixActionRepositoryGatewayInterface;
use RecipeBook\Domain\Model\Thermomix\ThermomixAction\ThermomixAction;
use RecipeBook\Domain\Model\Thermomix\ThermomixAction\ThermomixActionRepositoryGatewayInterface;
use RecipeBook\Domain\Model\Thermomix\ThermomixModel\ThermomixModel;
use RecipeBook\Domain\Model\Thermomix\ThermomixModel\ThermomixModelRepositoryGatewayInterface;
use RecipeBook\Domain\Model\Unit\Unit;
use RecipeBook\Domain\Model\Unit\UnitRepositoryGatewayInterface;
use RecipeBook\Domain\Model\User\User;
use RecipeBook\Domain\Model\User\UserFavorite;
use RecipeBook\Domain\Model\User\UserFavoriteRepositoryGatewayInterface;
use RecipeBook\Domain\Model\User\UserRepositoryGatewayInterface;
use RecipeBook\Infrastructure\InMemory\Repository\InMemoryCategoryRepositoryGateway;
use RecipeBook\Infrastructure\InMemory\Repository\InMemoryIngredientRepositoryGateway;
use RecipeBook\Infrastructure\InMemory\Repository\InMemoryRecipeIngredientRepositoryGateway;
use RecipeBook\Infrastructure\InMemory\Repository\InMemoryRecipePhotoRepositoryGateway;
use RecipeBook\Infrastructure\InMemory\Repository\InMemoryRecipeRepositoryGateway;
use RecipeBook\Infrastructure\InMemory\Repository\InMemoryRecipeStepRepositoryGateway;
use RecipeBook\Infrastructure\InMemory\Repository\InMemoryRecipeStepThermomixActionRepositoryGateway;
use RecipeBook\Infrastructure\InMemory\Repository\InMemoryThermomixActionRepositoryGateway;
use RecipeBook\Infrastructure\InMemory\Repository\InMemoryThermomixModelRepositoryGateway;
use RecipeBook\Infrastructure\InMemory\Repository\InMemoryUnitRepositoryGateway;
use RecipeBook\Infrastructure\InMemory\Repository\InMemoryUserFavoriteRepositoryGateway;
use RecipeBook\Infrastructure\InMemory\Repository\InMemoryUserRepositoryGateway;

return [
    'driver' => env('GATEWAY_DRIVER'),

    'repository-gateways' => [
        CategoryRepositoryGatewayInterface::class => [
            'doctrine'  => function () {
                return EntityManager::getRepository(Category::class);
            },
            'in_memory' => function () {
                return new InMemoryCategoryRepositoryGateway;
            },
        ],

        UserRepositoryGatewayInterface::class => [
            'doctrine'  => function () {
                return EntityManager::getRepository(User::class);
            },
            'in_memory' => function () {
                return new InMemoryUserRepositoryGateway;
            },
        ],

        UserFavoriteRepositoryGatewayInterface::class => [
            'doctrine'  => function () {
                return EntityManager::getRepository(UserFavorite::class);
            },
            'in_memory' => function () {
                return new InMemoryUserFavoriteRepositoryGateway;
            },
        ],

        UnitRepositoryGatewayInterface::class => [
            'doctrine'  => function () {
                return EntityManager::getRepository(Unit::class);
            },
            'in_memory' => function () {
                return new InMemoryUnitRepositoryGateway;
            },
        ],

        IngredientRepositoryGatewayInterface::class => [
            'doctrine'  => function () {
                return EntityManager::getRepository(Ingredient::class);
            },
            'in_memory' => function () {
                return new InMemoryIngredientRepositoryGateway;
            },
        ],

        ThermomixActionRepositoryGatewayInterface::class => [
            'doctrine'  => function () {
                return EntityManager::getRepository(ThermomixAction::class);
            },
            'in_memory' => function () {
                return new InMemoryThermomixActionRepositoryGateway;
            },
        ],

        ThermomixModelRepositoryGatewayInterface::class => [
            'doctrine'  => function () {
                return EntityManager::getRepository(ThermomixModel::class);
            },
            'in_memory' => function () {
                return new InMemoryThermomixModelRepositoryGateway;
            },
        ],

        RecipeRepositoryGatewayInterface::class => [
            'doctrine'  => function () {
                return EntityManager::getRepository(Recipe::class);
            },
            'in_memory' => function () {
                return new InMemoryRecipeRepositoryGateway;
            },
        ],

        RecipeIngredientRepositoryGatewayInterface::class => [
            'doctrine'  => function () {
                return EntityManager::getRepository(RecipeIngredient::class);
            },
            'in_memory' => function () {
                return new InMemoryRecipeIngredientRepositoryGateway;
            },
        ],

        RecipeStepRepositoryGatewayInterface::class => [
            'doctrine'  => function () {
                return EntityManager::getRepository(RecipeStep::class);
            },
            'in_memory' => function () {
                return new InMemoryRecipeStepRepositoryGateway;
            },
        ],

        RecipeStepThermomixActionRepositoryGatewayInterface::class => [
            'doctrine'  => function () {
                return EntityManager::getRepository(RecipeStepThermomixAction::class);
            },
            'in_memory' => function () {
                return new InMemoryRecipeStepThermomixActionRepositoryGateway;
            },
        ],

        RecipePhotoRepositoryGatewayInterface::class => [
            'doctrine'  => function () {
                return EntityManager::getRepository(RecipePhoto::class);
            },
            'in_memory' => function () {
                return new InMemoryRecipePhotoRepositoryGateway;
            },
        ],
    ]
];
