<?php

use Joselfonseca\LaravelTactician\Locator\LaravelLocator;
use League\Tactician\Handler\CommandNameExtractor\ClassNameExtractor;
use League\Tactician\Handler\MethodNameInflector\HandleInflector;
use RecipeBook\Application\Common\Bus\CommandBus;
use RecipeBook\Application\Service\Api\Category\Count\CountCategoriesCommand;
use RecipeBook\Application\Service\Api\Category\Count\CountCategoriesCommandHandler;
use RecipeBook\Application\Service\Api\Category\Create\CreateCategoryCommand;
use RecipeBook\Application\Service\Api\Category\Create\CreateCategoryCommandHandler;
use RecipeBook\Application\Service\Api\Category\Find\FindCategoriesCommand;
use RecipeBook\Application\Service\Api\Category\Find\FindCategoriesCommandHandler;
use RecipeBook\Application\Service\Api\Category\Get\GetCategoryCommand;
use RecipeBook\Application\Service\Api\Category\Get\GetCategoryCommandHandler;
use RecipeBook\Application\Service\Api\Ingredient\Count\CountIngredientsCommand;
use RecipeBook\Application\Service\Api\Ingredient\Count\CountIngredientsCommandHandler;
use RecipeBook\Application\Service\Api\Ingredient\Create\CreateIngredientCommand;
use RecipeBook\Application\Service\Api\Ingredient\Create\CreateIngredientCommandHandler;
use RecipeBook\Application\Service\Api\Ingredient\Find\FindIngredientsCommand;
use RecipeBook\Application\Service\Api\Ingredient\Find\FindIngredientsCommandHandler;
use RecipeBook\Application\Service\Api\Ingredient\Get\GetIngredientCommand;
use RecipeBook\Application\Service\Api\Ingredient\Get\GetIngredientCommandHandler;
use RecipeBook\Application\Service\Api\Recipe\Count\CountRecipesCommand;
use RecipeBook\Application\Service\Api\Recipe\Count\CountRecipesCommandHandler;
use RecipeBook\Application\Service\Api\Recipe\Create\CreateRecipeCommand;
use RecipeBook\Application\Service\Api\Recipe\Create\CreateRecipeCommandHandler;
use RecipeBook\Application\Service\Api\Recipe\Create\RecipeIngredient\CreateRecipeIngredientCommand;
use RecipeBook\Application\Service\Api\Recipe\Create\RecipeIngredient\CreateRecipeIngredientCommandHandler;
use RecipeBook\Application\Service\Api\Recipe\Create\RecipePhoto\CreateRecipePhotoCommand;
use RecipeBook\Application\Service\Api\Recipe\Create\RecipePhoto\CreateRecipePhotoCommandHandler;
use RecipeBook\Application\Service\Api\Recipe\Create\RecipeStep\CreateRecipeStepCommand;
use RecipeBook\Application\Service\Api\Recipe\Create\RecipeStep\CreateRecipeStepCommandHandler;
use RecipeBook\Application\Service\Api\Recipe\Delete\DeleteRecipeCommand;
use RecipeBook\Application\Service\Api\Recipe\Delete\DeleteRecipeCommandHandler;
use RecipeBook\Application\Service\Api\Recipe\Delete\RecipeIngredient\DeleteRecipeIngredientCommand;
use RecipeBook\Application\Service\Api\Recipe\Delete\RecipeIngredient\DeleteRecipeIngredientCommandHandler;
use RecipeBook\Application\Service\Api\Recipe\Delete\RecipePhoto\DeleteRecipePhotoCommand;
use RecipeBook\Application\Service\Api\Recipe\Delete\RecipePhoto\DeleteRecipePhotoCommandHandler;
use RecipeBook\Application\Service\Api\Recipe\Delete\RecipeStep\DeleteRecipeStepCommand;
use RecipeBook\Application\Service\Api\Recipe\Delete\RecipeStep\DeleteRecipeStepCommandHandler;
use RecipeBook\Application\Service\Api\Recipe\Find\FindRecipesCommand;
use RecipeBook\Application\Service\Api\Recipe\Find\FindRecipesCommandHandler;
use RecipeBook\Application\Service\Api\Recipe\Get\GetRecipeCommand;
use RecipeBook\Application\Service\Api\Recipe\Get\GetRecipeCommandHandler;
use RecipeBook\Application\Service\Api\Recipe\Get\RecipeIngredient\GetRecipeIngredientCommand;
use RecipeBook\Application\Service\Api\Recipe\Get\RecipeIngredient\GetRecipeIngredientCommandHandler;
use RecipeBook\Application\Service\Api\Recipe\Get\RecipeStep\GetRecipeStepCommand;
use RecipeBook\Application\Service\Api\Recipe\Get\RecipeStep\GetRecipeStepCommandHandler;
use RecipeBook\Application\Service\Api\Recipe\Update\Category\UpdateRecipeCategoryCommand;
use RecipeBook\Application\Service\Api\Recipe\Update\Category\UpdateRecipeCategoryCommandHandler;
use RecipeBook\Application\Service\Api\Recipe\Update\RecipeIngredient\UpdateRecipeIngredientCommand;
use RecipeBook\Application\Service\Api\Recipe\Update\RecipeIngredient\UpdateRecipeIngredientCommandHandler;
use RecipeBook\Application\Service\Api\Recipe\Update\RecipeStep\UpdateRecipeStepCommand;
use RecipeBook\Application\Service\Api\Recipe\Update\RecipeStep\UpdateRecipeStepCommandHandler;
use RecipeBook\Application\Service\Api\Recipe\Update\ThermomixModel\UpdateRecipeThermomixModelCommand;
use RecipeBook\Application\Service\Api\Recipe\Update\ThermomixModel\UpdateRecipeThermomixModelCommandHandler;
use RecipeBook\Application\Service\Api\Recipe\Update\UpdateRecipeCommand;
use RecipeBook\Application\Service\Api\Recipe\Update\UpdateRecipeCommandHandler;
use RecipeBook\Application\Service\Api\Thermomix\Count\ThermomixModel\CountThermomixModelsCommand;
use RecipeBook\Application\Service\Api\Thermomix\Count\ThermomixModel\CountThermomixModelsCommandHandler;
use RecipeBook\Application\Service\Api\Thermomix\Create\ThermomixAction\CreateThermomixActionCommand;
use RecipeBook\Application\Service\Api\Thermomix\Create\ThermomixAction\CreateThermomixActionCommandHandler;
use RecipeBook\Application\Service\Api\Thermomix\Create\ThermomixModel\CreateThermomixModelCommand;
use RecipeBook\Application\Service\Api\Thermomix\Create\ThermomixModel\CreateThermomixModelCommandHandler;
use RecipeBook\Application\Service\Api\Thermomix\Find\ThermomixAction\FindThermomixActionsCommand;
use RecipeBook\Application\Service\Api\Thermomix\Find\ThermomixAction\FindThermomixActionsCommandHandler;
use RecipeBook\Application\Service\Api\Thermomix\Find\ThermomixModel\FindThermomixModelsCommand;
use RecipeBook\Application\Service\Api\Thermomix\Find\ThermomixModel\FindThermomixModelsCommandHandler;
use RecipeBook\Application\Service\Api\Thermomix\Get\ThermomixModel\GetThermomixModelCommand;
use RecipeBook\Application\Service\Api\Thermomix\Get\ThermomixModel\GetThermomixModelCommandHandler;
use RecipeBook\Application\Service\Api\Unit\Count\CountUnitsCommand;
use RecipeBook\Application\Service\Api\Unit\Count\CountUnitsCommandHandler;
use RecipeBook\Application\Service\Api\Unit\Create\CreateUnitCommand;
use RecipeBook\Application\Service\Api\Unit\Create\CreateUnitCommandHandler;
use RecipeBook\Application\Service\Api\Unit\Find\FindUnitsCommand;
use RecipeBook\Application\Service\Api\Unit\Find\FindUnitsCommandHandler;
use RecipeBook\Application\Service\Api\Unit\Get\GetUnitCommand;
use RecipeBook\Application\Service\Api\Unit\Get\GetUnitCommandHandler;
use RecipeBook\Application\Service\Api\User\Count\CountUsersCommand;
use RecipeBook\Application\Service\Api\User\Count\CountUsersCommandHandler;
use RecipeBook\Application\Service\Api\User\Create\UserFavorite\CreateUserFavoriteCommand;
use RecipeBook\Application\Service\Api\User\Create\UserFavorite\CreateUserFavoriteCommandHandler;
use RecipeBook\Application\Service\Api\User\Delete\UserFavorite\DeleteUserFavoriteCommand;
use RecipeBook\Application\Service\Api\User\Delete\UserFavorite\DeleteUserFavoriteCommandHandler;
use RecipeBook\Application\Service\Api\User\Find\FindUsersCommand;
use RecipeBook\Application\Service\Api\User\Find\FindUsersCommandHandler;
use RecipeBook\Application\Service\Api\User\Find\UserFavorite\FindUserFavoritesCommand;
use RecipeBook\Application\Service\Api\User\Find\UserFavorite\FindUserFavoritesCommandHandler;
use RecipeBook\Application\Service\Api\User\Get\GetUserCommand;
use RecipeBook\Application\Service\Api\User\Get\GetUserCommandHandler;
use RecipeBook\Application\Service\Api\User\Login\LoginCommand;
use RecipeBook\Application\Service\Api\User\Login\LoginCommandHandler;
use RecipeBook\Application\Service\Api\User\Register\RegisterCommand;
use RecipeBook\Application\Service\Api\User\Register\RegisterCommandHandler;

return [
    'inflector'  => HandleInflector::class,
    'locator'    => LaravelLocator::class,
    'extractor'  => ClassNameExtractor::class,
    'bus'        => CommandBus::class,
    'handle-map' => [
        RegisterCommand::class => RegisterCommandHandler::class,
        LoginCommand::class => LoginCommandHandler::class,
        GetUserCommand::class => GetUserCommandHandler::class,
        FindUsersCommand::class => FindUsersCommandHandler::class,
        CountUsersCommand::class => CountUsersCommandHandler::class,

        FindUserFavoritesCommand::class => FindUserFavoritesCommandHandler::class,
        CreateUserFavoriteCommand::class => CreateUserFavoriteCommandHandler::class,
        DeleteUserFavoriteCommand::class => DeleteUserFavoriteCommandHandler::class,

        FindRecipesCommand::class => FindRecipesCommandHandler::class,
        CountRecipesCommand::class => CountRecipesCommandHandler::class,
        CreateRecipeCommand::class => CreateRecipeCommandHandler::class,
        GetRecipeCommand::class => GetRecipeCommandHandler::class,
        UpdateRecipeCommand::class => UpdateRecipeCommandHandler::class,
        DeleteRecipeCommand::class => DeleteRecipeCommandHandler::class,

        UpdateRecipeCategoryCommand::class => UpdateRecipeCategoryCommandHandler::class,
        UpdateRecipeThermomixModelCommand::class => UpdateRecipeThermomixModelCommandHandler::class,

        FindCategoriesCommand::class => FindCategoriesCommandHandler::class,
        CountCategoriesCommand::class => CountCategoriesCommandHandler::class,
        CreateCategoryCommand::class => CreateCategoryCommandHandler::class,
        GetCategoryCommand::class => GetCategoryCommandHandler::class,

        FindUnitsCommand::class => FindUnitsCommandHandler::class,
        CountUnitsCommand::class => CountUnitsCommandHandler::class,
        CreateUnitCommand::class => CreateUnitCommandHandler::class,
        GetUnitCommand::class => GetUnitCommandHandler::class,

        FindIngredientsCommand::class => FindIngredientsCommandHandler::class,
        CountIngredientsCommand::class => CountIngredientsCommandHandler::class,
        CreateIngredientCommand::class => CreateIngredientCommandHandler::class,
        GetIngredientCommand::class => GetIngredientCommandHandler::class,

        FindThermomixModelsCommand::class => FindThermomixModelsCommandHandler::class,
        CountThermomixModelsCommand::class => CountThermomixModelsCommandHandler::class,
        CreateThermomixModelCommand::class => CreateThermomixModelCommandHandler::class,
        GetThermomixModelCommand::class => GetThermomixModelCommandHandler::class,

        FindThermomixActionsCommand::class => FindThermomixActionsCommandHandler::class,
        CreateThermomixActionCommand::class => CreateThermomixActionCommandHandler::class,

        CreateRecipeIngredientCommand::class => CreateRecipeIngredientCommandHandler::class,
        GetRecipeIngredientCommand::class => GetRecipeIngredientCommandHandler::class,
        UpdateRecipeIngredientCommand::class => UpdateRecipeIngredientCommandHandler::class,
        DeleteRecipeIngredientCommand::class => DeleteRecipeIngredientCommandHandler::class,

        CreateRecipeStepCommand::class => CreateRecipeStepCommandHandler::class,
        GetRecipeStepCommand::class => GetRecipeStepCommandHandler::class,
        UpdateRecipeStepCommand::class => UpdateRecipeStepCommandHandler::class,
        DeleteRecipeStepCommand::class => DeleteRecipeStepCommandHandler::class,

        CreateRecipePhotoCommand::class => CreateRecipePhotoCommandHandler::class,
        DeleteRecipePhotoCommand::class => DeleteRecipePhotoCommandHandler::class
    ]
];
