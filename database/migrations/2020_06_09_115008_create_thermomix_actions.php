<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateThermomixActions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('thermomix_actions', function (Blueprint $table) {
            $table->string('id')->primary();
            $table->string('name');
            $table->text('description')->nullable();
            $table->timestamps();
        });

        Schema::create('thermomix_action_models', function (Blueprint $table) {
            $table->string('thermomix_action_id');
            $table->string('thermomix_model_id');

            $table->foreign('thermomix_action_id')->references('id')->on('thermomix_actions');
            $table->foreign('thermomix_model_id')->references('id')->on('thermomix_models');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('thermomix_actions');
        Schema::dropIfExists('thermomix_action_models');
    }
}
