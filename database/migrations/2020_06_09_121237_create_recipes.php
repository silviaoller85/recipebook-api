<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecipes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recipes', function (Blueprint $table) {
            $table->string('id')->primary();
            $table->string('user_id');
            $table->string('name');
            $table->text('description')->nullable();
            $table->float('preparation_time');
            $table->float('cooking_time');
            $table->float('serves');
            $table->string('photo')->nullable();
            $table->boolean('is_thermomix');

            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
        });

        Schema::create('recipe_ingredients', function (Blueprint $table) {
            $table->string('id')->primary();
            $table->string('recipe_id');
            $table->string('ingredient_id');
            $table->string('value');
            $table->string('unit_id');
            $table->timestamps();

            $table->foreign('recipe_id')->references('id')->on('recipes');
            $table->foreign('ingredient_id')->references('id')->on('ingredients');
            $table->foreign('unit_id')->references('id')->on('units');
        });

        Schema::create('recipe_thermomix_models', function (Blueprint $table) {
            $table->string('recipe_id');
            $table->string('thermomix_model_id');

            $table->foreign('recipe_id')->references('id')->on('recipes');
            $table->foreign('thermomix_model_id')->references('id')->on('thermomix_models');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recipe_ingredients');
        Schema::dropIfExists('recipe_thermomix_models');
        Schema::dropIfExists('recipes');
    }
}
