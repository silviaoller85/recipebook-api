<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecipeSteps extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recipe_steps', function (Blueprint $table) {
            $table->string('id')->primary();
            $table->string('recipe_id');
            $table->integer('number');
            $table->string('name');
            $table->text('description')->nullable();

            $table->timestamps();

            $table->foreign('recipe_id')->references('id')->on('recipes');
        });

        Schema::create('recipe_step_thermomix_actions', function (Blueprint $table) {
            $table->string('id')->primary();
            $table->string('recipe_step_id');
            $table->string('thermomix_action_id');
            $table->string('value')->nullable();

            $table->timestamps();

            $table->foreign('recipe_step_id')->references('id')->on('recipe_steps');
            $table->foreign('thermomix_action_id')->references('id')->on('thermomix_actions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recipe_step_thermomix_actions');
        Schema::dropIfExists('recipe_steps');
    }
}
