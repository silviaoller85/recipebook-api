<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecipePhotos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recipe_photos', function (Blueprint $table) {
            $table->string('id');
            $table->string('recipe_id');
            $table->string('recipe_step_id')->nullable();
            $table->string('path');
            $table->string('url');
            $table->integer('number');

            $table->foreign('recipe_id')->references('id')->on('recipes');
            $table->foreign('recipe_step_id')->references('id')->on('recipe_steps');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recipe_photos');
    }
}
