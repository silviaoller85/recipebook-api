<?php

namespace RecipeBook\Domain\Common\ValueObject;

use InvalidArgumentException;
use Ramsey\Uuid\Uuid as RamseyUuid;

class Uuid
{
    /**
     * @var string
     */
    private $value;

    /**
     * Uuid constructor.
     * @param string $value
     */
    public function __construct(string $value)
    {
        $this->guard($value);

        $this->value = $value;
    }

    /**
     * @param string $value
     * @return Uuid
     */
    public static function fromString(string $value = null): ?self
    {
        if (is_null($value)) {
            return null;
        }
        return new self($value);
    }

    /**
     * @return Uuid
     * @throws \Exception
     */
    public static function random(): self
    {
        return new self(RamseyUuid::uuid4()->toString());
    }

    /**
     * @return string|null
     */
    public function value(): ?string
    {
        return $this->value;
    }

    /**
     * @param $id
     */
    private function guard($id): void
    {
        if (env('APP_ENV') == 'testing') {
            return;
        }

        if (!RamseyUuid::isValid($id)) {
            throw new InvalidArgumentException(
                sprintf('<%s> does not allow the value <%s>.', static::class, is_scalar($id) ? $id : gettype($id))
            );
        }
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->value();
    }
}
