<?php


namespace RecipeBook\Domain\Common;

use DateTimeImmutable;

/**
 * Trait Timestamps
 *
 * @package RecipeBook\Domain\Common
 * @property DateTimeImmutable $updatedAt
 * @property DateTimeImmutable $createdAt
 */
trait Timestamps
{
    /**
     * @throws \Exception
     */
    public function setUpdatedAtTime(): void
    {
        $this->updatedAt = new DateTimeImmutable;
    }

    /**
     * @throws \Exception
     */
    public function setCreatedAtTime(): void
    {
        $this->createdAt = new DateTimeImmutable;
    }

    /**
     * @return DateTimeImmutable
     */
    public function createdAt(): DateTimeImmutable
    {
        return $this->createdAt;
    }

    /**
     * @return DateTimeImmutable
     */
    public function updatedAt(): DateTimeImmutable
    {
        return $this->updatedAt;
    }
}
