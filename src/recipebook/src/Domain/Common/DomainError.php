<?php

namespace RecipeBook\Domain\Common;

use DomainException;

/**
 * Class DomainError
 * @package RecipeBook\Domain\Common
 */
abstract class DomainError extends DomainException
{
    public function __construct()
    {
        parent::__construct($this->errorMessage());
    }

    abstract public function errorCode(): string;

    abstract protected function errorMessage(): string;
}
