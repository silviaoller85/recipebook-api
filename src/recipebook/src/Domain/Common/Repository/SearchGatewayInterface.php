<?php

namespace RecipeBook\Domain\Common\Repository;

/**
 * Interface SearchGatewayInterface
 * @package RecipeBook\Domain\Common\Repository
 */
interface SearchGatewayInterface
{

    /**
     * @param $id
     *
     * @return null|mixed
     */
    public function find($id);


    /**
     * @param array $criteria
     *
     * @return null|mixed
     */
    public function findOneBy(array $criteria);


    /**
     * @param array $criteria
     *
     * @return array
     */
    public function findBy(array $criteria);
}
