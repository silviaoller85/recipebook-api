<?php

namespace RecipeBook\Domain\Common\Repository;

/**
 * Interface FilterGatewayInterface
 * @package RecipeBook\Domain\Common\Repository
 */
interface FilterGatewayInterface
{

    const RESULT_MODE = 'result_mode';

    const GET_ONE_OR_NULL = 'one_or_null';

    const RESULT_MODES = [
        self::GET_ONE_OR_NULL,
    ];


    /**
     * @param array $filters
     * @param array $options
     *
     * @return array
     */
    public function filter(array $filters, array $options = []): ?array;

    /**
     * @param array $filters
     * @return integer
     */
    public function count(array $filters) : int;
}
