<?php

namespace RecipeBook\Domain\Common\Repository;

/**
 * Interface PersistenceGatewayInterface
 * @package RecipeBook\Domain\Common\Repository
 */
interface PersistenceGatewayInterface
{

    /**
     * @param $entity
     */
    public function persist($entity);
}
