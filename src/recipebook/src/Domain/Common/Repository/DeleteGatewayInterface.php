<?php

namespace RecipeBook\Domain\Common\Repository;

/**
 * Interface DeleteGatewayInterface
 * @package RecipeBook\Domain\Common\Repository
 */
interface DeleteGatewayInterface
{
    public function delete($entity);
}
