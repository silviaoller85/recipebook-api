<?php

namespace RecipeBook\Domain\Model\Recipe;

use RecipeBook\Domain\Common\DomainError;

final class RecipesNotFoundException extends DomainError
{
    /**
     * @var array
     */
    private $filters;

    /**
     * RecipesNotFoundException constructor.
     * @param array $filters
     */
    public function __construct(array $filters = [])
    {
        $this->filters = $filters;

        parent::__construct();
    }

    /**
     * @return string
     */
    public function errorCode(): string
    {
        return 'recipes_not_found';
    }

    /**
     * @return string
     */
    protected function errorMessage(): string
    {
        return sprintf('Recipes has not found with these filters: %s', json_encode($this->filters));
    }
}
