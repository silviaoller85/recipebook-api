<?php

namespace RecipeBook\Domain\Model\Recipe;

use Doctrine\Common\Collections\ArrayCollection;
use RecipeBook\Domain\Common\Timestamps;
use RecipeBook\Domain\Common\ValueObject\Uuid;
use RecipeBook\Domain\Model\Category\Category;
use RecipeBook\Domain\Model\Recipe\RecipeIngredient\RecipeIngredient;
use RecipeBook\Domain\Model\Recipe\RecipePhoto\RecipePhoto;
use RecipeBook\Domain\Model\Recipe\RecipeStep\RecipeStep;
use RecipeBook\Domain\Model\Thermomix\ThermomixModel\ThermomixModel;
use RecipeBook\Domain\Model\User\User;

class Recipe
{
    use Timestamps;

    /**
     * @var Uuid
     */
    private $id;

    /**
     * @var User
     */
    private $user;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $slug;

    /**
     * @var string|null
     */
    private $description;

    /**
     * @var float
     */
    private $preparationTime;

    /**
     * @var float
     */
    private $cookingTime;

    /**
     * @var float
     */
    private $serves;

    /**
     * @var string|null
     */
    private $photo;

    /**
     * @var boolean
     */
    private $isThermomix;

    /**
     * @var Category[]
     */
    private $categories;

    /**
     * @var RecipeIngredient[]
     */
    private $ingredients;

    /**
     * @var ThermomixModel[]
     */
    private $thermomixModels;

    /**
     * @var RecipeStep[]
     */
    private $steps;

    /**
     * @var ArrayCollection
     */
    private $photos;

    /**
     * @var ArrayCollection
     */
    private $favorites;

    /**
     * @var \DateTimeImmutable
     */
    private $createdAt;

    /**
     * @var null|\DateTimeImmutable
     */
    private $updatedAt;


    /**
     * Recipe constructor.
     * @param Uuid $id
     * @param User $user
     * @param string $name
     * @param string|null $description
     * @param float $preparationTime
     * @param float $cookingTime
     * @param float $serves
     * @param bool $isThermomix
     * @param ArrayCollection|null $categories
     * @param ArrayCollection|null $thermomixModels
     * @throws \Exception
     */
    public function __construct(
        Uuid $id,
        User $user,
        string $name,
        string $description = null,
        float $preparationTime,
        float $cookingTime,
        float $serves,
        bool $isThermomix,
        ArrayCollection $categories = null,
        ArrayCollection $thermomixModels = null
    ) {
        $this->id = $id;
        $this->user = $user;
        $this->name = $name;
        $this->description = $description;
        $this->preparationTime = $preparationTime;
        $this->cookingTime = $cookingTime;
        $this->serves = $serves;
        $this->isThermomix = $isThermomix;

        $this->categories = $categories ?: new ArrayCollection();
        $this->thermomixModels = $thermomixModels ?: new ArrayCollection();

        $this->ingredients = new ArrayCollection();
        $this->steps = new ArrayCollection();
        $this->photos = new ArrayCollection();
        $this->favorites = new ArrayCollection();

        $this->setCreatedAtTime();
        $this->setUpdatedAtTime();
    }

    /**
     * @param Uuid $id
     * @param User $user
     * @param string $name
     * @param string|null $description
     * @param float $preparationTime
     * @param float $cookingTime
     * @param float $serves
     * @param bool $isThermomix
     * @param ArrayCollection|null $categories
     * @param ArrayCollection|null $thermomixModels
     * @return Recipe
     * @throws \Exception
     */
    public static function create(
        Uuid $id,
        User $user,
        string $name,
        string $description = null,
        float $preparationTime,
        float $cookingTime,
        float $serves,
        bool $isThermomix,
        ArrayCollection $categories = null,
        ArrayCollection $thermomixModels = null
    ) {
        return new self(
            $id,
            $user,
            $name,
            $description,
            $preparationTime,
            $cookingTime,
            $serves,
            $isThermomix,
            $categories,
            $thermomixModels
        );
    }

    /**
     * @return Uuid
     */
    public function id(): Uuid
    {
        return $this->id;
    }

    /**
     * @return User
     */
    public function user(): User
    {
        return $this->user;
    }
    /**
     * @return string
     */
    public function name(): string
    {
        return $this->name;
    }

    /**
     * @return string|null
     */
    public function slug() : ?string
    {
        return $this->slug;
    }

    /**
     * @return string|null
     */
    public function description(): ?string
    {
        return $this->description;
    }

    /**
     * @return float
     */
    public function preparationTime(): float
    {
        return $this->preparationTime;
    }

    /**
     * @return float
     */
    public function cookingTime(): float
    {
        return $this->cookingTime;
    }

    /**
     * @return float
     */
    public function serves(): float
    {
        return $this->serves;
    }

    /**
     * @return string|null
     */
    public function photo(): ?string
    {
        return $this->photo;
    }

    /**
     * @return bool
     */
    public function isThermomix(): bool
    {
        return $this->isThermomix;
    }

    public function categories()
    {
        return $this->categories;
    }

    public function ingredients()
    {
        return $this->ingredients;
    }

    public function thermomixModels()
    {
        return $this->thermomixModels;
    }

    public function steps()
    {
        return $this->steps;
    }

    public function photos()
    {
        return $this->photos;
    }

    public function favorites()
    {
        return $this->favorites;
    }

    /**
     * @param array $data
     */
    public function batchUpdate(array $data)
    {
        if (array_key_exists('user', $data)) {
            $this->user = $data['user'];
        }

        if (array_key_exists('name', $data)) {
            $this->name = $data['name'];
        }

        if (array_key_exists('description', $data)) {
            $this->description = $data['description'];
        }

        if (array_key_exists('preparationTime', $data)) {
            $this->preparationTime = $data['preparationTime'];
        }

        if (array_key_exists('cookingTime', $data)) {
            $this->cookingTime = $data['cookingTime'];
        }

        if (array_key_exists('serves', $data)) {
            $this->serves = $data['serves'];
        }

        if (array_key_exists('photo', $data)) {
            $this->photo = $data['photo'];
        }

        if (array_key_exists('isThermomix', $data)) {
            $this->isThermomix = $data['isThermomix'];
        }
    }

    /**
     * @param Category $category
     */
    public function addCategory(Category $category)
    {
        $this->categories[] = $category;
    }

    /**
     * @param Category $category
     */
    public function removeCategory(Category $category)
    {
        $this->categories->removeElement($category);
    }

    /**
     *
     */
    public function removeAllCategories()
    {
        $categories = $this->categories();

        array_map(function (Category $category) {
            $this->removeCategory($category);
        }, $categories->toArray());
    }

    /**
     * @param ThermomixModel $thermomixModel
     */
    public function addThermomixModel(ThermomixModel $thermomixModel)
    {
        $this->thermomixModels[] = $thermomixModel;
    }

    /**
     * @param ThermomixModel $thermomixModel
     */
    public function removeThermomixModel(ThermomixModel $thermomixModel)
    {
        $this->thermomixModels->removeElement($thermomixModel);
    }

    /**
     *
     */
    public function removeAllThermomixModels()
    {
        $thermomixModels = $this->thermomixModels();

        array_map(function (ThermomixModel $thermomixModel) {
            $this->removeThermomixModel($thermomixModel);
        }, $thermomixModels->toArray());
    }

    /**
     * @return array
     */
    public function toArray() : array
    {
        $categories = $categoriesId = [];
        array_map(function (Category $category) use (&$categories, &$categoriesId) {
            $categories[] = $category->toArrayMinified();
            $categoriesId[] = $category->id()->value();
        }, $this->categories()->toArray());

        $models = $modelsId = [];
        if ($this->isThermomix()) {
            array_map(function (ThermomixModel $thermomixModel) use (&$models, &$modelsId) {
                $models[] = $thermomixModel->toArrayMinified();
                $modelsId[] = $thermomixModel->id()->value();
            }, $this->thermomixModels()->toArray());
        }

        $ingredients = [];
        array_map(function (RecipeIngredient $ingredient) use (&$ingredients) {
            $ingredients[] = $ingredient->toArrayMinified();
        }, $this->ingredients()->toArray());

        $steps = [];
        array_map(function (RecipeStep $step) use (&$steps) {
            $steps[] = $step->toArrayMinified();
        }, $this->steps()->toArray());

        $photos = [];
        array_map(function (RecipePhoto $photo) use (&$photos) {
            $photos[] = $photo->toArrayMinified();
        }, $this->photos()->toArray());

        return [
            'id' => $this->id()->value(),
            'user' => $this->user()->toArrayMinified(),
            'name' => $this->name(),
            'slug' => $this->slug(),
            'description' => $this->description(),
            'preparationTime' => $this->preparationTime(),
            'cookingTime' => $this->cookingTime(),
            'serves' => $this->serves(),
            'photo' => $this->photo(),
            'isThermomix' => $this->isThermomix(),
            'categories' => $categories,
            'categoriesId' => $categoriesId,
            'ingredients' => $ingredients,
            'thermomixModels' => $models,
            'thermomixModelsId' => $modelsId,
            'steps' => $steps,
            'photos' => $photos,
            'favorites' => $this->favorites()->count()
        ];
    }

    /**
     * @return array
     */
    public function toArrayMinified() : array
    {
        return [
            'id' => $this->id()->value(),
            'name' => $this->name(),
        ];
    }

    /**
     * @return array
     */
    public function jsonSerialize() : array
    {
        return $this->toArray();
    }
}
