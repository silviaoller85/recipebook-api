<?php

namespace RecipeBook\Domain\Model\Recipe\RecipeIngredient;

use RecipeBook\Domain\Common\Repository\DeleteGatewayInterface;
use RecipeBook\Domain\Common\Repository\PersistenceGatewayInterface;
use RecipeBook\Domain\Common\Repository\SearchGatewayInterface;
use RecipeBook\Domain\Common\Repository\FilterGatewayInterface;

/**
 * Interface RecipeIngredientRepositoryGatewayInterface
 * @package RecipeBook\Domain\Model\Recipe
 */
interface RecipeIngredientRepositoryGatewayInterface extends
    PersistenceGatewayInterface,
    SearchGatewayInterface,
    FilterGatewayInterface,
    DeleteGatewayInterface
{

    public function findOneById(string $id): ?RecipeIngredient;
}
