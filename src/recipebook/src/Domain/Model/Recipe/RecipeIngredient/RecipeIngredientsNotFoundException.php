<?php

namespace RecipeBook\Domain\Model\Recipe\RecipeIngredient;

use RecipeBook\Domain\Common\DomainError;

final class RecipeIngredientsNotFoundException extends DomainError
{
    /**
     * @var array
     */
    private $filters;

    /**
     * RecipesNotFoundException constructor.
     * @param array $filters
     */
    public function __construct(array $filters = [])
    {
        $this->filters = $filters;

        parent::__construct();
    }

    /**
     * @return string
     */
    public function errorCode(): string
    {
        return 'recipe_ingredients_not_found';
    }

    /**
     * @return string
     */
    protected function errorMessage(): string
    {
        return sprintf('Recipe ingredients has not found with these filters: %s', json_encode($this->filters));
    }
}
