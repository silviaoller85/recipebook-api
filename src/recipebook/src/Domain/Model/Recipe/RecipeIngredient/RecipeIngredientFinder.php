<?php

namespace RecipeBook\Domain\Model\Recipe\RecipeIngredient;

final class RecipeIngredientFinder
{
    /**
     * @var RecipeIngredientRepositoryGatewayInterface
     */
    private $repository;

    /**
     * RecipeFinder constructor.
     * @param RecipeIngredientRepositoryGatewayInterface $repository
     */
    public function __construct(RecipeIngredientRepositoryGatewayInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param array $filters
     * @param array $options
     * @return array
     */
    public function findBy(array $filters = [], array $options = []) : array
    {
        $recipes = $this->repository->filter($filters, $options);

        if ($recipes === null) {
            throw new RecipeIngredientsNotFoundException($filters);
        }

        return $recipes;
    }

    /**
     * @param string $id
     * @return RecipeIngredient
     */
    public function getById(string $id) : RecipeIngredient
    {
        $recipe = $this->repository->findOneBy(['id' => $id]);

        if ($recipe === null) {
            throw new RecipeIngredientNotFoundException($id);
        }

        return $recipe;
    }

    /**
     * @param array $filters
     *
     * @return int
     */
    public function count(array $filters = []): int
    {
        return $this->repository->count($filters);
    }
}
