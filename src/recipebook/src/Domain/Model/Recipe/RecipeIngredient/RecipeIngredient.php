<?php

namespace RecipeBook\Domain\Model\Recipe\RecipeIngredient;

use RecipeBook\Domain\Common\Timestamps;
use RecipeBook\Domain\Common\ValueObject\Uuid;
use RecipeBook\Domain\Model\Ingredient\Ingredient;
use RecipeBook\Domain\Model\Recipe\Recipe;
use RecipeBook\Domain\Model\Unit\Unit;

class RecipeIngredient
{
    use Timestamps;

    /**
     * @var Uuid
     */
    private $id;

    /**
     * @var Recipe
     */
    private $recipe;

    /**
     * @var Ingredient
     */
    private $ingredient;

    /**
     * @var string
     */
    private $value;

    /**
     * @var Unit
     */
    private $unit;

    /**
     * @var \DateTimeImmutable
     */
    private $createdAt;

    /**
     * @var null|\DateTimeImmutable
     */
    private $updatedAt;


    /**
     * Unit constructor.
     * @param Uuid $id
     * @param Recipe $recipe
     * @param Ingredient $ingredient
     * @param string $value
     * @param Unit $unit
     * @throws \Exception
     */
    public function __construct(
        Uuid $id,
        Recipe $recipe,
        Ingredient $ingredient,
        string $value,
        Unit $unit
    ) {
        $this->id = $id;
        $this->recipe = $recipe;
        $this->ingredient = $ingredient;
        $this->value = $value;
        $this->unit = $unit;

        $this->setCreatedAtTime();
        $this->setUpdatedAtTime();
    }

    /**
     * @param Uuid $id
     * @param Recipe $recipe
     * @param Ingredient $ingredient
     * @param string $value
     * @param Unit $unit
     * @return RecipeIngredient
     * @throws \Exception
     */
    public static function create(
        Uuid $id,
        Recipe $recipe,
        Ingredient $ingredient,
        string $value,
        Unit $unit
    ) {
        return new self($id, $recipe, $ingredient, $value, $unit);
    }

    /**
     * @return Uuid
     */
    public function id(): Uuid
    {
        return $this->id;
    }

    /**
     * @return Recipe
     */
    public function recipe(): Recipe
    {
        return $this->recipe;
    }

    /**
     * @return Ingredient
     */
    public function ingredient(): Ingredient
    {
        return $this->ingredient;
    }

    /**
     * @return string
     */
    public function value(): string
    {
        return $this->value;
    }

    /**
     * @return Unit
     */
    public function unit(): Unit
    {
        return $this->unit;
    }

    /**
     * @param array $data
     */
    public function batchUpdate(array $data)
    {
        if (array_key_exists('recipe', $data)) {
            $this->recipe = $data['recipe'];
        }

        if (array_key_exists('ingredient', $data)) {
            $this->ingredient = $data['ingredient'];
        }

        if (array_key_exists('value', $data)) {
            $this->value = $data['value'];
        }

        if (array_key_exists('unit', $data)) {
            $this->unit = $data['unit'];
        }
    }

    /**
     * @return array
     */
    public function toArray() : array
    {
        return [
            'id' => $this->id()->value(),
            'recipe' => $this->recipe()->toArrayMinified(),
            'ingredientId' => $this->ingredient()->id()->value(),
            'ingredient' => $this->ingredient()->toArrayMinified(),
            'value' => $this->value(),
            'unitId' => $this->unit()->id()->value(),
            'unit' => $this->unit()->toArrayMinified(),
        ];
    }

    /**
     * @return array
     */
    public function toArrayMinified() : array
    {
        return $this->toArray();
    }

    /**
     * @return array
     */
    public function jsonSerialize() : array
    {
        return $this->toArray();
    }
}
