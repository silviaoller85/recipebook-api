<?php

namespace RecipeBook\Domain\Model\Recipe;

use RecipeBook\Domain\Common\Repository\DeleteGatewayInterface;
use RecipeBook\Domain\Common\Repository\PersistenceGatewayInterface;
use RecipeBook\Domain\Common\Repository\SearchGatewayInterface;
use RecipeBook\Domain\Common\Repository\FilterGatewayInterface;

/**
 * Interface RecipeRepositoryGatewayInterface
 * @package RecipeBook\Domain\Model\Recipe
 */
interface RecipeRepositoryGatewayInterface extends
    PersistenceGatewayInterface,
    SearchGatewayInterface,
    FilterGatewayInterface,
    DeleteGatewayInterface
{

    public function findOneById(string $id): ?Recipe;
}
