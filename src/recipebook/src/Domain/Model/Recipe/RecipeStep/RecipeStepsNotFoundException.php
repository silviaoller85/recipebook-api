<?php

namespace RecipeBook\Domain\Model\Recipe\RecipeStep;

use RecipeBook\Domain\Common\DomainError;

final class RecipeStepsNotFoundException extends DomainError
{
    /**
     * @var array
     */
    private $filters;

    /**
     * RecipesNotFoundException constructor.
     * @param array $filters
     */
    public function __construct(array $filters = [])
    {
        $this->filters = $filters;

        parent::__construct();
    }

    /**
     * @return string
     */
    public function errorCode(): string
    {
        return 'recipe_steps_not_found';
    }

    /**
     * @return string
     */
    protected function errorMessage(): string
    {
        return sprintf('Recipe steps has not found with these filters: %s', json_encode($this->filters));
    }
}
