<?php

namespace RecipeBook\Domain\Model\Recipe\RecipeStep;

use Doctrine\Common\Collections\ArrayCollection;
use RecipeBook\Domain\Common\Timestamps;
use RecipeBook\Domain\Common\ValueObject\Uuid;
use RecipeBook\Domain\Model\Recipe\Recipe;
use RecipeBook\Domain\Model\Thermomix\ThermomixAction\ThermomixAction;

class RecipeStep
{
    use Timestamps;

    /**
     * @var Uuid
     */
    private $id;

    /**
     * @var Recipe
     */
    private $recipe;

    /**
     * @var integer
     */
    private $number;

    /**
     * @var string|null
     */
    private $name;

    /**
     * @var string
     */
    private $description;

    /**
     * @var ThermomixAction[]
     */
    private $thermomixActions;

    /**
     * @var \DateTimeImmutable
     */
    private $createdAt;

    /**
     * @var null|\DateTimeImmutable
     */
    private $updatedAt;


    /**
     * RecipeStep constructor.
     * @param Uuid $id
     * @param Recipe $recipe
     * @param int $number
     * @param string|null $name
     * @param string $description
     * @throws \Exception
     */
    public function __construct(
        Uuid $id,
        Recipe $recipe,
        int $number,
        string $name = null,
        string $description
    ) {
        $this->id = $id;
        $this->recipe = $recipe;
        $this->number = $number;
        $this->name = $name;
        $this->description = $description;

        $this->thermomixActions = new ArrayCollection();

        $this->setCreatedAtTime();
        $this->setUpdatedAtTime();
    }

    /**
     * @param Uuid $id
     * @param Recipe $recipe
     * @param int $number
     * @param string|null $name
     * @param string $description
     * @return RecipeStep
     * @throws \Exception
     */
    public static function create(
        Uuid $id,
        Recipe $recipe,
        int $number,
        string $name = null,
        string $description
    ) {
        return new self($id, $recipe, $number, $name, $description);
    }

    /**
     * @return Uuid
     */
    public function id(): Uuid
    {
        return $this->id;
    }

    /**
     * @return Recipe
     */
    public function recipe(): Recipe
    {
        return $this->recipe;
    }

    /**
     * @return int
     */
    public function number(): int
    {
        return $this->number;
    }

    /**
     * @return string|null
     */
    public function name(): ?string
    {
        return $this->name;
    }

    /**
     * @return string|null
     */
    public function description(): ?string
    {
        return $this->description;
    }

    public function thermomixActions()
    {
        return $this->thermomixActions;
    }

    /**
     * @param array $data
     */
    public function batchUpdate(array $data)
    {
        if (array_key_exists('recipe', $data)) {
            $this->recipe = $data['recipe'];
        }

        if (array_key_exists('number', $data)) {
            $this->number = $data['number'];
        }

        if (array_key_exists('name', $data)) {
            $this->name = $data['name'];
        }

        if (array_key_exists('description', $data)) {
            $this->description = $data['description'];
        }
    }

    /**
     * @return array
     */
    public function toArray() : array
    {
        $recipe = $this->recipe();

        $thermomixActions = [];
        if ($recipe->isThermomix()) {
            array_map(function (ThermomixAction $thermomixAction) use (&$thermomixActions) {
                $thermomixActions[] = $thermomixAction->toArrayMinified();
            }, $this->thermomixActions()->toArray());
        }

        return [
            'id' => $this->id()->value(),
            'recipe' => $recipe->toArrayMinified(),
            'number' => $this->number(),
            'name' => $this->name(),
            'description' => $this->description(),
            'thermomixActions' => $thermomixActions
        ];
    }

    /**
     * @return array
     */
    public function toArrayMinified() : array
    {
        return $this->toArray();
    }

    /**
     * @return array
     */
    public function jsonSerialize() : array
    {
        return $this->toArray();
    }
}
