<?php

namespace RecipeBook\Domain\Model\Recipe\RecipeStep;

use RecipeBook\Domain\Common\Timestamps;
use RecipeBook\Domain\Common\ValueObject\Uuid;
use RecipeBook\Domain\Model\Thermomix\ThermomixAction\ThermomixAction;

class RecipeStepThermomixAction
{
    use Timestamps;

    /**
     * @var Uuid
     */
    private $id;

    /**
     * @var RecipeStep
     */
    private $recipeStep;

    /**
     * @var ThermomixAction
     */
    private $thermomixAction;

    /**
     * @var string|null
     */
    private $value;

    /**
     * @var \DateTimeImmutable
     */
    private $createdAt;

    /**
     * @var null|\DateTimeImmutable
     */
    private $updatedAt;


    /**
     * Unit constructor.
     * @param Uuid $id
     * @param RecipeStep $recipeStep
     * @param ThermomixAction $thermomixAction
     * @param string $value
     * @throws \Exception
     */
    public function __construct(
        Uuid $id,
        RecipeStep $recipeStep,
        ThermomixAction $thermomixAction,
        string $value = null
    ) {
        $this->id = $id;
        $this->recipeStep = $recipeStep;
        $this->thermomixAction = $thermomixAction;
        $this->value = $value;

        $this->setCreatedAtTime();
        $this->setUpdatedAtTime();
    }

    /**
     * @param Uuid $id
     * @param RecipeStep $recipeStep
     * @param ThermomixAction $thermomixAction
     * @param string $value
     * @return RecipeStepThermomixAction
     * @throws \Exception
     */
    public static function create(
        Uuid $id,
        RecipeStep $recipeStep,
        ThermomixAction $thermomixAction,
        string $value = null
    ) {
        return new self($id, $recipeStep, $thermomixAction, $value);
    }

    /**
     * @return Uuid
     */
    public function id(): Uuid
    {
        return $this->id;
    }

    /**
     * @return RecipeStep
     */
    public function recipeStep(): RecipeStep
    {
        return $this->recipeStep;
    }

    /**
     * @return ThermomixAction
     */
    public function thermomixAction(): ThermomixAction
    {
        return $this->thermomixAction;
    }

    /**
     * @return string|null
     */
    public function value(): ?string
    {
        return $this->value;
    }

    /**
     * @param array $data
     */
    public function batchUpdate(array $data)
    {
        if (array_key_exists('recipeStep', $data)) {
            $this->recipeStep = $data['recipeStep'];
        }

        if (array_key_exists('thermomixAction', $data)) {
            $this->thermomixAction = $data['thermomixAction'];
        }

        if (array_key_exists('value', $data)) {
            $this->value = $data['value'];
        }
    }

    /**
     * @return array
     */
    public function toArray() : array
    {
        return [
            'id' => $this->id()->value(),
            'step' => $this->recipeStep()->toArrayMinified(),
            'thermomixAction' => $this->thermomixAction()->toArrayMinified(),
            'value' => $this->value()
        ];
    }

    /**
     * @return array
     */
    public function toArrayMinified() : array
    {
        return $this->toArray();
    }

    /**
     * @return array
     */
    public function jsonSerialize() : array
    {
        return $this->toArray();
    }
}
