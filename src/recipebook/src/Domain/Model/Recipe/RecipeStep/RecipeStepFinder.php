<?php

namespace RecipeBook\Domain\Model\Recipe\RecipeStep;

final class RecipeStepFinder
{
    /**
     * @var RecipeStepRepositoryGatewayInterface
     */
    private $repository;

    /**
     * RecipeFinder constructor.
     * @param RecipeStepRepositoryGatewayInterface $repository
     */
    public function __construct(RecipeStepRepositoryGatewayInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param array $filters
     * @param array $options
     * @return array
     */
    public function findBy(array $filters = [], array $options = []) : array
    {
        $recipes = $this->repository->filter($filters, $options);

        if ($recipes === null) {
            throw new RecipeStepsNotFoundException($filters);
        }

        return $recipes;
    }

    /**
     * @param string $id
     * @return RecipeStep
     */
    public function getById(string $id) : RecipeStep
    {
        $recipe = $this->repository->findOneBy(['id' => $id]);

        if ($recipe === null) {
            throw new RecipeStepNotFoundException($id);
        }

        return $recipe;
    }

    /**
     * @param array $filters
     *
     * @return int
     */
    public function count(array $filters = []): int
    {
        return $this->repository->count($filters);
    }
}
