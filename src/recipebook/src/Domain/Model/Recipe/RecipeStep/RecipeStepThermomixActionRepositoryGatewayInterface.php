<?php

namespace RecipeBook\Domain\Model\Recipe\RecipeStep;

use RecipeBook\Domain\Common\Repository\PersistenceGatewayInterface;
use RecipeBook\Domain\Common\Repository\SearchGatewayInterface;
use RecipeBook\Domain\Common\Repository\FilterGatewayInterface;

/**
 * Interface RecipeStepThermomixActionRepositoryGatewayInterface
 * @package RecipeBook\Domain\Model\Recipe
 */
interface RecipeStepThermomixActionRepositoryGatewayInterface extends
    PersistenceGatewayInterface,
    SearchGatewayInterface,
    FilterGatewayInterface
{

    public function findOneById(string $id): ?RecipeStepThermomixAction;
}
