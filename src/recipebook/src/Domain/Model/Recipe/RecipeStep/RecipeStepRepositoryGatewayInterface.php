<?php

namespace RecipeBook\Domain\Model\Recipe\RecipeStep;

use RecipeBook\Domain\Common\Repository\DeleteGatewayInterface;
use RecipeBook\Domain\Common\Repository\PersistenceGatewayInterface;
use RecipeBook\Domain\Common\Repository\SearchGatewayInterface;
use RecipeBook\Domain\Common\Repository\FilterGatewayInterface;

/**
 * Interface RecipeStepRepositoryGatewayInterface
 * @package RecipeBook\Domain\Model\Recipe
 */
interface RecipeStepRepositoryGatewayInterface extends
    PersistenceGatewayInterface,
    SearchGatewayInterface,
    FilterGatewayInterface,
    DeleteGatewayInterface
{

    public function findOneById(string $id): ?RecipeStep;
}
