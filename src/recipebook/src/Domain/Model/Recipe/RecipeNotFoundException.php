<?php

namespace RecipeBook\Domain\Model\Recipe;

use RecipeBook\Domain\Common\DomainError;

final class RecipeNotFoundException extends DomainError
{
    /**
     * @var string
     */
    private $id;

    /**
     * RecipeNotFoundException constructor.
     * @param string $id
     */
    public function __construct(string $id)
    {
        $this->id = $id;

        parent::__construct();
    }

    /**
     * @return string
     */
    public function errorCode(): string
    {
        return 'recipe_not_found';
    }

    /**
     * @return string
     */
    protected function errorMessage(): string
    {
        return sprintf('Recipe has not found with this ID: %s', $this->id);
    }
}
