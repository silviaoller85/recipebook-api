<?php

namespace RecipeBook\Domain\Model\Recipe\RecipePhoto;

use RecipeBook\Domain\Common\Repository\DeleteGatewayInterface;
use RecipeBook\Domain\Common\Repository\PersistenceGatewayInterface;
use RecipeBook\Domain\Common\Repository\SearchGatewayInterface;
use RecipeBook\Domain\Common\Repository\FilterGatewayInterface;

/**
 * Interface RecipePhotoRepositoryGatewayInterface
 * @package RecipeBook\Domain\Model\Recipe
 */
interface RecipePhotoRepositoryGatewayInterface extends
    PersistenceGatewayInterface,
    SearchGatewayInterface,
    FilterGatewayInterface,
    DeleteGatewayInterface
{

    public function findOneById(string $id): ?RecipePhoto;
}
