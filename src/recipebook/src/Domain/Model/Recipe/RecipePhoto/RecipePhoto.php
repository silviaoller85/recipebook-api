<?php

namespace RecipeBook\Domain\Model\Recipe\RecipePhoto;

use RecipeBook\Domain\Common\Timestamps;
use RecipeBook\Domain\Common\ValueObject\Uuid;
use RecipeBook\Domain\Model\Recipe\Recipe;
use RecipeBook\Domain\Model\Recipe\RecipeStep\RecipeStep;

class RecipePhoto
{
    use Timestamps;

    /**
     * @var Uuid
     */
    private $id;

    /**
     * @var Recipe
     */
    private $recipe;

    /**
     * @var RecipeStep|null
     */
    private $recipeStep;

    /**
     * @var string
     */
    private $path;

    /**
     * @var string
     */
    private $url;

    /**
     * @var int
     */
    private $number;

    /**
     * @var \DateTimeImmutable
     */
    private $createdAt;

    /**
     * @var null|\DateTimeImmutable
     */
    private $updatedAt;

    /**
     * RecipePhoto constructor.
     * @param Uuid $id
     * @param Recipe $recipe
     * @param RecipeStep $recipeStep
     * @param string $path
     * @param string $url
     * @param int $number
     * @throws \Exception
     */
    public function __construct(
        Uuid $id,
        Recipe $recipe,
        RecipeStep $recipeStep = null,
        string $path,
        string $url,
        int $number
    ) {
        $this->id = $id;
        $this->recipe = $recipe;
        $this->recipeStep = $recipeStep;
        $this->path = $path;
        $this->url = $url;
        $this->number = $number;

        $this->setCreatedAtTime();
        $this->setUpdatedAtTime();
    }

    /**
     * @param Uuid $id
     * @param Recipe $recipe
     * @param RecipeStep $recipeStep
     * @param string $path
     * @param string $url
     * @param int $number
     * @return RecipePhoto
     * @throws \Exception
     */
    public static function create(
        Uuid $id,
        Recipe $recipe,
        RecipeStep $recipeStep = null,
        string $path,
        string $url,
        int $number
    ) {
        return new self($id, $recipe, $recipeStep, $path, $url, $number);
    }

    /**
     * @return Uuid
     */
    public function id(): Uuid
    {
        return $this->id;
    }

    /**
     * @return Recipe
     */
    public function recipe(): Recipe
    {
        return $this->recipe;
    }

    /**
     * @return RecipeStep|null
     */
    public function recipeStep(): ?RecipeStep
    {
        return $this->recipeStep;
    }

    /**
     * @return string
     */
    public function path(): string
    {
        return $this->path;
    }

    /**
     * @return string
     */
    public function url(): string
    {
        return $this->url;
    }

    /**
     * @return int
     */
    public function number(): int
    {
        return $this->number;
    }

    /**
     * @param array $data
     */
    public function batchUpdate(array $data)
    {
        if (array_key_exists('recipe', $data)) {
            $this->recipe = $data['recipe'];
        }

        if (array_key_exists('recipeStep', $data)) {
            $this->recipe = $data['recipeStep'];
        }

        if (array_key_exists('path', $data)) {
            $this->path = $data['path'];
        }

        if (array_key_exists('url', $data)) {
            $this->url = $data['url'];
        }

        if (array_key_exists('number', $data)) {
            $this->number = $data['number'];
        }
    }

    /**
     * @return array
     */
    public function toArray() : array
    {
        return [
            'id' => $this->id()->value(),
            'path' => $this->path(),
            'url' => $this->url()
        ];
    }

    /**
     * @return array
     */
    public function toArrayMinified() : array
    {
        return $this->toArray();
    }

    /**
     * @return array
     */
    public function jsonSerialize() : array
    {
        return $this->toArray();
    }
}
