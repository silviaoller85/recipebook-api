<?php

namespace RecipeBook\Domain\Model\Recipe;

final class RecipeFinder
{
    /**
     * @var RecipeRepositoryGatewayInterface
     */
    private $repository;

    /**
     * RecipeFinder constructor.
     * @param RecipeRepositoryGatewayInterface $repository
     */
    public function __construct(RecipeRepositoryGatewayInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param array $filters
     * @param array $options
     * @return array
     */
    public function findBy(array $filters = [], array $options = []) : array
    {
        $recipes = $this->repository->filter($filters, $options);

        if ($recipes === null) {
            throw new RecipesNotFoundException($filters);
        }

        return $recipes;
    }

    /**
     * @param string $id
     * @return Recipe
     */
    public function getById(string $id) : Recipe
    {
        $recipe = $this->repository->findOneBy(['id' => $id]);

        if ($recipe === null) {
            throw new RecipeNotFoundException($id);
        }

        return $recipe;
    }

    /**
     * @param array $filters
     *
     * @return int
     */
    public function count(array $filters = []): int
    {
        return $this->repository->count($filters);
    }
}
