<?php

namespace RecipeBook\Domain\Model\Thermomix\ThermomixAction;

use RecipeBook\Domain\Common\DomainError;

final class ThermomixActionsNotFoundException extends DomainError
{
    /**
     * @var array
     */
    private $filters;

    /**
     * ThermomixActionsNotFoundException constructor.
     * @param array $filters
     */
    public function __construct(array $filters = [])
    {
        $this->filters = $filters;

        parent::__construct();
    }

    /**
     * @return string
     */
    public function errorCode(): string
    {
        return 'thermomixActions_not_found';
    }

    /**
     * @return string
     */
    protected function errorMessage(): string
    {
        return sprintf('ThermomixActions has not found with these filters: %s', json_encode($this->filters));
    }
}
