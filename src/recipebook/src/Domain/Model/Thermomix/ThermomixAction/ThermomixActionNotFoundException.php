<?php

namespace RecipeBook\Domain\Model\Thermomix\ThermomixAction;

use RecipeBook\Domain\Common\DomainError;

final class ThermomixActionNotFoundException extends DomainError
{
    /**
     * @var string
     */
    private $id;

    /**
     * ThermomixActionNotFoundException constructor.
     * @param string $id
     */
    public function __construct(string $id)
    {
        $this->id = $id;

        parent::__construct();
    }

    /**
     * @return string
     */
    public function errorCode(): string
    {
        return 'thermomixAction_not_found';
    }

    /**
     * @return string
     */
    protected function errorMessage(): string
    {
        return sprintf('ThermomixAction has not found with this ID: %s', $this->id);
    }
}
