<?php

namespace RecipeBook\Domain\Model\Thermomix\ThermomixAction;

final class ThermomixActionFinder
{
    /**
     * @var ThermomixActionRepositoryGatewayInterface
     */
    private $repository;

    /**
     * ThermomixActionFinder constructor.
     * @param ThermomixActionRepositoryGatewayInterface $repository
     */
    public function __construct(ThermomixActionRepositoryGatewayInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param array $filters
     * @param array $options
     * @return array
     */
    public function findBy(array $filters = [], array $options = []) : array
    {
        $thermomixActions = $this->repository->filter($filters, $options);

        if ($thermomixActions === null) {
            throw new ThermomixActionsNotFoundException($filters);
        }

        return $thermomixActions;
    }

    /**
     * @param string $id
     * @return ThermomixAction
     */
    public function getById(string $id) : ThermomixAction
    {
        $thermomixAction = $this->repository->findOneBy(['id' => $id]);

        if ($thermomixAction === null) {
            throw new ThermomixActionNotFoundException($id);
        }

        return $thermomixAction;
    }

    /**
     * @param array $filters
     *
     * @return int
     */
    public function count(array $filters = []): int
    {
        return $this->repository->count($filters);
    }
}
