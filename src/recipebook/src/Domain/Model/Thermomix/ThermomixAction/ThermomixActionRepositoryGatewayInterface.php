<?php

namespace RecipeBook\Domain\Model\Thermomix\ThermomixAction;

use RecipeBook\Domain\Common\Repository\PersistenceGatewayInterface;
use RecipeBook\Domain\Common\Repository\SearchGatewayInterface;
use RecipeBook\Domain\Common\Repository\FilterGatewayInterface;

/**
 * Interface ThermomixActionRepositoryGatewayInterface
 * @package RecipeBook\Domain\Model\Thermomix
 */
interface ThermomixActionRepositoryGatewayInterface extends
    PersistenceGatewayInterface,
    SearchGatewayInterface,
    FilterGatewayInterface
{

    public function findOneById(string $id): ?ThermomixAction;
}
