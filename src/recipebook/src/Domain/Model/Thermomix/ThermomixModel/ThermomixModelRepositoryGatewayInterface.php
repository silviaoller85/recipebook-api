<?php

namespace RecipeBook\Domain\Model\Thermomix\ThermomixModel;

use RecipeBook\Domain\Common\Repository\PersistenceGatewayInterface;
use RecipeBook\Domain\Common\Repository\SearchGatewayInterface;
use RecipeBook\Domain\Common\Repository\FilterGatewayInterface;

/**
 * Interface ThermomixModelRepositoryGatewayInterface
 * @package RecipeBook\Domain\Model\Thermomix
 */
interface ThermomixModelRepositoryGatewayInterface extends
    PersistenceGatewayInterface,
    SearchGatewayInterface,
    FilterGatewayInterface
{

    public function findOneById(string $id): ?ThermomixModel;
}
