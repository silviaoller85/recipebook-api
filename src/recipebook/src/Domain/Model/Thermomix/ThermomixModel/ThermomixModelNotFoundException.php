<?php

namespace RecipeBook\Domain\Model\Thermomix\ThermomixModel;

use RecipeBook\Domain\Common\DomainError;

final class ThermomixModelNotFoundException extends DomainError
{
    /**
     * @var string
     */
    private $id;

    /**
     * ThermomixModelNotFoundException constructor.
     * @param string $id
     */
    public function __construct(string $id)
    {
        $this->id = $id;

        parent::__construct();
    }

    /**
     * @return string
     */
    public function errorCode(): string
    {
        return 'thermomixModel_not_found';
    }

    /**
     * @return string
     */
    protected function errorMessage(): string
    {
        return sprintf('ThermomixModel has not found with this ID: %s', $this->id);
    }
}
