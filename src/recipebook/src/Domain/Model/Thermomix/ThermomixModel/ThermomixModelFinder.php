<?php

namespace RecipeBook\Domain\Model\Thermomix\ThermomixModel;

final class ThermomixModelFinder
{
    /**
     * @var ThermomixModelRepositoryGatewayInterface
     */
    private $repository;

    /**
     * ThermomixModelFinder constructor.
     * @param ThermomixModelRepositoryGatewayInterface $repository
     */
    public function __construct(ThermomixModelRepositoryGatewayInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param array $filters
     * @param array $options
     * @return array
     */
    public function findBy(array $filters = [], array $options = []) : array
    {
        $thermomixModels = $this->repository->filter($filters, $options);

        if ($thermomixModels === null) {
            throw new ThermomixModelsNotFoundException($filters);
        }

        return $thermomixModels;
    }

    /**
     * @param string $id
     * @return ThermomixModel
     */
    public function getById(string $id) : ThermomixModel
    {
        $thermomixModel = $this->repository->findOneBy(['id' => $id]);

        if ($thermomixModel === null) {
            throw new ThermomixModelNotFoundException($id);
        }

        return $thermomixModel;
    }

    /**
     * @param array $filters
     *
     * @return int
     */
    public function count(array $filters = []): int
    {
        return $this->repository->count($filters);
    }
}
