<?php

namespace RecipeBook\Domain\Model\Thermomix\ThermomixModel;

use RecipeBook\Domain\Common\DomainError;

final class ThermomixModelsNotFoundException extends DomainError
{
    /**
     * @var array
     */
    private $filters;

    /**
     * ThermomixModelsNotFoundException constructor.
     * @param array $filters
     */
    public function __construct(array $filters = [])
    {
        $this->filters = $filters;

        parent::__construct();
    }

    /**
     * @return string
     */
    public function errorCode(): string
    {
        return 'thermomixModels_not_found';
    }

    /**
     * @return string
     */
    protected function errorMessage(): string
    {
        return sprintf('ThermomixModels has not found with these filters: %s', json_encode($this->filters));
    }
}
