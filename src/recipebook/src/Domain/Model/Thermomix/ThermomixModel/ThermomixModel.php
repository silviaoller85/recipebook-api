<?php

namespace RecipeBook\Domain\Model\Thermomix\ThermomixModel;

use Doctrine\Common\Collections\ArrayCollection;
use RecipeBook\Domain\Common\Timestamps;
use RecipeBook\Domain\Common\ValueObject\Uuid;
use RecipeBook\Domain\Model\Thermomix\ThermomixAction\ThermomixAction;

class ThermomixModel
{
    use Timestamps;

    /**
     * @var Uuid
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $description;

    /**
     * @var ThermomixAction[]
     */
    private $actions;

    /**
     * @var \DateTimeImmutable
     */
    private $createdAt;

    /**
     * @var null|\DateTimeImmutable
     */
    private $updatedAt;


    /**
     * ThermomixModel constructor.
     * @param Uuid $id
     * @param string $name
     * @param string|null $description
     * @throws \Exception
     */
    public function __construct(
        Uuid $id,
        string $name,
        string $description = null
    ) {
        $this->id = $id;
        $this->name = $name;
        $this->description = $description;

        $this->actions = new ArrayCollection();

        $this->setCreatedAtTime();
        $this->setUpdatedAtTime();
    }

    /**
     * @param Uuid $id
     * @param string $name
     * @param string|null $description
     * @return ThermomixModel
     * @throws \Exception
     */
    public static function create(
        Uuid $id,
        string $name,
        string $description = null
    ) {
        return new self($id, $name, $description);
    }

    /**
     * @return Uuid
     */
    public function id(): Uuid
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function name(): string
    {
        return $this->name;
    }

    /**
     * @return string|null
     */
    public function description(): ?string
    {
        return $this->description;
    }

    /**
     * @return ArrayCollection
     */
    public function actions() : ArrayCollection
    {
        return $this->actions;
    }

    /**
     * @param array $data
     */
    public function batchUpdate(array $data)
    {
        if (array_key_exists('name', $data)) {
            $this->name = $data['name'];
        }

        if (array_key_exists('description', $data)) {
            $this->description = $data['description'];
        }
    }

    /**
     *
     * @param ThermomixAction $action
     */
    public function addAction(ThermomixAction $action)
    {
        $this->actions[] = $action;
    }

    /**
     * @param ThermomixAction $action
     */
    public function removeAction(ThermomixAction $action)
    {
        $this->actions->removeElement($action);
    }

    /**
     * @return array
     */
    public function toArray() : array
    {
        return [
            'id' => $this->id()->value(),
            'name' => $this->name(),
            'description' => $this->description()
        ];
    }

    /**
     * @return array
     */
    public function toArrayMinified() : array
    {
        return $this->toArray();
    }

    /**
     * @return array
     */
    public function jsonSerialize() : array
    {
        return $this->toArray();
    }
}
