<?php

namespace RecipeBook\Domain\Model\Unit;

final class UnitFinder
{
    /**
     * @var UnitRepositoryGatewayInterface
     */
    private $repository;

    /**
     * UnitFinder constructor.
     * @param UnitRepositoryGatewayInterface $repository
     */
    public function __construct(UnitRepositoryGatewayInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param array $filters
     * @param array $options
     * @return array
     */
    public function findBy(array $filters = [], array $options = []) : array
    {
        $units = $this->repository->filter($filters, $options);

        if ($units === null) {
            throw new UnitsNotFoundException($filters);
        }

        return $units;
    }

    /**
     * @param string $id
     * @return Unit
     */
    public function getById(string $id) : Unit
    {
        $unit = $this->repository->findOneBy(['id' => $id]);

        if ($unit === null) {
            throw new UnitNotFoundException($id);
        }

        return $unit;
    }

    /**
     * @param array $filters
     *
     * @return int
     */
    public function count(array $filters = []): int
    {
        return $this->repository->count($filters);
    }
}
