<?php

namespace RecipeBook\Domain\Model\Unit;

use RecipeBook\Domain\Common\DomainError;

final class UnitNotFoundException extends DomainError
{
    /**
     * @var string
     */
    private $id;

    /**
     * UnitNotFoundException constructor.
     * @param string $id
     */
    public function __construct(string $id)
    {
        $this->id = $id;

        parent::__construct();
    }

    /**
     * @return string
     */
    public function errorCode(): string
    {
        return 'unit_not_found';
    }

    /**
     * @return string
     */
    protected function errorMessage(): string
    {
        return sprintf('Unit has not found with this ID: %s', $this->id);
    }
}
