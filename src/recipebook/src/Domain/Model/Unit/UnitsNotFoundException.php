<?php

namespace RecipeBook\Domain\Model\Unit;

use RecipeBook\Domain\Common\DomainError;

final class UnitsNotFoundException extends DomainError
{
    /**
     * @var array
     */
    private $filters;

    /**
     * UnitsNotFoundException constructor.
     * @param array $filters
     */
    public function __construct(array $filters = [])
    {
        $this->filters = $filters;

        parent::__construct();
    }

    /**
     * @return string
     */
    public function errorCode(): string
    {
        return 'units_not_found';
    }

    /**
     * @return string
     */
    protected function errorMessage(): string
    {
        return sprintf('Units has not found with these filters: %s', json_encode($this->filters));
    }
}
