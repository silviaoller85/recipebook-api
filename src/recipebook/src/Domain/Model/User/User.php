<?php

namespace RecipeBook\Domain\Model\User;

use Doctrine\Common\Collections\ArrayCollection;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Hash;
use RecipeBook\Domain\Common\Timestamps;
use Tymon\JWTAuth\Contracts\JWTSubject;

/**
 * Class User
 * @package RecipeBook\Domain\Model\User
 */
class User extends Authenticatable implements JWTSubject, \JsonSerializable
{
    use Timestamps;

    /**
     * @var string
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $password;

    /**
     * @var boolean
     */
    private $isActive;

    /**
     * @var UserFavorite[]
     */
    private $favorites;

    /**
     * @var \DateTimeImmutable
     */
    private $createdAt;

    /**
     * @var null|\DateTimeImmutable
     */
    private $updatedAt;

    /**
     * User constructor.
     * @param string $id
     * @param string $name
     * @param string $email
     * @param string $password
     * @param bool $isActive
     * @throws \Exception
     */
    public function __construct(
        string $id,
        string $name,
        string $email,
        string $password,
        bool $isActive
    ) {
        $this->id = $id;
        $this->name = $name;
        $this->email = $email;
        $this->password = Hash::make($password);
        $this->isActive = $isActive;

        $this->favorites = new ArrayCollection();

        $this->setCreatedAtTime();
        $this->setUpdatedAtTime();
    }

    /**
     * @param string $id
     * @param string $name
     * @param string $email
     * @param string $password
     * @param bool $isActive
     * @return User
     * @throws \Exception
     */
    public static function create(
        string $id,
        string $name,
        string $email,
        string $password,
        bool $isActive
    ) {
        return new self($id, $name, $email, $password, $isActive);
    }

    /**
     * @return string
     */
    public function id(): string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function name(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function email(): string
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function password(): string
    {
        return $this->password;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->isActive;
    }


    public function favorites()
    {
        return $this->favorites;
    }

    /**
     * @param array $data
     */
    public function batchUpdate(array $data)
    {
        if (array_key_exists('name', $data)) {
            $this->name = $data['name'];
        }

        if (array_key_exists('email', $data)) {
            $this->email = $data['email'];
        }

        if (array_key_exists('password', $data)) {
            $this->password = $data['password'];
        }

        if (array_key_exists('isActive', $data)) {
            $this->isActive = $data['isActive'];
        }
    }
    /**
     * @return array
     */
    public function toArray() : array
    {
        $favorites = [];
        array_map(function (UserFavorite $userFavorite) use (&$favorites) {
            $favorites[] = $userFavorite->recipe()->toArray();
        }, $this->favorites()->toArray());

        return [
            'id' => $this->id(),
            'name' => $this->name(),
            'email' => $this->email(),
            'favorites' => $favorites
        ];
    }

    /**
     * @return array
     */
    public function toArrayMinified() : array
    {
        return [
            'id' => $this->id(),
            'name' => $this->name()
        ];
    }

    /**
     * @return array
     */
    public function toArrayLogin() : array
    {
        $favorites = [];
        array_map(function (UserFavorite $userFavorite) use (&$favorites) {
            $favorites[] = $userFavorite->recipe()->toArray();
        }, $this->favorites()->toArray());

        return [
            'id' => $this->id(),
            'name' => $this->name(),
            'email' => $this->email(),
            'favorites' => $favorites
        ];
    }

    /**
     * @return array|mixed
     */
    public function jsonSerialize()
    {
        return $this->toArray();
    }

    /**
     * Get the name of the unique identifier for the user.
     *
     * @return string
     */
    public function getAuthIdentifierName()
    {
        return 'id';
    }

    /**
     * Get the unique identifier for the user.
     *
     * @return mixed
     */
    public function getAuthIdentifier()
    {
        // TODO: Implement getAuthIdentifier() method.
    }

    /**
     * Get the password for the user.
     *
     * @return string
     */
    public function getAuthPassword()
    {
        return $this->password;
    }

    /**
     * Get the token value for the "remember me" session.
     *
     * @return string
     */
    public function getRememberToken()
    {
        // TODO: Implement getRememberToken() method.
    }

    /**
     * Set the token value for the "remember me" session.
     *
     * @param string $value
     * @return void
     */
    public function setRememberToken($value)
    {
        // TODO: Implement setRememberToken() method.
    }

    /**
     * Get the column name for the "remember me" token.
     *
     * @return string
     */
    public function getRememberTokenName()
    {
        // TODO: Implement getRememberTokenName() method.
    }

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->id();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
}
