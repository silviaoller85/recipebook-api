<?php

namespace RecipeBook\Domain\Model\User;

use RecipeBook\Domain\Common\DomainError;

final class UserFavoriteNotFoundException extends DomainError
{
    /**
     * @var string
     */
    private $id;

    /**
     * UserNotFoundException constructor.
     * @param string $id
     */
    public function __construct(string $id)
    {
        $this->id = $id;

        parent::__construct();
    }

    /**
     * @return string
     */
    public function errorCode(): string
    {
        return 'user_favorite_not_found';
    }

    /**
     * @return string
     */
    protected function errorMessage(): string
    {
        return sprintf('User favorite has not found with this ID: %s', $this->id);
    }
}
