<?php

namespace RecipeBook\Domain\Model\User;

use Illuminate\Support\Facades\Hash;

/**
 * Class UserFinder
 * @package RecipeBook\Domain\Model\User
 */
final class UserFinder
{
    /**
     * @var UserRepositoryGatewayInterface
     */
    private $repository;

    /**
     * UserFinder constructor.
     * @param UserRepositoryGatewayInterface $repository
     */
    public function __construct(UserRepositoryGatewayInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param array $filters
     * @param array $options
     * @return array
     */
    public function findBy(array $filters = [], array $options = []) : array
    {
        $users = $this->repository->filter($filters, $options);

        if ($users === null) {
            throw new UsersNotFoundException($filters);
        }

        return $users;
    }

    /**
     * @param string $id
     * @return User
     */
    public function getById(string $id) : User
    {
        $user = $this->repository->findOneBy(['id' => $id]);

        if ($user === null) {
            throw new UserNotFoundException($id);
        }

        return $user;
    }

    /**
     * @param array $filters
     *
     * @return int
     */
    public function count(array $filters = []): int
    {
        return $this->repository->count($filters);
    }

    /**
     * @param string $email
     * @param string $password
     * @return User
     */
    public function credentials(string $email, string $password) : User
    {
        $users = $this->repository->findBy(['email' => $email, 'isActive' => true]);
        if (!$users) {
            throw new UserNotFoundException('Credentials are incorrect');
        }

        /** @var User $user */
        $user = $users[0];

        if (Hash::check($password, $user->getAuthPassword())) {
            return $user;
        }

        throw new UserNotFoundException('Credentials are incorrect');
    }
}
