<?php

namespace RecipeBook\Domain\Model\User;

use RecipeBook\Domain\Common\DomainError;

final class UserFavoritesNotFoundException extends DomainError
{
    /**
     * @var array
     */
    private $filters;

    /**
     * UsersNotFoundException constructor.
     * @param array $filters
     */
    public function __construct(array $filters = [])
    {
        $this->filters = $filters;

        parent::__construct();
    }

    /**
     * @return string
     */
    public function errorCode(): string
    {
        return 'user_favorites_not_found';
    }

    /**
     * @return string
     */
    protected function errorMessage(): string
    {
        return sprintf('User favorites has not found with these filters: %s', json_encode($this->filters));
    }
}
