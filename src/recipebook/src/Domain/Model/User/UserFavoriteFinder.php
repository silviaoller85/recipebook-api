<?php

namespace RecipeBook\Domain\Model\User;

/**
 * Class UserFavoriteFinder
 * @package RecipeBook\Domain\Model\User
 */
final class UserFavoriteFinder
{
    /**
     * @var UserFavoriteRepositoryGatewayInterface
     */
    private $repository;

    /**
     * UserFinder constructor.
     * @param UserFavoriteRepositoryGatewayInterface $repository
     */
    public function __construct(UserFavoriteRepositoryGatewayInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param array $filters
     * @param array $options
     * @return array
     */
    public function findBy(array $filters = [], array $options = []) : array
    {
        $users = $this->repository->filter($filters, $options);

        if ($users === null) {
            throw new UserFavoritesNotFoundException($filters);
        }

        return $users;
    }

    /**
     * @param string $id
     * @return UserFavorite
     */
    public function getById(string $id) : UserFavorite
    {
        $user = $this->repository->findOneBy(['id' => $id]);

        if ($user === null) {
            throw new UserFavoriteNotFoundException($id);
        }

        return $user;
    }

    /**
     * @param array $filters
     *
     * @return int
     */
    public function count(array $filters = []): int
    {
        return $this->repository->count($filters);
    }
}
