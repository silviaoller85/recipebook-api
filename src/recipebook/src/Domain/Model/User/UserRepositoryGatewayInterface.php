<?php

namespace RecipeBook\Domain\Model\User;

use RecipeBook\Domain\Common\Repository\PersistenceGatewayInterface;
use RecipeBook\Domain\Common\Repository\SearchGatewayInterface;
use RecipeBook\Domain\Common\Repository\FilterGatewayInterface;

/**
 * Interface UserRepositoryGatewayInterface
 * @package RecipeBook\Domain\Model\User
 */
interface UserRepositoryGatewayInterface extends
    PersistenceGatewayInterface,
    SearchGatewayInterface,
    FilterGatewayInterface
{

    public function findOneById(string $id): ?User;
}
