<?php

namespace RecipeBook\Domain\Model\User;

use RecipeBook\Domain\Common\Timestamps;
use RecipeBook\Domain\Common\ValueObject\Uuid;
use RecipeBook\Domain\Model\Recipe\Recipe;

class UserFavorite
{
    use Timestamps;

    /**
     * @var Uuid
     */
    private $id;

    /**
     * @var User
     */
    private $user;

    /**
     * @var Recipe
     */
    private $recipe;

    /**
     * @var \DateTimeImmutable
     */
    private $createdAt;

    /**
     * @var null|\DateTimeImmutable
     */
    private $updatedAt;

    /**
     * UserFavorite constructor.
     * @param Uuid $id
     * @param User $user
     * @param Recipe $recipe
     * @throws \Exception
     */
    public function __construct(
        Uuid $id,
        User $user,
        Recipe $recipe
    ) {
        $this->id = $id;
        $this->user = $user;
        $this->recipe = $recipe;

        $this->setCreatedAtTime();
        $this->setUpdatedAtTime();
    }

    /**
     * @param Uuid $id
     * @param User $user
     * @param Recipe $recipe
     * @return UserFavorite
     * @throws \Exception
     */
    public static function create(
        Uuid $id,
        User $user,
        Recipe $recipe
    ) {
        return new self($id, $user, $recipe);
    }

    /**
     * @return Uuid
     */
    public function id(): Uuid
    {
        return $this->id;
    }

    /**
     * @return User
     */
    public function user() : User
    {
        return $this->user;
    }

    /**
     * @return Recipe
     */
    public function recipe() : Recipe
    {
        return $this->recipe;
    }

    /**
     * @return array
     */
    public function toArrayMinified() : array
    {
        return [
            'user' => $this->user()->toArrayMinified()
        ];
    }
}
