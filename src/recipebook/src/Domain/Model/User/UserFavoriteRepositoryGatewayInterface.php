<?php

namespace RecipeBook\Domain\Model\User;

use RecipeBook\Domain\Common\Repository\DeleteGatewayInterface;
use RecipeBook\Domain\Common\Repository\PersistenceGatewayInterface;
use RecipeBook\Domain\Common\Repository\SearchGatewayInterface;
use RecipeBook\Domain\Common\Repository\FilterGatewayInterface;

/**
 * Interface UserFavoriteRepositoryGatewayInterface
 * @package RecipeBook\Domain\Model\User
 */
interface UserFavoriteRepositoryGatewayInterface extends
    PersistenceGatewayInterface,
    SearchGatewayInterface,
    FilterGatewayInterface,
    DeleteGatewayInterface
{

    public function findOneById(string $id): ?User;
}
