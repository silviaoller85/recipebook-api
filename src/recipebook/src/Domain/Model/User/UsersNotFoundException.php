<?php

namespace RecipeBook\Domain\Model\User;

use RecipeBook\Domain\Common\DomainError;

final class UsersNotFoundException extends DomainError
{
    /**
     * @var array
     */
    private $filters;

    /**
     * UsersNotFoundException constructor.
     * @param array $filters
     */
    public function __construct(array $filters = [])
    {
        $this->filters = $filters;

        parent::__construct();
    }

    /**
     * @return string
     */
    public function errorCode(): string
    {
        return 'users_not_found';
    }

    /**
     * @return string
     */
    protected function errorMessage(): string
    {
        return sprintf('Users has not found with these filters: %s', json_encode($this->filters));
    }
}
