<?php

namespace RecipeBook\Domain\Model\Ingredient;

final class IngredientFinder
{
    /**
     * @var IngredientRepositoryGatewayInterface
     */
    private $repository;

    /**
     * IngredientFinder constructor.
     * @param IngredientRepositoryGatewayInterface $repository
     */
    public function __construct(IngredientRepositoryGatewayInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param array $filters
     * @param array $options
     * @return array
     */
    public function findBy(array $filters = [], array $options = []) : array
    {
        $ingredients = $this->repository->filter($filters, $options);

        if ($ingredients === null) {
            throw new IngredientsNotFoundException($filters);
        }

        return $ingredients;
    }

    /**
     * @param string $id
     * @return Ingredient
     */
    public function getById(string $id) : Ingredient
    {
        $ingredient = $this->repository->findOneBy(['id' => $id]);

        if ($ingredient === null) {
            throw new IngredientNotFoundException($id);
        }

        return $ingredient;
    }

    /**
     * @param array $filters
     *
     * @return int
     */
    public function count(array $filters = []): int
    {
        return $this->repository->count($filters);
    }
}
