<?php

namespace RecipeBook\Domain\Model\Ingredient;

use RecipeBook\Domain\Common\DomainError;

final class IngredientNotFoundException extends DomainError
{
    /**
     * @var string
     */
    private $id;

    /**
     * IngredientNotFoundException constructor.
     * @param string $id
     */
    public function __construct(string $id)
    {
        $this->id = $id;

        parent::__construct();
    }

    /**
     * @return string
     */
    public function errorCode(): string
    {
        return 'ingredient_not_found';
    }

    /**
     * @return string
     */
    protected function errorMessage(): string
    {
        return sprintf('Ingredient has not found with this ID: %s', $this->id);
    }
}
