<?php

namespace RecipeBook\Domain\Model\Ingredient;

use RecipeBook\Domain\Common\Repository\PersistenceGatewayInterface;
use RecipeBook\Domain\Common\Repository\SearchGatewayInterface;
use RecipeBook\Domain\Common\Repository\FilterGatewayInterface;

/**
 * Interface IngredientRepositoryGatewayInterface
 * @package RecipeBook\Domain\Model\Ingredient
 */
interface IngredientRepositoryGatewayInterface extends
    PersistenceGatewayInterface,
    SearchGatewayInterface,
    FilterGatewayInterface
{

    public function findOneById(string $id): ?Ingredient;
}
