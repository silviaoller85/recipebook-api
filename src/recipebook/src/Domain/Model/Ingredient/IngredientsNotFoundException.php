<?php

namespace RecipeBook\Domain\Model\Ingredient;

use RecipeBook\Domain\Common\DomainError;

final class IngredientsNotFoundException extends DomainError
{
    /**
     * @var array
     */
    private $filters;

    /**
     * IngredientsNotFoundException constructor.
     * @param array $filters
     */
    public function __construct(array $filters = [])
    {
        $this->filters = $filters;

        parent::__construct();
    }

    /**
     * @return string
     */
    public function errorCode(): string
    {
        return 'ingredients_not_found';
    }

    /**
     * @return string
     */
    protected function errorMessage(): string
    {
        return sprintf('Ingredients has not found with these filters: %s', json_encode($this->filters));
    }
}
