<?php

namespace RecipeBook\Domain\Model\Ingredient;

use RecipeBook\Domain\Common\Timestamps;
use RecipeBook\Domain\Common\ValueObject\Uuid;
use RecipeBook\Domain\Model\Unit\Unit;

class Ingredient
{
    use Timestamps;

    /**
     * @var Uuid
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $description;

    /**
     * @var Unit
     */
    private $defaultUnit;

    /**
     * @var \DateTimeImmutable
     */
    private $createdAt;

    /**
     * @var null|\DateTimeImmutable
     */
    private $updatedAt;


    /**
     * Ingredient constructor.
     * @param Uuid $id
     * @param string $name
     * @param string|null $description
     * @param Unit|null $defaultUnit
     * @throws \Exception
     */
    public function __construct(
        Uuid $id,
        string $name,
        string $description = null,
        Unit $defaultUnit = null
    ) {
        $this->id = $id;
        $this->name = $name;
        $this->description = $description;
        $this->defaultUnit = $defaultUnit;

        $this->setCreatedAtTime();
        $this->setUpdatedAtTime();
    }

    /**
     * @param Uuid $id
     * @param string $name
     * @param string|null $description
     * @param Unit|null $defaultUnit
     * @return Ingredient
     * @throws \Exception
     */
    public static function create(
        Uuid $id,
        string $name,
        string $description = null,
        Unit $defaultUnit = null
    ) {
        return new self($id, $name, $description, $defaultUnit);
    }

    /**
     * @return Uuid
     */
    public function id(): Uuid
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function name(): string
    {
        return $this->name;
    }

    /**
     * @return string|null
     */
    public function description(): ?string
    {
        return $this->description;
    }

    /**
     * @return null|Unit
     */
    public function defaultUnit() :?Unit
    {
        return $this->defaultUnit;
    }

    /**
     * @param array $data
     */
    public function batchUpdate(array $data)
    {
        if (array_key_exists('name', $data)) {
            $this->name = $data['name'];
        }

        if (array_key_exists('description', $data)) {
            $this->description = $data['description'];
        }

        if (array_key_exists('defaultUnit', $data)) {
            $this->defaultUnit = $data['defaultUnit'];
        }
    }

    /**
     * @return array
     */
    public function toArray() : array
    {
        $defaultUnit = $this->defaultUnit();

        return [
            'id' => $this->id()->value(),
            'name' => $this->name(),
            'description' => $this->description(),
            'defaultUnit' => $defaultUnit ? $defaultUnit->toArrayMinified() : null
        ];
    }

    /**
     * @return array
     */
    public function toArrayMinified() : array
    {
        return $this->toArray();
    }

    /**
     * @return array
     */
    public function jsonSerialize() : array
    {
        return $this->toArray();
    }
}
