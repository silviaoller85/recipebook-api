<?php

namespace RecipeBook\Domain\Model;

class File
{
    /**
     * @var string
     */
    private $path;

    /**
     * @var string
     */
    private $filename;

    /**
     * @var string
     */
    private $extension;

    /**
     * @var int
     */
    private $size;

    /**
     * @var null|string
     */
    private $mime;

    /**
     * File constructor.
     * @param string $path
     * @param string $filename
     * @param string $extension
     * @param int $size
     * @param string|null $mime
     */
    public function __construct(
        string $path,
        string $filename,
        string $extension,
        int $size,
        string $mime = null
    ) {
        $this->path = $path;
        $this->filename = $filename;
        $this->extension = $extension;
        $this->size = $size;
        $this->mime = $mime;
    }

    /**
     * @param array $data
     * @return File
     */
    public static function createFromArray(array $data) : self
    {
        return new self(
            $data['path'],
            $data['filename'],
            $data['extension'],
            $data['size'],
            $data['mime']
        );
    }

    /**
     * @return string
     */
    public function path() : string
    {
        return $this->path;
    }

    /**
     * @return string
     */
    public function filename() : string
    {
        return $this->filename;
    }

    /**
     * @return string
     */
    public function extension() : string
    {
        return $this->extension;
    }

    /**
     * @return int
     */
    public function size() : int
    {
        return $this->size;
    }

    /**
     * @return string
     */
    public function mime() : ?string
    {
        return $this->mime;
    }
}
