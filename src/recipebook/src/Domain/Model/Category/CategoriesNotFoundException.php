<?php

namespace RecipeBook\Domain\Model\Category;

use RecipeBook\Domain\Common\DomainError;

final class CategoriesNotFoundException extends DomainError
{
    /**
     * @var array
     */
    private $filters;

    /**
     * CategoriesNotFoundException constructor.
     * @param array $filters
     */
    public function __construct(array $filters = [])
    {
        $this->filters = $filters;

        parent::__construct();
    }

    /**
     * @return string
     */
    public function errorCode(): string
    {
        return 'categories_not_found';
    }

    /**
     * @return string
     */
    protected function errorMessage(): string
    {
        return sprintf('Categories has not found with these filters: %s', json_encode($this->filters));
    }
}
