<?php

namespace RecipeBook\Domain\Model\Category;

use RecipeBook\Domain\Common\DomainError;

final class CategoryNotFoundException extends DomainError
{
    /**
     * @var string
     */
    private $id;

    /**
     * CategoryNotFoundException constructor.
     * @param string $id
     */
    public function __construct(string $id)
    {
        $this->id = $id;

        parent::__construct();
    }

    /**
     * @return string
     */
    public function errorCode(): string
    {
        return 'category_not_found';
    }

    /**
     * @return string
     */
    protected function errorMessage(): string
    {
        return sprintf('Category has not found with this ID: %s', $this->id);
    }
}
