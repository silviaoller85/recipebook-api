<?php

namespace RecipeBook\Domain\Model\Category;

use Illuminate\Support\Str;
use RecipeBook\Domain\Common\Timestamps;
use RecipeBook\Domain\Common\ValueObject\Uuid;

class Category
{
    use Timestamps;

    /**
     * @var Uuid
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $slug;

    /**
     * @var \DateTimeImmutable
     */
    private $createdAt;

    /**
     * @var null|\DateTimeImmutable
     */
    private $updatedAt;


    /**
     * Category constructor.
     * @param Uuid $id
     * @param string $name
     * @param string $slug
     * @throws \Exception
     */
    public function __construct(
        Uuid $id,
        string $name,
        string $slug
    ) {
        $this->id = $id;
        $this->name = $name;
        $this->slug = $slug;

        $this->setCreatedAtTime();
        $this->setUpdatedAtTime();
    }

    /**
     * @param Uuid $id
     * @param string $name
     * @return Category
     * @throws \Exception
     */
    public static function create(
        Uuid $id,
        string $name
    ) {
        return new self($id, $name, Str::slug($name));
    }

    /**
     * @return Uuid
     */
    public function id(): Uuid
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function name(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function slug(): string
    {
        return $this->slug;
    }

    /**
     * @param array $data
     */
    public function batchUpdate(array $data)
    {
        if (array_key_exists('name', $data)) {
            $this->name = $data['name'];

            $this->slug = Str::slug($this->name);
        }
    }

    /**
     * @return array
     */
    public function toArray() : array
    {
        return [
            'id' => $this->id()->value(),
            'name' => $this->name(),
            'slug' => $this->slug(),
        ];
    }

    /**
     * @return array
     */
    public function toArrayMinified() : array
    {
        return $this->toArray();
    }

    /**
     * @return array
     */
    public function jsonSerialize() : array
    {
        return $this->toArray();
    }
}
