<?php

namespace RecipeBook\Domain\Model\Category;

final class CategoryFinder
{
    /**
     * @var CategoryRepositoryGatewayInterface
     */
    private $repository;

    /**
     * CategoryFinder constructor.
     * @param CategoryRepositoryGatewayInterface $repository
     */
    public function __construct(CategoryRepositoryGatewayInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param array $filters
     * @param array $options
     * @return array
     */
    public function findBy(array $filters = [], array $options = []) : array
    {
        $categories = $this->repository->filter($filters, $options);

        if ($categories === null) {
            throw new CategoriesNotFoundException($filters);
        }

        return $categories;
    }

    /**
     * @param string $id
     * @return Category
     */
    public function getById(string $id) : Category
    {
        $category = $this->repository->findOneBy(['id' => $id]);

        if ($category === null) {
            throw new CategoryNotFoundException($id);
        }

        return $category;
    }

    /**
     * @param array $filters
     *
     * @return int
     */
    public function count(array $filters = []): int
    {
        return $this->repository->count($filters);
    }
}
