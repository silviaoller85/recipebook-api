<?php

namespace RecipeBook\Domain\Model\Category;

use RecipeBook\Domain\Common\Repository\PersistenceGatewayInterface;
use RecipeBook\Domain\Common\Repository\SearchGatewayInterface;
use RecipeBook\Domain\Common\Repository\FilterGatewayInterface;

/**
 * Interface CategoryRepositoryGatewayInterface
 * @package RecipeBook\Domain\Model\Category
 */
interface CategoryRepositoryGatewayInterface extends
    PersistenceGatewayInterface,
    SearchGatewayInterface,
    FilterGatewayInterface
{

    public function findOneById(string $id): ?Category;
}
