<?php

namespace RecipeBook\Application\Service\Api\Category\Find;

use RecipeBook\Domain\Model\Category\CategoryFinder as DomainCategoryFinder;

final class CategoryFinder
{
    /**
     * @var DomainCategoryFinder
     */
    private $finder;

    /**
     * CategoryFinder constructor.
     * @param DomainCategoryFinder $finder
     */
    public function __construct(DomainCategoryFinder $finder)
    {
        $this->finder = $finder;
    }

    /**
     * @param array $filters
     * @param array $options
     * @return array
     */
    public function filterOrFail(array $filters = [], array $options = []) : array
    {
        return $this->finder->findBy($filters, $options);
    }
}
