<?php

namespace RecipeBook\Application\Service\Api\Category\Find;

use RecipeBook\Application\Common\Transformer\PaginateResponse;
use RecipeBook\Domain\Model\Category\Category;

final class CategoriesResponse
{
    use PaginateResponse;

    /**
     * @param array $categories
     * @param int $currentPage
     * @param int $itemsPerPage
     * @param int $total
     * @return array
     */
    public function transform(array $categories = [], int $currentPage, int $itemsPerPage, int $total) : array
    {
        $items = [];
        array_map(function (Category $category) use (&$items) {
            $items[] = $category->toArray();
        }, $categories);

        return $this->paginateTransform($items, $currentPage, $itemsPerPage, $total);
    }
}
