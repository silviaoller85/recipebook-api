<?php

namespace RecipeBook\Application\Service\Api\Category\Find;

final class FindCategoriesCommandHandler
{
    /**
     * @var CategoryFinder
     */
    private $finder;

    /**
     * FindCategoriesCommandHandler constructor.
     * @param CategoryFinder $finder
     */
    public function __construct(CategoryFinder $finder)
    {
        $this->finder = $finder;
    }

    /**
     * @param FindCategoriesCommand $command
     * @return array
     */
    public function handle(FindCategoriesCommand $command) : array
    {
        return $this->finder->filterOrFail($command->filters(), $command->options());
    }
}
