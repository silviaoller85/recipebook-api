<?php

namespace RecipeBook\Application\Service\Api\Category\Count;

use RecipeBook\Domain\Model\Category\CategoryFinder as DomainCategoryFinder;

final class CategoryCounter
{
    /**
     * @var DomainCategoryFinder
     */
    private $finder;

    /**
     * CategoryFinder constructor.
     * @param DomainCategoryFinder $finder
     */
    public function __construct(DomainCategoryFinder $finder)
    {
        $this->finder = $finder;
    }

    /**
     * @param array $filters
     * @return int
     */
    public function count(array $filters = []) : int
    {
        return $this->finder->count($filters);
    }
}
