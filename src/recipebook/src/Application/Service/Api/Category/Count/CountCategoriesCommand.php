<?php

namespace RecipeBook\Application\Service\Api\Category\Count;

final class CountCategoriesCommand
{
    /**
     * @var array
     */
    private $filters;

    /**
     * CountCategoriesCommand constructor.
     * @param array $filters
     */
    public function __construct(array $filters = [])
    {
        $this->filters = $filters;
    }

    /**
     * @return array
     */
    public function filters(): array
    {
        return $this->filters;
    }
}
