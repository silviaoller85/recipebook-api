<?php

namespace RecipeBook\Application\Service\Api\Category\Count;

final class CountCategoriesCommandHandler
{
    /**
     * @var CategoryCounter
     */
    private $counter;

    /**
     * CountCategoriesCommandHandler constructor.
     * @param CategoryCounter $counter
     */
    public function __construct(CategoryCounter $counter)
    {
        $this->counter = $counter;
    }

    /**
     * @param CountCategoriesCommand $command
     * @return int
     */
    public function handle(CountCategoriesCommand $command) : int
    {
        return $this->counter->count($command->filters());
    }
}
