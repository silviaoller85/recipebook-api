<?php

namespace RecipeBook\Application\Service\Api\Category\Get;

/**
 * Class GetCategoryCommand
 * @package RecipeBook\Application\Service\Api\Category\Get
 */
final class GetCategoryCommand
{
    /**
     * @var string
     */
    private $id;

    /**
     * GetCategoryCommand constructor.
     * @param string $id
     */
    public function __construct(string $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function id(): string
    {
        return $this->id;
    }
}
