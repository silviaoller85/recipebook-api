<?php

namespace RecipeBook\Application\Service\Api\Category\Get;

use RecipeBook\Domain\Model\Category\Category;
use RecipeBook\Domain\Model\Category\CategoryFinder as DomainCategoryFinder;

/**
 * Class CategoryGetter
 * @package RecipeBook\Application\Service\Api\Category\Get
 */
final class CategoryGetter
{
    /**
     * @var DomainCategoryFinder
     */
    private $finder;

    /**
     * CategoryGetter constructor.
     * @param DomainCategoryFinder $finder
     */
    public function __construct(DomainCategoryFinder $finder)
    {
        $this->finder = $finder;
    }

    /**
     * @param string $id
     * @return Category
     * @throws \RecipeBook\Domain\Model\Category\CategoryNotFoundException
     */
    public function getById(string $id) : Category
    {
        return $this->finder->getById($id);
    }
}
