<?php

namespace RecipeBook\Application\Service\Api\Category\Get;

use RecipeBook\Domain\Model\Category\Category;

/**
 * Class GetCategoryCommandHandler
 * @package RecipeBook\Application\Service\Api\Category\Get
 */
final class GetCategoryCommandHandler
{
    /**
     * @var CategoryGetter
     */
    private $getter;

    /**
     * GetCategoryCommandHandler constructor.
     * @param CategoryGetter $getter
     */
    public function __construct(CategoryGetter $getter)
    {
        $this->getter = $getter;
    }

    /**
     * @param GetCategoryCommand $command
     * @return Category
     */
    public function handle(GetCategoryCommand $command) : Category
    {
        return $this->getter->getById($command->id());
    }
}
