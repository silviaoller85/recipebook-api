<?php

namespace RecipeBook\Application\Service\Api\Category\Create;

use RecipeBook\Domain\Common\ValueObject\Uuid;
use RecipeBook\Domain\Model\Category\Category;

final class CreateCategoryCommandHandler
{
    /**
     * @var CategoryCreator
     */
    private $creator;

    /**
     * CreateCategoryCommandHandler constructor.
     * @param CategoryCreator $creator
     */
    public function __construct(CategoryCreator $creator)
    {
        $this->creator = $creator;
    }

    /**
     * @param CreateCategoryCommand $command
     * @return Category
     * @throws \Exception
     */
    public function handle(CreateCategoryCommand $command) : Category
    {
        return $this->creator->create(new Uuid($command->id()), $command->name());
    }
}
