<?php

namespace RecipeBook\Application\Service\Api\Category\Create;

final class CreateCategoryCommand
{
    /**
     * @var string
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @param string $id
     * @param string $name
     */
    public function __construct(string $id, string $name)
    {
        $this->id = $id;
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function id(): string
    {
        return $this->id;
    }

    /**
     * @return null|string
     */
    public function name(): ?string
    {
        return $this->name;
    }
}
