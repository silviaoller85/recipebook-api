<?php

namespace RecipeBook\Application\Service\Api\Category\Create;

use RecipeBook\Domain\Common\ValueObject\Uuid;
use RecipeBook\Domain\Model\Category\Category;
use RecipeBook\Domain\Model\Category\CategoryRepositoryGatewayInterface;

final class CategoryCreator
{
    /**
     * @var CategoryRepositoryGatewayInterface
     */
    private $repository;

    /**
     * CategoryCreator constructor.
     * @param CategoryRepositoryGatewayInterface $repository
     */
    public function __construct(CategoryRepositoryGatewayInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param Uuid $id
     * @param string $name
     * @return Category
     * @throws \Exception
     */
    public function create(
        Uuid $id,
        string $name
    ) : Category {
        $category = Category::create($id, $name);

        $this->repository->persist($category);

        return $category;
    }
}
