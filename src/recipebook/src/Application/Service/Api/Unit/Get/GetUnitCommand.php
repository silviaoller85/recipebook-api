<?php

namespace RecipeBook\Application\Service\Api\Unit\Get;

/**
 * Class GetUnitCommand
 * @package Recipe\Application\Service\Api\Unit\Get
 */
final class GetUnitCommand
{
    /**
     * @var string
     */
    private $id;

    /**
     * GetUnitCommand constructor.
     * @param string $id
     */
    public function __construct(string $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function id(): string
    {
        return $this->id;
    }
}
