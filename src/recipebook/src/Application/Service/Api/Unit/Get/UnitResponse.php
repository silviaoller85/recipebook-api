<?php

namespace RecipeBook\Application\Service\Api\Unit\Get;

use RecipeBook\Domain\Model\Unit\Unit;

/**
 * Class UnitResponse
 * @package RecipeBook\Application\Service\Api\Recipe\Get
 */
final class UnitResponse
{
    /**
     * @param Unit $unit
     * @return array
     */
    public function transform(Unit $unit) : array
    {
        return $unit->toArray();
    }
}
