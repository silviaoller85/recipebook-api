<?php

namespace RecipeBook\Application\Service\Api\Unit\Get;

use RecipeBook\Domain\Model\Unit\Unit;

/**
 * Class GetUnitCommandHandler
 * @package RecipeBook\Application\Service\Api\Unit\Get
 */
final class GetUnitCommandHandler
{
    /**
     * @var UnitGetter
     */
    private $getter;

    /**
     * GetUnitCommandHandler constructor.
     * @param UnitGetter $getter
     */
    public function __construct(UnitGetter $getter)
    {
        $this->getter = $getter;
    }

    /**
     * @param GetUnitCommand $command
     * @return Unit
     */
    public function handle(GetUnitCommand $command) : Unit
    {
        return $this->getter->getById($command->id());
    }
}
