<?php

namespace RecipeBook\Application\Service\Api\Unit\Get;

use RecipeBook\Domain\Model\Unit\Unit;
use RecipeBook\Domain\Model\Unit\UnitFinder as DomainUnitFinder;

/**
 * Class UnitGetter
 * @package RecipeBook\Application\Service\Api\Unit\Get
 */
final class UnitGetter
{
    /**
     * @var DomainUnitFinder
     */
    private $finder;

    /**
     * UnitGetter constructor.
     * @param DomainUnitFinder $finder
     */
    public function __construct(DomainUnitFinder $finder)
    {
        $this->finder = $finder;
    }

    /**
     * @param string $id
     * @return Unit
     * @throws \RecipeBook\Domain\Model\Unit\UnitNotFoundException
     */
    public function getById(string $id) : Unit
    {
        return $this->finder->getById($id);
    }
}
