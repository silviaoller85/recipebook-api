<?php

namespace RecipeBook\Application\Service\Api\Unit\Create;

use RecipeBook\Domain\Common\ValueObject\Uuid;
use RecipeBook\Domain\Model\Unit\Unit;

final class CreateUnitCommandHandler
{
    /**
     * @var UnitCreator
     */
    private $creator;

    /**
     * CreateUnitCommandHandler constructor.
     * @param UnitCreator $creator
     */
    public function __construct(UnitCreator $creator)
    {
        $this->creator = $creator;
    }

    /**
     * @param CreateUnitCommand $command
     * @return Unit
     * @throws \Exception
     */
    public function handle(CreateUnitCommand $command) : Unit
    {
        return $this->creator->create(new Uuid($command->id()), $command->name(), $command->description());
    }
}
