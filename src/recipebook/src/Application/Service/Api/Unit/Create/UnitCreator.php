<?php

namespace RecipeBook\Application\Service\Api\Unit\Create;

use RecipeBook\Domain\Common\ValueObject\Uuid;
use RecipeBook\Domain\Model\Unit\Unit;
use RecipeBook\Domain\Model\Unit\UnitRepositoryGatewayInterface;

final class UnitCreator
{
    /**
     * @var UnitRepositoryGatewayInterface
     */
    private $repository;

    /**
     * UnitCreator constructor.
     * @param UnitRepositoryGatewayInterface $repository
     */
    public function __construct(UnitRepositoryGatewayInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param Uuid $id
     * @param string $name
     * @param string|null $description
     * @return Unit
     * @throws \Exception
     */
    public function create(
        Uuid $id,
        string $name,
        string $description = null
    ) : Unit {
        $unit = Unit::create($id, $name, $description);

        $this->repository->persist($unit);

        return $unit;
    }
}
