<?php

namespace RecipeBook\Application\Service\Api\Unit\Find;

final class FindUnitsCommandHandler
{
    /**
     * @var UnitFinder
     */
    private $finder;

    /**
     * FindUnitsCommandHandler constructor.
     * @param UnitFinder $finder
     */
    public function __construct(UnitFinder $finder)
    {
        $this->finder = $finder;
    }

    /**
     * @param FindUnitsCommand $command
     * @return array
     */
    public function handle(FindUnitsCommand $command) : array
    {
        return $this->finder->filterOrFail($command->filters(), $command->options());
    }
}
