<?php

namespace RecipeBook\Application\Service\Api\Unit\Find;

use RecipeBook\Application\Common\Transformer\PaginateResponse;
use RecipeBook\Domain\Model\Unit\Unit;

final class UnitsResponse
{
    use PaginateResponse;

    /**
     * @param array $units
     * @param int $currentPage
     * @param int $itemsPerPage
     * @param int $total
     * @return array
     */
    public function transform(array $units = [], int $currentPage, int $itemsPerPage, int $total) : array
    {
        $items = [];
        array_map(function (Unit $unit) use (&$items) {
            $items[] = $unit->toArray();
        }, $units);

        return $this->paginateTransform($items, $currentPage, $itemsPerPage, $total);
    }
}
