<?php

namespace RecipeBook\Application\Service\Api\Unit\Find;

use RecipeBook\Domain\Model\Unit\UnitFinder as DomainUnitFinder;

final class UnitFinder
{
    /**
     * @var DomainUnitFinder
     */
    private $finder;

    /**
     * UnitFinder constructor.
     * @param DomainUnitFinder $finder
     */
    public function __construct(DomainUnitFinder $finder)
    {
        $this->finder = $finder;
    }

    /**
     * @param array $filters
     * @param array $options
     * @return array
     */
    public function filterOrFail(array $filters = [], array $options = []) : array
    {
        return $this->finder->findBy($filters, $options);
    }
}
