<?php

namespace RecipeBook\Application\Service\Api\Unit\Count;

final class CountUnitsCommand
{
    /**
     * @var array
     */
    private $filters;

    /**
     * CountUnitsCommand constructor.
     * @param array $filters
     */
    public function __construct(array $filters = [])
    {
        $this->filters = $filters;
    }

    /**
     * @return array
     */
    public function filters(): array
    {
        return $this->filters;
    }
}
