<?php

namespace RecipeBook\Application\Service\Api\Unit\Count;

use RecipeBook\Domain\Model\Unit\UnitFinder as DomainUnitFinder;

final class UnitCounter
{
    /**
     * @var DomainUnitFinder
     */
    private $finder;

    /**
     * UnitFinder constructor.
     * @param DomainUnitFinder $finder
     */
    public function __construct(DomainUnitFinder $finder)
    {
        $this->finder = $finder;
    }

    /**
     * @param array $filters
     * @return int
     */
    public function count(array $filters = []) : int
    {
        return $this->finder->count($filters);
    }
}
