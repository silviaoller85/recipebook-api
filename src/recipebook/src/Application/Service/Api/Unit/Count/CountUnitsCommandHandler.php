<?php

namespace RecipeBook\Application\Service\Api\Unit\Count;

final class CountUnitsCommandHandler
{
    /**
     * @var UnitCounter
     */
    private $counter;

    /**
     * CountUnitsCommandHandler constructor.
     * @param UnitCounter $counter
     */
    public function __construct(UnitCounter $counter)
    {
        $this->counter = $counter;
    }

    /**
     * @param CountUnitsCommand $command
     * @return int
     */
    public function handle(CountUnitsCommand $command) : int
    {
        return $this->counter->count($command->filters());
    }
}
