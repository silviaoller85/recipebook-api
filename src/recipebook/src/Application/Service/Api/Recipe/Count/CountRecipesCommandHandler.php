<?php

namespace RecipeBook\Application\Service\Api\Recipe\Count;

final class CountRecipesCommandHandler
{
    /**
     * @var RecipeCounter
     */
    private $counter;

    /**
     * CountRecipesCommandHandler constructor.
     * @param RecipeCounter $counter
     */
    public function __construct(RecipeCounter $counter)
    {
        $this->counter = $counter;
    }

    /**
     * @param CountRecipesCommand $command
     * @return int
     */
    public function handle(CountRecipesCommand $command) : int
    {
        return $this->counter->count($command->filters());
    }
}
