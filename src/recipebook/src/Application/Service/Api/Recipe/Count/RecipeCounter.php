<?php

namespace RecipeBook\Application\Service\Api\Recipe\Count;

use RecipeBook\Domain\Model\Recipe\RecipeFinder as DomainRecipeFinder;

final class RecipeCounter
{
    /**
     * @var DomainRecipeFinder
     */
    private $finder;

    /**
     * RecipeFinder constructor.
     * @param DomainRecipeFinder $finder
     */
    public function __construct(DomainRecipeFinder $finder)
    {
        $this->finder = $finder;
    }

    /**
     * @param array $filters
     * @return int
     */
    public function count(array $filters = []) : int
    {
        return $this->finder->count($filters);
    }
}
