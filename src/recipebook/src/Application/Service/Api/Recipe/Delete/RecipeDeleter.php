<?php

namespace RecipeBook\Application\Service\Api\Recipe\Delete;

use RecipeBook\Domain\Model\Recipe\Recipe;
use RecipeBook\Domain\Model\Recipe\RecipeRepositoryGatewayInterface;

final class RecipeDeleter
{
    /**
     * @var RecipeRepositoryGatewayInterface
     */
    private $repository;

    /**
     * RecipeDeleter constructor.
     * @param RecipeRepositoryGatewayInterface $repository
     */
    public function __construct(RecipeRepositoryGatewayInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param Recipe $recipe
     * @return bool
     */
    public function delete(Recipe $recipe)
    {
        $this->repository->delete($recipe);

        return true;
    }
}
