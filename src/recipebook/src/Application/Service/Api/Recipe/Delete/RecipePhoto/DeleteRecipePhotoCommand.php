<?php

namespace RecipeBook\Application\Service\Api\Recipe\Delete\RecipePhoto;

use RecipeBook\Domain\Model\Recipe\RecipePhoto\RecipePhoto;

final class DeleteRecipePhotoCommand
{
    /**
     * @var RecipePhoto
     */
    private $recipePhoto;


    /**
     * @param RecipePhoto $recipePhoto
     */
    public function __construct(
        RecipePhoto $recipePhoto
    ) {
        $this->recipePhoto = $recipePhoto;
    }

    /**
     * @return RecipePhoto
     */
    public function recipePhoto(): RecipePhoto
    {
        return $this->recipePhoto;
    }
}
