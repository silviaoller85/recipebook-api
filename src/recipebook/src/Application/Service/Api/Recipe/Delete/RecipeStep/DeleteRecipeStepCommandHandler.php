<?php

namespace RecipeBook\Application\Service\Api\Recipe\Delete\RecipeStep;

final class DeleteRecipeStepCommandHandler
{
    /**
     * @var RecipeStepDeleter
     */
    private $deleter;

    /**
     * DeleteRecipeCommandHandler constructor.
     * @param RecipeStepDeleter $deleter
     */
    public function __construct(RecipeStepDeleter $deleter)
    {
        $this->deleter = $deleter;
    }

    /**
     * @param DeleteRecipeStepCommand $command
     * @return bool
     */
    public function handle(DeleteRecipeStepCommand $command) : bool
    {
        return $this->deleter->delete($command->recipeStep());
    }
}
