<?php

namespace RecipeBook\Application\Service\Api\Recipe\Delete;

final class DeleteRecipeCommandHandler
{
    /**
     * @var RecipeDeleter
     */
    private $creator;

    /**
     * DeleteRecipeCommandHandler constructor.
     * @param RecipeDeleter $creator
     */
    public function __construct(RecipeDeleter $creator)
    {
        $this->creator = $creator;
    }

    /**
     * @param DeleteRecipeCommand $command
     * @return bool
     */
    public function handle(DeleteRecipeCommand $command) : bool
    {
        return $this->creator->delete($command->recipe());
    }
}
