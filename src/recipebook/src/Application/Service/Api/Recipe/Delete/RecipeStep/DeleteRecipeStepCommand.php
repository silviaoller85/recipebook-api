<?php

namespace RecipeBook\Application\Service\Api\Recipe\Delete\RecipeStep;

use RecipeBook\Domain\Model\Recipe\RecipeStep\RecipeStep;

final class DeleteRecipeStepCommand
{
    /**
     * @var RecipeStep
     */
    private $recipeStep;


    /**
     * @param RecipeStep $recipeStep
     */
    public function __construct(
        RecipeStep $recipeStep
    ) {
        $this->recipeStep = $recipeStep;
    }

    /**
     * @return RecipeStep
     */
    public function recipeStep(): RecipeStep
    {
        return $this->recipeStep;
    }
}
