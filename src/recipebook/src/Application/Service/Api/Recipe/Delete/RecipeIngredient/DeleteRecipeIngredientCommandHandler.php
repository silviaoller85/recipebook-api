<?php

namespace RecipeBook\Application\Service\Api\Recipe\Delete\RecipeIngredient;

final class DeleteRecipeIngredientCommandHandler
{
    /**
     * @var RecipeIngredientDeleter
     */
    private $deleter;

    /**
     * DeleteRecipeCommandHandler constructor.
     * @param RecipeIngredientDeleter $deleter
     */
    public function __construct(RecipeIngredientDeleter $deleter)
    {
        $this->deleter = $deleter;
    }

    /**
     * @param DeleteRecipeIngredientCommand $command
     * @return bool
     */
    public function handle(DeleteRecipeIngredientCommand $command) : bool
    {
        return $this->deleter->delete($command->recipeIngredient());
    }
}
