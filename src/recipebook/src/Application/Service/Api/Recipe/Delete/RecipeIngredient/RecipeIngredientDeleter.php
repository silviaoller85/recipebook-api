<?php

namespace RecipeBook\Application\Service\Api\Recipe\Delete\RecipeIngredient;

use RecipeBook\Domain\Model\Recipe\RecipeIngredient\RecipeIngredient;
use RecipeBook\Domain\Model\Recipe\RecipeIngredient\RecipeIngredientRepositoryGatewayInterface;

final class RecipeIngredientDeleter
{
    /**
     * @var RecipeIngredientRepositoryGatewayInterface
     */
    private $repository;

    /**
     * RecipeDeleter constructor.
     * @param RecipeIngredientRepositoryGatewayInterface $repository
     */
    public function __construct(RecipeIngredientRepositoryGatewayInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param RecipeIngredient $recipeIngredient
     * @return bool
     */
    public function delete(RecipeIngredient $recipeIngredient)
    {
        $this->repository->delete($recipeIngredient);

        return true;
    }
}
