<?php

namespace RecipeBook\Application\Service\Api\Recipe\Delete;

use RecipeBook\Domain\Model\Recipe\Recipe;

final class DeleteRecipeCommand
{
    /**
     * @var Recipe
     */
    private $recipe;

    /**
     * @param Recipe $recipe
     */
    public function __construct(Recipe $recipe)
    {
        $this->recipe = $recipe;
    }

    /**
     * @return Recipe
     */
    public function recipe(): Recipe
    {
        return $this->recipe;
    }
}
