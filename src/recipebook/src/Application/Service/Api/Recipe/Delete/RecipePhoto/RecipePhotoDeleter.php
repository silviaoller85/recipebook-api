<?php

namespace RecipeBook\Application\Service\Api\Recipe\Delete\RecipePhoto;

use RecipeBook\Domain\Model\Recipe\RecipePhoto\RecipePhoto;
use RecipeBook\Domain\Model\Recipe\RecipePhoto\RecipePhotoRepositoryGatewayInterface;
use RecipeBook\Infrastructure\Storage\FileUploader;

final class RecipePhotoDeleter
{
    /**
     * @var RecipePhotoRepositoryGatewayInterface
     */
    private $repository;

    /**
     * @var FileUploader
     */
    private $uploader;

    /**
     * RecipeDeleter constructor.
     * @param RecipePhotoRepositoryGatewayInterface $repository
     * @param FileUploader $uploader
     */
    public function __construct(
        RecipePhotoRepositoryGatewayInterface $repository,
        FileUploader $uploader
    ) {
        $this->repository = $repository;
        $this->uploader = $uploader;
    }

    /**
     * @param RecipePhoto $recipePhoto
     * @return bool
     */
    public function delete(RecipePhoto $recipePhoto)
    {
        $this->repository->delete($recipePhoto);

        $this->uploader->delete($recipePhoto->path());

        return true;
    }
}
