<?php

namespace RecipeBook\Application\Service\Api\Recipe\Delete\RecipeIngredient;

use RecipeBook\Domain\Model\Recipe\RecipeIngredient\RecipeIngredient;

final class DeleteRecipeIngredientCommand
{
    /**
     * @var RecipeIngredient
     */
    private $recipeIngredient;


    /**
     * @param RecipeIngredient $recipeIngredient
     */
    public function __construct(
        RecipeIngredient $recipeIngredient
    ) {
        $this->recipeIngredient = $recipeIngredient;
    }

    /**
     * @return RecipeIngredient
     */
    public function recipeIngredient(): RecipeIngredient
    {
        return $this->recipeIngredient;
    }
}
