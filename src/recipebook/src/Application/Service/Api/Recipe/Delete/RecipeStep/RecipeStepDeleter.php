<?php

namespace RecipeBook\Application\Service\Api\Recipe\Delete\RecipeStep;

use RecipeBook\Domain\Model\Recipe\RecipeStep\RecipeStep;
use RecipeBook\Domain\Model\Recipe\RecipeStep\RecipeStepRepositoryGatewayInterface;

final class RecipeStepDeleter
{
    /**
     * @var RecipeStepRepositoryGatewayInterface
     */
    private $repository;

    /**
     * RecipeDeleter constructor.
     * @param RecipeStepRepositoryGatewayInterface $repository
     */
    public function __construct(RecipeStepRepositoryGatewayInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param RecipeStep $recipeStep
     * @return bool
     */
    public function delete(RecipeStep $recipeStep)
    {
        $this->repository->delete($recipeStep);

        return true;
    }
}
