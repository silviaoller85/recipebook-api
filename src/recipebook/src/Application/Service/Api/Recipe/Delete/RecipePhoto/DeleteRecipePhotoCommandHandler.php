<?php

namespace RecipeBook\Application\Service\Api\Recipe\Delete\RecipePhoto;

final class DeleteRecipePhotoCommandHandler
{
    /**
     * @var RecipePhotoDeleter
     */
    private $deleter;

    /**
     * DeleteRecipeCommandHandler constructor.
     * @param RecipePhotoDeleter $deleter
     */
    public function __construct(RecipePhotoDeleter $deleter)
    {
        $this->deleter = $deleter;
    }

    /**
     * @param DeleteRecipePhotoCommand $command
     * @return bool
     */
    public function handle(DeleteRecipePhotoCommand $command) : bool
    {
        return $this->deleter->delete($command->recipePhoto());
    }
}
