<?php

namespace RecipeBook\Application\Service\Api\Recipe\Update\RecipeStep;

use RecipeBook\Domain\Common\ValueObject\Uuid;
use RecipeBook\Domain\Model\Recipe\RecipeStep\RecipeStep;

final class UpdateRecipeStepCommandHandler
{
    /**
     * @var RecipeStepUpdater
     */
    private $updater;

    /**
     * UpdateRecipeCommandHandler constructor.
     * @param RecipeStepUpdater $updater
     */
    public function __construct(RecipeStepUpdater $updater)
    {
        $this->updater = $updater;
    }

    /**
     * @param UpdateRecipeStepCommand $command
     * @return RecipeStep
     * @throws \Exception
     */
    public function handle(UpdateRecipeStepCommand $command) : RecipeStep
    {
        return $this->updater->update(
            new Uuid($command->id()),
            $command->number(),
            $command->name(),
            $command->description(),
            $command->recipe()
        );
    }
}
