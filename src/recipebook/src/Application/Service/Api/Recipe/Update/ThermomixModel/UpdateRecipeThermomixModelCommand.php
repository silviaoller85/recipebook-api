<?php

namespace RecipeBook\Application\Service\Api\Recipe\Update\ThermomixModel;

use RecipeBook\Domain\Model\Recipe\Recipe;

final class UpdateRecipeThermomixModelCommand
{
    /**
     * @var Recipe
     */
    private $recipe;

    /**
     * @var array
     */
    private $thermomixModels;

    /**
     * UpdateRecipeThermomixModelCommand constructor.
     * @param Recipe $recipe
     * @param array $thermomixModels
     */
    public function __construct(
        Recipe $recipe,
        array $thermomixModels
    ) {
        $this->recipe = $recipe;
        $this->thermomixModels = $thermomixModels;
    }

    /**
     * @return Recipe
     */
    public function recipe() : Recipe
    {
        return $this->recipe;
    }

    /**
     * @return array
     */
    public function thermomixModels() : array
    {
        return $this->thermomixModels;
    }
}
