<?php

namespace RecipeBook\Application\Service\Api\Recipe\Update\RecipeStep;

use RecipeBook\Domain\Common\ValueObject\Uuid;
use RecipeBook\Domain\Model\Recipe\Recipe;
use RecipeBook\Domain\Model\Recipe\RecipeStep\RecipeStep;
use RecipeBook\Domain\Model\Recipe\RecipeStep\RecipeStepNotFoundException;
use RecipeBook\Domain\Model\Recipe\RecipeStep\RecipeStepRepositoryGatewayInterface;

final class RecipeStepUpdater
{
    /**
     * @var RecipeStepRepositoryGatewayInterface
     */
    private $repository;

    /**
     * RecipeUpdater constructor.
     * @param RecipeStepRepositoryGatewayInterface $repository
     */
    public function __construct(RecipeStepRepositoryGatewayInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param Uuid $id
     * @param int $number
     * @param string|null $name
     * @param string $description
     * @param Recipe $recipe
     * @return RecipeStep
     */
    public function update(
        Uuid $id,
        int $number,
        string $name = null,
        string $description,
        Recipe $recipe
    ) : RecipeStep {

        $recipeStep = $this->repository->findOneById($id->value());

        if ($recipeStep === null) {
            throw new RecipeStepNotFoundException($id->value());
        }

        if ($recipeStep->recipe()->id() !== $recipe->id()) {
            throw new RecipeStepNotFoundException($recipe->id());
        }

        $data = [
            'number' => $number,
            'name' => $name,
            'description' => $description
        ];

        $recipeStep->batchUpdate($data);

        return $recipeStep;
    }
}
