<?php

namespace RecipeBook\Application\Service\Api\Recipe\Update\Category;

use RecipeBook\Domain\Model\Category\Category;
use RecipeBook\Domain\Model\Recipe\Recipe;
use RecipeBook\Domain\Model\Recipe\RecipeRepositoryGatewayInterface;

final class RecipeCategoryUpdater
{
    /**
     * @var RecipeRepositoryGatewayInterface
     */
    private $repository;

    /**
     * RecipeCategoryUpdater constructor.
     * @param RecipeRepositoryGatewayInterface $repository
     */
    public function __construct(RecipeRepositoryGatewayInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param Recipe $recipe
     * @param array $categories
     * @return Recipe
     */
    public function update(Recipe $recipe, array $categories)
    {
        $recipe->removeAllCategories();

        array_map(function (Category $category) use (&$recipe) {
            $recipe->addCategory($category);
        }, $categories);

        return $recipe;
    }
}
