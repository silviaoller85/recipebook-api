<?php

namespace RecipeBook\Application\Service\Api\Recipe\Update;

use RecipeBook\Domain\Common\ValueObject\Uuid;
use RecipeBook\Domain\Model\Recipe\Recipe;

final class UpdateRecipeCommandHandler
{
    /**
     * @var RecipeUpdater
     */
    private $updater;

    /**
     * UpdateRecipeCommandHandler constructor.
     * @param RecipeUpdater $updater
     */
    public function __construct(RecipeUpdater $updater)
    {
        $this->updater = $updater;
    }

    /**
     * @param UpdateRecipeCommand $command
     * @return Recipe
     * @throws \Exception
     */
    public function handle(UpdateRecipeCommand $command) : Recipe
    {
        return $this->updater->update(
            new Uuid($command->id()),
            $command->name(),
            $command->description(),
            $command->user(),
            $command->preparationTime(),
            $command->cookingTime(),
            $command->serves(),
            $command->isThermomix()
        );
    }
}
