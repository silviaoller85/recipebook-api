<?php

namespace RecipeBook\Application\Service\Api\Recipe\Update\ThermomixModel;

use RecipeBook\Domain\Model\Thermomix\ThermomixModel\ThermomixModel;
use RecipeBook\Domain\Model\Recipe\Recipe;
use RecipeBook\Domain\Model\Recipe\RecipeRepositoryGatewayInterface;

final class RecipeThermomixModelUpdater
{
    /**
     * @var RecipeRepositoryGatewayInterface
     */
    private $repository;

    /**
     * RecipeThermomixModelUpdater constructor.
     * @param RecipeRepositoryGatewayInterface $repository
     */
    public function __construct(RecipeRepositoryGatewayInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param Recipe $recipe
     * @param array $thermomixModels
     * @return Recipe
     */
    public function update(Recipe $recipe, array $thermomixModels)
    {
        $recipe->removeAllThermomixModels();

        array_map(function (ThermomixModel $thermomixModel) use (&$recipe) {
            $recipe->addThermomixModel($thermomixModel);
        }, $thermomixModels);

        return $recipe;
    }
}
