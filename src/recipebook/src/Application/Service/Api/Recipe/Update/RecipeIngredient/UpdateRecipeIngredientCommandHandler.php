<?php

namespace RecipeBook\Application\Service\Api\Recipe\Update\RecipeIngredient;

use RecipeBook\Domain\Common\ValueObject\Uuid;
use RecipeBook\Domain\Model\Recipe\RecipeIngredient\RecipeIngredient;

final class UpdateRecipeIngredientCommandHandler
{
    /**
     * @var RecipeIngredientUpdater
     */
    private $updater;

    /**
     * UpdateRecipeCommandHandler constructor.
     * @param RecipeIngredientUpdater $updater
     */
    public function __construct(RecipeIngredientUpdater $updater)
    {
        $this->updater = $updater;
    }

    /**
     * @param UpdateRecipeIngredientCommand $command
     * @return RecipeIngredient
     * @throws \Exception
     */
    public function handle(UpdateRecipeIngredientCommand $command) : RecipeIngredient
    {
        return $this->updater->update(
            new Uuid($command->id()),
            $command->recipe(),
            $command->ingredient(),
            $command->value(),
            $command->unit()
        );
    }
}
