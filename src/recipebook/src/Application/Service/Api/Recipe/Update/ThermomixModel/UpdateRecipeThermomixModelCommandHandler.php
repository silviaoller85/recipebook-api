<?php

namespace RecipeBook\Application\Service\Api\Recipe\Update\ThermomixModel;

final class UpdateRecipeThermomixModelCommandHandler
{
    /**
     * @var RecipeThermomixModelUpdater
     */
    private $updater;

    /**
     * UpdateRecipeThermomixModelCommandHandler constructor.
     * @param RecipeThermomixModelUpdater $updater
     */
    public function __construct(RecipeThermomixModelUpdater $updater)
    {
        $this->updater = $updater;
    }

    /**
     * @param UpdateRecipeThermomixModelCommand $command
     * @return mixed
     */
    public function handle(UpdateRecipeThermomixModelCommand $command)
    {
        return $this->updater->update($command->recipe(), $command->thermomixModels());
    }
}
