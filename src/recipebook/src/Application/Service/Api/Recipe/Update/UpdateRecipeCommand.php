<?php

namespace RecipeBook\Application\Service\Api\Recipe\Update;

use RecipeBook\Domain\Model\Category\Category;
use RecipeBook\Domain\Model\User\User;

final class UpdateRecipeCommand
{
    /**
     * @var string
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var null|string
     */
    private $description;

    /**ç
     * @var User
     */
    private $user;

    /**
     * @var float
     */
    private $preparationTime;

    /**
     * @var float
     */
    private $cookingTime;

    /**
     * @var int
     */
    private $serves;

    /**
     * @var bool
     */
    private $isThermomix;

    /**
     * @param string $id
     * @param string $name
     * @param string|null $description
     * @param User $user
     * @param float $preparationTime
     * @param float $cookingTime
     * @param int $serves
     * @param bool $isThermomix
     */
    public function __construct(
        string $id,
        string $name,
        string $description = null,
        User $user,
        float $preparationTime,
        float $cookingTime,
        int $serves,
        bool $isThermomix
    ) {
        $this->id = $id;
        $this->name = $name;
        $this->description = $description;
        $this->user = $user;
        $this->preparationTime = $preparationTime;
        $this->cookingTime = $cookingTime;
        $this->serves = $serves;
        $this->isThermomix = $isThermomix;
    }

    /**
     * @return string
     */
    public function id(): string
    {
        return $this->id;
    }

    /**
     * @return null|string
     */
    public function name(): ?string
    {
        return $this->name;
    }

    /**
     * @return null|string
     */
    public function description(): ?string
    {
        return $this->description;
    }

    /**
     * @return User
     */
    public function user() : User
    {
        return $this->user;
    }

    /**
     * @return float
     */
    public function preparationTime() : float
    {
        return $this->preparationTime;
    }

    /**
     * @return float
     */
    public function cookingTime() : float
    {
        return $this->cookingTime;
    }

    /**
     * @return int
     */
    public function serves() : int
    {
        return $this->serves;
    }

    /**
     * @return bool
     */
    public function isThermomix() : bool
    {
        return $this->isThermomix;
    }
}
