<?php

namespace RecipeBook\Application\Service\Api\Recipe\Update\RecipeStep;

use RecipeBook\Domain\Model\Recipe\Recipe;

final class UpdateRecipeStepCommand
{
    /**
     * @var string
     */
    private $id;

    /**
     * @var int
     */
    private $number;

    /**
     * @var string|null
     */
    private $name;

    /**ç
     * @var string
     */
    private $description;

    /**
     * @var Recipe
     */
    private $recipe;

    /**
     * UpdateRecipeStepCommand constructor.
     * @param string $id
     * @param int $number
     * @param string $name
     * @param string $description
     * @param Recipe $recipe
     */
    public function __construct(
        string $id,
        int $number,
        string $name = null,
        string $description,
        Recipe $recipe
    ) {
        $this->id = $id;
        $this->recipe = $recipe;
        $this->number = $number;
        $this->name = $name;
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function id(): string
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function number() : int
    {
        return $this->number;
    }

    /**
     * @return string|null
     */
    public function name(): ?string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function description() : string
    {
        return $this->description;
    }

    /**
     * @return Recipe
     */
    public function recipe(): Recipe
    {
        return $this->recipe;
    }
}
