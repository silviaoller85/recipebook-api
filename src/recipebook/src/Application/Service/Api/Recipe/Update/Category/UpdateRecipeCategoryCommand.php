<?php

namespace RecipeBook\Application\Service\Api\Recipe\Update\Category;

use RecipeBook\Domain\Model\Recipe\Recipe;

final class UpdateRecipeCategoryCommand
{
    /**
     * @var Recipe
     */
    private $recipe;

    /**
     * @var array
     */
    private $categories;

    /**
     * UpdateRecipeCategoryCommand constructor.
     * @param Recipe $recipe
     * @param array $categories
     */
    public function __construct(
        Recipe $recipe,
        array $categories
    ) {
        $this->recipe = $recipe;
        $this->categories = $categories;
    }

    /**
     * @return Recipe
     */
    public function recipe() : Recipe
    {
        return $this->recipe;
    }

    /**
     * @return array
     */
    public function categories() : array
    {
        return $this->categories;
    }
}
