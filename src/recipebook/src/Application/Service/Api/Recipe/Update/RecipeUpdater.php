<?php

namespace RecipeBook\Application\Service\Api\Recipe\Update;

use RecipeBook\Domain\Common\ValueObject\Uuid;
use RecipeBook\Domain\Model\Recipe\Recipe;
use RecipeBook\Domain\Model\Recipe\RecipeNotFoundException;
use RecipeBook\Domain\Model\Recipe\RecipeRepositoryGatewayInterface;
use RecipeBook\Domain\Model\User\User;

final class RecipeUpdater
{
    /**
     * @var RecipeRepositoryGatewayInterface
     */
    private $repository;

    /**
     * RecipeUpdater constructor.
     * @param RecipeRepositoryGatewayInterface $repository
     */
    public function __construct(RecipeRepositoryGatewayInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param Uuid $id
     * @param string $name
     * @param string|null $description
     * @param User $user
     * @param float $preparationTime
     * @param float $cookingTime
     * @param int $serves
     * @param bool $isThermomix
     * @return Recipe
     */
    public function update(
        Uuid $id,
        string $name,
        string $description = null,
        User $user,
        float $preparationTime,
        float $cookingTime,
        int $serves,
        bool $isThermomix
    ) : Recipe {
        $recipe = $this->repository->findOneById($id->value());

        if ($recipe === null) {
            throw new RecipeNotFoundException($id->value());
        }

        $data = [
            'name' => $name,
            'description' => $description,
            'user' => $user,
            'preparationTime' => $preparationTime,
            'cookingTime' => $cookingTime,
            'serves' => $serves,
            'isThermomix' => $isThermomix
        ];

        $recipe->batchUpdate($data);

        return $recipe;
    }
}
