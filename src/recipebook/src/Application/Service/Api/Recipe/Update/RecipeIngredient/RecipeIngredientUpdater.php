<?php

namespace RecipeBook\Application\Service\Api\Recipe\Update\RecipeIngredient;

use RecipeBook\Domain\Common\ValueObject\Uuid;
use RecipeBook\Domain\Model\Ingredient\Ingredient;
use RecipeBook\Domain\Model\Recipe\Recipe;
use RecipeBook\Domain\Model\Recipe\RecipeIngredient\RecipeIngredient;
use RecipeBook\Domain\Model\Recipe\RecipeIngredient\RecipeIngredientNotFoundException;
use RecipeBook\Domain\Model\Recipe\RecipeIngredient\RecipeIngredientRepositoryGatewayInterface;
use RecipeBook\Domain\Model\Unit\Unit;

final class RecipeIngredientUpdater
{
    /**
     * @var RecipeIngredientRepositoryGatewayInterface
     */
    private $repository;

    /**
     * RecipeUpdater constructor.
     * @param RecipeIngredientRepositoryGatewayInterface $repository
     */
    public function __construct(RecipeIngredientRepositoryGatewayInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param Uuid $id
     * @param Recipe $recipe
     * @param Ingredient $ingredient
     * @param string $value
     * @param Unit $unit
     * @return RecipeIngredient
     */
    public function update(
        Uuid $id,
        Recipe $recipe,
        Ingredient $ingredient,
        string $value,
        Unit $unit
    ) : RecipeIngredient {

        $recipeIngredient = $this->repository->findOneById($id->value());

        if ($recipeIngredient === null) {
            throw new RecipeIngredientNotFoundException($id->value());
        }

        if ($recipeIngredient->recipe()->id() !== $recipe->id()) {
            throw new RecipeIngredientNotFoundException($recipe->id());
        }

        $data = [
            'ingredient' => $ingredient,
            'value' => $value,
            'unit' => $unit
        ];

        $recipeIngredient->batchUpdate($data);

        return $recipeIngredient;
    }
}
