<?php

namespace RecipeBook\Application\Service\Api\Recipe\Update\Category;

final class UpdateRecipeCategoryCommandHandler
{
    /**
     * @var RecipeCategoryUpdater
     */
    private $updater;

    /**
     * UpdateRecipeCategoryCommandHandler constructor.
     * @param RecipeCategoryUpdater $updater
     */
    public function __construct(RecipeCategoryUpdater $updater)
    {
        $this->updater = $updater;
    }

    /**
     * @param UpdateRecipeCategoryCommand $command
     * @return mixed
     */
    public function handle(UpdateRecipeCategoryCommand $command)
    {
        return $this->updater->update($command->recipe(), $command->categories());
    }
}
