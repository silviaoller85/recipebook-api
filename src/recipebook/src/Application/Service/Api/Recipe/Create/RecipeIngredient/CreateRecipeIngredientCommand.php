<?php

namespace RecipeBook\Application\Service\Api\Recipe\Create\RecipeIngredient;

use RecipeBook\Domain\Model\Ingredient\Ingredient;
use RecipeBook\Domain\Model\Recipe\Recipe;
use RecipeBook\Domain\Model\Unit\Unit;

final class CreateRecipeIngredientCommand
{
    /**
     * @var string
     */
    private $id;

    /**
     * @var Recipe
     */
    private $recipe;

    /**
     * @var Ingredient
     */
    private $ingredient;

    /**ç
     * @var string
     */
    private $value;

    /**
     * @var Unit
     */
    private $unit;


    /**
     * @param string $id
     * @param Recipe $recipe
     * @param Ingredient $ingredient
     * @param string $value
     * @param Unit $unit
     */
    public function __construct(
        string $id,
        Recipe $recipe,
        Ingredient $ingredient,
        string $value,
        Unit $unit
    ) {
        $this->id = $id;
        $this->recipe = $recipe;
        $this->ingredient = $ingredient;
        $this->value = $value;
        $this->unit = $unit;
    }

    /**
     * @return string
     */
    public function id(): string
    {
        return $this->id;
    }

    /**
     * @return Recipe
     */
    public function recipe(): Recipe
    {
        return $this->recipe;
    }

    /**
     * @return Ingredient
     */
    public function ingredient(): Ingredient
    {
        return $this->ingredient;
    }

    /**
     * @return string
     */
    public function value() : string
    {
        return $this->value;
    }

    /**
     * @return Unit
     */
    public function unit() : Unit
    {
        return $this->unit;
    }
}
