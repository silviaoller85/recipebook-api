<?php

namespace RecipeBook\Application\Service\Api\Recipe\Create\RecipeIngredient;

use RecipeBook\Domain\Common\ValueObject\Uuid;
use RecipeBook\Domain\Model\Recipe\RecipeIngredient\RecipeIngredient;

final class CreateRecipeIngredientCommandHandler
{
    /**
     * @var RecipeIngredientCreator
     */
    private $creator;

    /**
     * CreateRecipeCommandHandler constructor.
     * @param RecipeIngredientCreator $creator
     */
    public function __construct(RecipeIngredientCreator $creator)
    {
        $this->creator = $creator;
    }

    /**
     * @param CreateRecipeIngredientCommand $command
     * @return RecipeIngredient
     * @throws \Exception
     */
    public function handle(CreateRecipeIngredientCommand $command) : RecipeIngredient
    {
        return $this->creator->create(
            new Uuid($command->id()),
            $command->recipe(),
            $command->ingredient(),
            $command->value(),
            $command->unit()
        );
    }
}
