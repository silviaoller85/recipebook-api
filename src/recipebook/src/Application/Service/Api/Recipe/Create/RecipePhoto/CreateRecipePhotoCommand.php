<?php

namespace RecipeBook\Application\Service\Api\Recipe\Create\RecipePhoto;

use RecipeBook\Domain\Model\File;
use RecipeBook\Domain\Model\Recipe\Recipe;
use RecipeBook\Domain\Model\Recipe\RecipeStep\RecipeStep;
use RecipeBook\Domain\Model\User\User;

final class CreateRecipePhotoCommand
{
    /**
     * @var string
     */
    private $id;

    /**
     * @var User
     */
    private $user;

    /**
     * @var Recipe
     */
    private $recipe;

    /**
     * @var null|RecipeStep
     */
    private $recipeStep;

    /**
     * @var File
     */
    private $photo;

    /**
     * CreateRecipePhotoCommand constructor.
     * @param string $id
     * @param User $user
     * @param Recipe $recipe
     * @param RecipeStep|null $recipeStep
     * @param File $photo
     */
    public function __construct(
        string $id,
        User $user,
        Recipe $recipe,
        RecipeStep $recipeStep = null,
        File $photo
    ) {
        $this->id = $id;
        $this->user = $user;
        $this->recipe = $recipe;
        $this->recipeStep = $recipeStep;
        $this->photo = $photo;
    }

    /**
     * @return string
     */
    public function id(): string
    {
        return $this->id;
    }

    /**
     * @return User
     */
    public function user(): User
    {
        return $this->user;
    }

    /**
     * @return Recipe
     */
    public function recipe(): Recipe
    {
        return $this->recipe;
    }

    /**
     * @return RecipeStep|null
     */
    public function recipeStep() : ?RecipeStep
    {
        return $this->recipeStep;
    }

    /**
     * @return File
     */
    public function photo(): File
    {
        return $this->photo;
    }
}
