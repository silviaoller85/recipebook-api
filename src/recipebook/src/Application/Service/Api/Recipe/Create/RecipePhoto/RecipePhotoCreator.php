<?php

namespace RecipeBook\Application\Service\Api\Recipe\Create\RecipePhoto;

use RecipeBook\Domain\Common\ValueObject\Uuid;
use RecipeBook\Domain\Model\File;
use RecipeBook\Domain\Model\Recipe\Recipe;
use RecipeBook\Domain\Model\Recipe\RecipePhoto\RecipePhoto;
use RecipeBook\Domain\Model\Recipe\RecipePhoto\RecipePhotoRepositoryGatewayInterface;
use RecipeBook\Domain\Model\Recipe\RecipeStep\RecipeStep;
use RecipeBook\Infrastructure\Storage\FileUploader;

final class RecipePhotoCreator
{
    /**
     * @var RecipePhotoRepositoryGatewayInterface
     */
    private $repository;

    /**
     * @var FileUploader
     */
    private $uploader;

    /**
     * RecipeCreator constructor.
     * @param RecipePhotoRepositoryGatewayInterface $repository
     * @param FileUploader $uploader
     */
    public function __construct(
        RecipePhotoRepositoryGatewayInterface $repository,
        FileUploader $uploader
    ) {
        $this->repository = $repository;
        $this->uploader = $uploader;
    }

    /**
     * @param Uuid $id
     * @param Recipe $recipe
     * @param RecipeStep $recipeStep
     * @param File $photo
     * @return RecipePhoto
     * @throws \Exception
     */
    public function create(
        Uuid $id,
        Recipe $recipe,
        RecipeStep $recipeStep = null,
        File $photo
    ) : RecipePhoto {

        $file = $this->uploader->upload($photo, $recipe, $recipeStep);

        $recipePhoto = RecipePhoto::create(
            $id,
            $recipe,
            $recipeStep,
            $file->path(),
            $file->url(),
            1
        );

        $this->repository->persist($recipePhoto);

        return $recipePhoto;
    }
}
