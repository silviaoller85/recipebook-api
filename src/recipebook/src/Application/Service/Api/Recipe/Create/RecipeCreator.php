<?php

namespace RecipeBook\Application\Service\Api\Recipe\Create;

use RecipeBook\Domain\Common\ValueObject\Uuid;
use RecipeBook\Domain\Model\Category\Category;
use RecipeBook\Domain\Model\Recipe\Recipe;
use RecipeBook\Domain\Model\Recipe\RecipeRepositoryGatewayInterface;
use RecipeBook\Domain\Model\User\User;

final class RecipeCreator
{
    /**
     * @var RecipeRepositoryGatewayInterface
     */
    private $repository;

    /**
     * RecipeCreator constructor.
     * @param RecipeRepositoryGatewayInterface $repository
     */
    public function __construct(RecipeRepositoryGatewayInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param Uuid $id
     * @param string $name
     * @param string|null $description
     * @param User $user
     * @param Category $category
     * @param float $preparationTime
     * @param float $cookingTime
     * @param int $serves
     * @param bool $isThermomix
     * @return Recipe
     * @throws \Exception
     */
    public function create(
        Uuid $id,
        string $name,
        string $description = null,
        User $user,
        float $preparationTime,
        float $cookingTime,
        int $serves,
        bool $isThermomix
    ) : Recipe {

        $recipe = Recipe::create(
            $id,
            $user,
            $name,
            $description,
            $preparationTime,
            $cookingTime,
            $serves,
            $isThermomix
        );

        $this->repository->persist($recipe);

        return $recipe;
    }
}
