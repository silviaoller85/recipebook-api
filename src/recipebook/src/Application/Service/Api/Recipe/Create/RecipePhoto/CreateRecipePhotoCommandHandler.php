<?php

namespace RecipeBook\Application\Service\Api\Recipe\Create\RecipePhoto;

use RecipeBook\Domain\Common\ValueObject\Uuid;
use RecipeBook\Domain\Model\Recipe\RecipeNotFoundException;
use RecipeBook\Domain\Model\Recipe\RecipePhoto\RecipePhoto;
use RecipeBook\Infrastructure\Storage\FileUploader;

final class CreateRecipePhotoCommandHandler
{
    /**
     * @var RecipePhotoCreator
     */
    private $creator;

    /**
     * CreateRecipeCommandHandler constructor.
     * @param RecipePhotoCreator $creator
     */
    public function __construct(RecipePhotoCreator $creator)
    {
        $this->creator = $creator;
    }

    /**
     * @param CreateRecipePhotoCommand $command
     * @return RecipePhoto
     * @throws \Exception
     */
    public function handle(CreateRecipePhotoCommand $command) : RecipePhoto
    {
        $recipe = $command->recipe();
        if ($command->user() && $recipe->user()->id() != $command->user()->id()) {
            throw new RecipeNotFoundException($command->id());
        }

        return $this->creator->create(
            new Uuid($command->id()),
            $command->recipe(),
            $command->recipeStep(),
            $command->photo()
        );
    }
}
