<?php

namespace RecipeBook\Application\Service\Api\Recipe\Create\RecipeIngredient;

use RecipeBook\Domain\Common\ValueObject\Uuid;
use RecipeBook\Domain\Model\Ingredient\Ingredient;
use RecipeBook\Domain\Model\Recipe\Recipe;
use RecipeBook\Domain\Model\Recipe\RecipeIngredient\RecipeIngredient;
use RecipeBook\Domain\Model\Recipe\RecipeIngredient\RecipeIngredientRepositoryGatewayInterface;
use RecipeBook\Domain\Model\Unit\Unit;

final class RecipeIngredientCreator
{
    /**
     * @var RecipeIngredientRepositoryGatewayInterface
     */
    private $repository;

    /**
     * RecipeCreator constructor.
     * @param RecipeIngredientRepositoryGatewayInterface $repository
     */
    public function __construct(RecipeIngredientRepositoryGatewayInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param Uuid $id
     * @param Recipe $recipe
     * @param Ingredient $ingredient
     * @param string $value
     * @param Unit $unit
     * @return RecipeIngredient
     * @throws \Exception
     */
    public function create(
        Uuid $id,
        Recipe $recipe,
        Ingredient $ingredient,
        string $value,
        Unit $unit
    ) : RecipeIngredient {

        $recipeIngredient = RecipeIngredient::create(
            $id,
            $recipe,
            $ingredient,
            $value,
            $unit
        );

        $this->repository->persist($recipeIngredient);

        return $recipeIngredient;
    }
}
