<?php

namespace RecipeBook\Application\Service\Api\Recipe\Create;

use RecipeBook\Domain\Common\ValueObject\Uuid;
use RecipeBook\Domain\Model\Recipe\Recipe;

final class CreateRecipeCommandHandler
{
    /**
     * @var RecipeCreator
     */
    private $creator;

    /**
     * CreateRecipeCommandHandler constructor.
     * @param RecipeCreator $creator
     */
    public function __construct(RecipeCreator $creator)
    {
        $this->creator = $creator;
    }

    /**
     * @param CreateRecipeCommand $command
     * @return Recipe
     * @throws \Exception
     */
    public function handle(CreateRecipeCommand $command) : Recipe
    {
        return $this->creator->create(
            new Uuid($command->id()),
            $command->name(),
            $command->description(),
            $command->user(),
            $command->preparationTime(),
            $command->cookingTime(),
            $command->serves(),
            $command->isThermomix()
        );
    }
}
