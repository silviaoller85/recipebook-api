<?php

namespace RecipeBook\Application\Service\Api\Recipe\Create\RecipeStep;

use RecipeBook\Domain\Common\ValueObject\Uuid;
use RecipeBook\Domain\Model\Recipe\RecipeStep\RecipeStep;

final class CreateRecipeStepCommandHandler
{
    /**
     * @var RecipeStepCreator
     */
    private $creator;

    /**
     * CreateRecipeCommandHandler constructor.
     * @param RecipeStepCreator $creator
     */
    public function __construct(RecipeStepCreator $creator)
    {
        $this->creator = $creator;
    }

    /**
     * @param CreateRecipeStepCommand $command
     * @return RecipeStep
     * @throws \Exception
     */
    public function handle(CreateRecipeStepCommand $command) : RecipeStep
    {
        return $this->creator->create(
            new Uuid($command->id()),
            $command->recipe(),
            $command->number(),
            $command->name(),
            $command->description()
        );
    }
}
