<?php

namespace RecipeBook\Application\Service\Api\Recipe\Create\RecipeStep;

use RecipeBook\Domain\Common\ValueObject\Uuid;
use RecipeBook\Domain\Model\Recipe\Recipe;
use RecipeBook\Domain\Model\Recipe\RecipeStep\RecipeStep;
use RecipeBook\Domain\Model\Recipe\RecipeStep\RecipeStepRepositoryGatewayInterface;

final class RecipeStepCreator
{
    /**
     * @var RecipeStepRepositoryGatewayInterface
     */
    private $repository;

    /**
     * RecipeCreator constructor.
     * @param RecipeStepRepositoryGatewayInterface $repository
     */
    public function __construct(RecipeStepRepositoryGatewayInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param Uuid $id
     * @param Recipe $recipe
     * @param int $number
     * @param string|null $name
     * @param string $description
     * @return RecipeStep
     * @throws \Exception
     */
    public function create(
        Uuid $id,
        Recipe $recipe,
        int $number,
        string $name = null,
        string $description
    ) : RecipeStep {

        $recipeStep = RecipeStep::create(
            $id,
            $recipe,
            $number,
            $name,
            $description
        );

        $this->repository->persist($recipeStep);

        return $recipeStep;
    }
}
