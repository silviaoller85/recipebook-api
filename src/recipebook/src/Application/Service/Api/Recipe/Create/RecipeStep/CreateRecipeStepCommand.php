<?php

namespace RecipeBook\Application\Service\Api\Recipe\Create\RecipeStep;

use RecipeBook\Domain\Model\Recipe\Recipe;

final class CreateRecipeStepCommand
{
    /**
     * @var string
     */
    private $id;

    /**
     * @var Recipe
     */
    private $recipe;

    /**
     * @var int
     */
    private $number;

    /**
     * @var string|null
     */
    private $name;

    /**ç
     * @var string
     */
    private $description;

    /**
     * CreateRecipeStepCommand constructor.
     * @param string $id
     * @param Recipe $recipe
     * @param int $number
     * @param string $name
     * @param string|null $description
     */
    public function __construct(
        string $id,
        Recipe $recipe,
        int $number,
        string $name = null,
        string $description
    ) {
        $this->id = $id;
        $this->recipe = $recipe;
        $this->number = $number;
        $this->name = $name;
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function id(): string
    {
        return $this->id;
    }

    /**
     * @return Recipe
     */
    public function recipe(): Recipe
    {
        return $this->recipe;
    }

    /**
     * @return int
     */
    public function number() : int
    {
        return $this->number;
    }

    /**
     * @return string|null
     */
    public function name(): ?string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function description() : string
    {
        return $this->description;
    }
}
