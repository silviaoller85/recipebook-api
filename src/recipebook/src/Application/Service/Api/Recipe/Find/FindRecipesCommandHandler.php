<?php

namespace RecipeBook\Application\Service\Api\Recipe\Find;

final class FindRecipesCommandHandler
{
    /**
     * @var RecipeFinder
     */
    private $finder;

    /**
     * FindRecipesCommandHandler constructor.
     * @param RecipeFinder $finder
     */
    public function __construct(RecipeFinder $finder)
    {
        $this->finder = $finder;
    }

    /**
     * @param FindRecipesCommand $command
     * @return array
     */
    public function handle(FindRecipesCommand $command) : array
    {
        return $this->finder->filterOrFail($command->filters(), $command->options());
    }
}
