<?php

namespace RecipeBook\Application\Service\Api\Recipe\Find;

use RecipeBook\Domain\Model\Recipe\RecipeFinder as DomainRecipeFinder;

final class RecipeFinder
{
    /**
     * @var DomainRecipeFinder
     */
    private $finder;

    /**
     * RecipeFinder constructor.
     * @param DomainRecipeFinder $finder
     */
    public function __construct(DomainRecipeFinder $finder)
    {
        $this->finder = $finder;
    }

    /**
     * @param array $filters
     * @param array $options
     * @return array
     */
    public function filterOrFail(array $filters = [], array $options = []) : array
    {
        return $this->finder->findBy($filters, $options);
    }
}
