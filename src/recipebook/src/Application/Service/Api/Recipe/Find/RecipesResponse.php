<?php

namespace RecipeBook\Application\Service\Api\Recipe\Find;

use RecipeBook\Application\Common\Transformer\PaginateResponse;
use RecipeBook\Domain\Model\Recipe\Recipe;

final class RecipesResponse
{
    use PaginateResponse;

    /**
     * @param array $recipes
     * @param int $currentPage
     * @param int $itemsPerPage
     * @param int $total
     * @return array
     */
    public function transform(array $recipes = [], int $currentPage, int $itemsPerPage, int $total) : array
    {
        $items = [];
        array_map(function (Recipe $recipe) use (&$items) {
            $items[] = $recipe->toArray();
        }, $recipes);

        return $this->paginateTransform($items, $currentPage, $itemsPerPage, $total);
    }
}
