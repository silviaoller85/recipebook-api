<?php

namespace RecipeBook\Application\Service\Api\Recipe\Get\RecipeStep;

use RecipeBook\Domain\Model\Recipe\RecipeStep\RecipeStep;
use RecipeBook\Domain\Model\Recipe\RecipeStep\RecipeStepFinder as DomainRecipeFinder;

/**
 * Class RecipeStepGetter
 * @package RecipeBook\Application\Service\Api\Recipe\Get\RecipeStep
 */
final class RecipeStepGetter
{
    /**
     * @var DomainRecipeFinder
     */
    private $finder;

    /**
     * RecipeGetter constructor.
     * @param DomainRecipeFinder $finder
     */
    public function __construct(DomainRecipeFinder $finder)
    {
        $this->finder = $finder;
    }

    /**
     * @param string $id
     * @return RecipeStep
     */
    public function getById(string $id) : RecipeStep
    {
        return $this->finder->getById($id);
    }
}
