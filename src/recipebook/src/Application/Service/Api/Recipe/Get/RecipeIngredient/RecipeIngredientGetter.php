<?php

namespace RecipeBook\Application\Service\Api\Recipe\Get\RecipeIngredient;

use RecipeBook\Domain\Model\Recipe\RecipeIngredient\RecipeIngredient;
use RecipeBook\Domain\Model\Recipe\RecipeIngredient\RecipeIngredientFinder as DomainRecipeFinder;

/**
 * Class RecipeIngredientGetter
 * @package RecipeBook\Application\Service\Api\Recipe\Get\RecipeIngredient
 */
final class RecipeIngredientGetter
{
    /**
     * @var DomainRecipeFinder
     */
    private $finder;

    /**
     * RecipeGetter constructor.
     * @param DomainRecipeFinder $finder
     */
    public function __construct(DomainRecipeFinder $finder)
    {
        $this->finder = $finder;
    }

    /**
     * @param string $id
     * @return RecipeIngredient
     */
    public function getById(string $id) : RecipeIngredient
    {
        return $this->finder->getById($id);
    }
}
