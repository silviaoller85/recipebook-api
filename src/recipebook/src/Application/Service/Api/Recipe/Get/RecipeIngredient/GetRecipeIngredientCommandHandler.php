<?php

namespace RecipeBook\Application\Service\Api\Recipe\Get\RecipeIngredient;

use RecipeBook\Domain\Model\Recipe\RecipeIngredient\RecipeIngredient;
use RecipeBook\Domain\Model\Recipe\RecipeIngredient\RecipeIngredientNotFoundException;

/**
 * Class GetRecipeIngredientCommandHandler
 * @package RecipeBook\Application\Service\Api\Recipe\Get\RecipeIngredient
 */
final class GetRecipeIngredientCommandHandler
{
    /**
     * @var RecipeIngredientGetter
     */
    private $getter;

    /**
     * GetRecipeCommandHandler constructor.
     * @param RecipeIngredientGetter $getter
     */
    public function __construct(RecipeIngredientGetter $getter)
    {
        $this->getter = $getter;
    }

    /**
     * @param GetRecipeIngredientCommand $command
     * @return RecipeIngredient
     */
    public function handle(GetRecipeIngredientCommand $command) : RecipeIngredient
    {
        $recipeIngredient = $this->getter->getById($command->id());

        if ($recipeIngredient->recipe()->id() !== $command->recipe()->id()) {
            throw new RecipeIngredientNotFoundException($command->id());
        }

        return $recipeIngredient;
    }
}
