<?php

namespace RecipeBook\Application\Service\Api\Recipe\Get;

use RecipeBook\Domain\Model\Recipe\Recipe;

/**
 * Class RecipeResponse
 * @package RecipeBook\Application\Service\Api\Recipe\Get
 */
final class RecipeResponse
{
    /**
     * @param Recipe $recipe
     * @return array
     */
    public function transform(Recipe $recipe) : array
    {
        return $recipe->toArray();
    }
}
