<?php

namespace RecipeBook\Application\Service\Api\Recipe\Get;

use RecipeBook\Domain\Model\User\User;

/**
 * Class GetRecipeCommand
 * @package RecipeBook\Application\Service\Api\Recipe\Get
 */
final class GetRecipeCommand
{
    /**
     * @var string
     */
    private $id;

    /**
     * @var User|null
     */
    private $user;

    /**
     * GetRecipeCommand constructor.
     * @param string $id
     * @param User $user
     */
    public function __construct(string $id, User $user = null)
    {
        $this->id = $id;
        $this->user = $user;
    }

    /**
     * @return string
     */
    public function id(): string
    {
        return $this->id;
    }

    /**
     * @return User|null
     */
    public function user() : ?User
    {
        return $this->user;
    }
}
