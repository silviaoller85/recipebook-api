<?php

namespace RecipeBook\Application\Service\Api\Recipe\Get;

use RecipeBook\Domain\Model\Recipe\Recipe;
use RecipeBook\Domain\Model\Recipe\RecipeFinder as DomainRecipeFinder;

/**
 * Class RecipeGetter
 * @package RecipeBook\Application\Service\Api\Recipe\Get
 */
final class RecipeGetter
{
    /**
     * @var DomainRecipeFinder
     */
    private $finder;

    /**
     * RecipeGetter constructor.
     * @param DomainRecipeFinder $finder
     */
    public function __construct(DomainRecipeFinder $finder)
    {
        $this->finder = $finder;
    }

    /**
     * @param string $id
     * @return Recipe
     * @throws \RecipeBook\Domain\Model\Recipe\RecipeNotFoundException
     */
    public function getById(string $id) : Recipe
    {
        return $this->finder->getById($id);
    }
}
