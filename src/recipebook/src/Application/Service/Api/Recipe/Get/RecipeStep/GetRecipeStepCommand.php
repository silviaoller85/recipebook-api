<?php

namespace RecipeBook\Application\Service\Api\Recipe\Get\RecipeStep;

use RecipeBook\Domain\Model\Recipe\Recipe;

/**
 * Class GetRecipeStepCommand
 * @package RecipeBook\Application\Service\Api\Recipe\Get\RecipeStep
 */
final class GetRecipeStepCommand
{
    /**
     * @var string
     */
    private $id;

    /**
     * @var Recipe
     */
    private $recipe;

    /**
     * GetRecipeCommand constructor.
     * @param string $id
     * @param Recipe $recipe
     */
    public function __construct(string $id, Recipe $recipe)
    {
        $this->id = $id;
        $this->recipe = $recipe;
    }

    /**
     * @return string
     */
    public function id(): string
    {
        return $this->id;
    }

    /**
     * @return Recipe
     */
    public function recipe() : Recipe
    {
        return $this->recipe;
    }
}
