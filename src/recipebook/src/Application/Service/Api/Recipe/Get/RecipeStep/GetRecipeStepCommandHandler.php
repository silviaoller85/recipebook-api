<?php

namespace RecipeBook\Application\Service\Api\Recipe\Get\RecipeStep;

use RecipeBook\Domain\Model\Recipe\RecipeStep\RecipeStep;
use RecipeBook\Domain\Model\Recipe\RecipeStep\RecipeStepNotFoundException;

/**
 * Class GetRecipeStepCommandHandler
 * @package RecipeBook\Application\Service\Api\Recipe\Get\RecipeStep
 */
final class GetRecipeStepCommandHandler
{
    /**
     * @var RecipeStepGetter
     */
    private $getter;

    /**
     * GetRecipeCommandHandler constructor.
     * @param RecipeStepGetter $getter
     */
    public function __construct(RecipeStepGetter $getter)
    {
        $this->getter = $getter;
    }

    /**
     * @param GetRecipeStepCommand $command
     * @return RecipeStep
     */
    public function handle(GetRecipeStepCommand $command) : RecipeStep
    {
        $recipeStep = $this->getter->getById($command->id());

        if ($recipeStep->recipe()->id() !== $command->recipe()->id()) {
            throw new RecipeStepNotFoundException($command->id());
        }

        return $recipeStep;
    }
}
