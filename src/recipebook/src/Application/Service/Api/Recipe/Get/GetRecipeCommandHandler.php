<?php

namespace RecipeBook\Application\Service\Api\Recipe\Get;

use RecipeBook\Domain\Model\Recipe\Recipe;
use RecipeBook\Domain\Model\Recipe\RecipeNotFoundException;

/**
 * Class GetRecipeCommandHandler
 * @package RecipeBook\Application\Service\Api\Recipe\Get
 */
final class GetRecipeCommandHandler
{
    /**
     * @var RecipeGetter
     */
    private $getter;

    /**
     * GetRecipeCommandHandler constructor.
     * @param RecipeGetter $getter
     */
    public function __construct(RecipeGetter $getter)
    {
        $this->getter = $getter;
    }

    /**
     * @param GetRecipeCommand $command
     * @return Recipe
     */
    public function handle(GetRecipeCommand $command) : Recipe
    {
        $recipe = $this->getter->getById($command->id());

        if ($command->user() && $recipe->user()->id() != $command->user()->id()) {
            throw new RecipeNotFoundException($command->id());
        }

        return $recipe;
    }
}
