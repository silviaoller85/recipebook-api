<?php

namespace RecipeBook\Application\Service\Api\Thermomix\Get\ThermomixModel;

use RecipeBook\Domain\Model\Thermomix\ThermomixModel\ThermomixModel;
use RecipeBook\Domain\Model\Thermomix\ThermomixModel\ThermomixModelFinder as DomainThermomixModelFinder;

/**
 * Class ThermomixModelGetter
 * @package RecipeBook\Application\Service\Api\ThermomixModel\Get
 */
final class ThermomixModelGetter
{
    /**
     * @var DomainThermomixModelFinder
     */
    private $finder;

    /**
     * ThermomixModelGetter constructor.
     * @param DomainThermomixModelFinder $finder
     */
    public function __construct(DomainThermomixModelFinder $finder)
    {
        $this->finder = $finder;
    }

    /**
     * @param string $id
     * @return ThermomixModel
     */
    public function getById(string $id) : ThermomixModel
    {
        return $this->finder->getById($id);
    }
}
