<?php

namespace RecipeBook\Application\Service\Api\Thermomix\Get\ThermomixModel;

/**
 * Class GetThermomixModelCommand
 * @package Recipe\Application\Service\Api\ThermomixModel\Get
 */
final class GetThermomixModelCommand
{
    /**
     * @var string
     */
    private $id;

    /**
     * GetThermomixModelCommand constructor.
     * @param string $id
     */
    public function __construct(string $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function id(): string
    {
        return $this->id;
    }
}
