<?php

namespace RecipeBook\Application\Service\Api\Thermomix\Get\ThermomixModel;

use RecipeBook\Domain\Model\Thermomix\ThermomixModel\ThermomixModel;

/**
 * Class GetThermomixModelCommandHandler
 * @package RecipeBook\Application\Service\Api\ThermomixModel\Get
 */
final class GetThermomixModelCommandHandler
{
    /**
     * @var ThermomixModelGetter
     */
    private $getter;

    /**
     * GetThermomixModelCommandHandler constructor.
     * @param ThermomixModelGetter $getter
     */
    public function __construct(ThermomixModelGetter $getter)
    {
        $this->getter = $getter;
    }

    /**
     * @param GetThermomixModelCommand $command
     * @return ThermomixModel
     */
    public function handle(GetThermomixModelCommand $command) : ThermomixModel
    {
        return $this->getter->getById($command->id());
    }
}
