<?php

namespace RecipeBook\Application\Service\Api\Thermomix\Find\ThermomixAction;

use RecipeBook\Domain\Model\Thermomix\ThermomixAction\ThermomixActionFinder as DomainThermomixActionFinder;

final class ThermomixActionFinder
{
    /**
     * @var DomainThermomixActionFinder
     */
    private $finder;

    /**
     * ThermomixActionFinder constructor.
     * @param DomainThermomixActionFinder $finder
     */
    public function __construct(DomainThermomixActionFinder $finder)
    {
        $this->finder = $finder;
    }

    /**
     * @param array $filters
     * @param array $options
     * @return array
     */
    public function filterOrFail(array $filters = [], array $options = []) : array
    {
        return $this->finder->findBy($filters, $options);
    }
}
