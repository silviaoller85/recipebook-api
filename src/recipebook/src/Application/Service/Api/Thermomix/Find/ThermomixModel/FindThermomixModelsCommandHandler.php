<?php

namespace RecipeBook\Application\Service\Api\Thermomix\Find\ThermomixModel;

final class FindThermomixModelsCommandHandler
{
    /**
     * @var ThermomixModelFinder
     */
    private $finder;

    /**
     * FindThermomixModelsCommandHandler constructor.
     * @param ThermomixModelFinder $finder
     */
    public function __construct(ThermomixModelFinder $finder)
    {
        $this->finder = $finder;
    }

    /**
     * @param FindThermomixModelsCommand $command
     * @return array
     */
    public function handle(FindThermomixModelsCommand $command) : array
    {
        return $this->finder->filterOrFail($command->filters(), $command->options());
    }
}
