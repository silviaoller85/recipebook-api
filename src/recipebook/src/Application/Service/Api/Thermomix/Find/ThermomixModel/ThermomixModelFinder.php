<?php

namespace RecipeBook\Application\Service\Api\Thermomix\Find\ThermomixModel;

use RecipeBook\Domain\Model\Thermomix\ThermomixModel\ThermomixModelFinder as DomainThermomixModelFinder;

final class ThermomixModelFinder
{
    /**
     * @var DomainThermomixModelFinder
     */
    private $finder;

    /**
     * ThermomixModelFinder constructor.
     * @param DomainThermomixModelFinder $finder
     */
    public function __construct(DomainThermomixModelFinder $finder)
    {
        $this->finder = $finder;
    }

    /**
     * @param array $filters
     * @param array $options
     * @return array
     */
    public function filterOrFail(array $filters = [], array $options = []) : array
    {
        return $this->finder->findBy($filters, $options);
    }
}
