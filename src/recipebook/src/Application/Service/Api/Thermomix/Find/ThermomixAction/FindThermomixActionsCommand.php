<?php

namespace RecipeBook\Application\Service\Api\Thermomix\Find\ThermomixAction;

final class FindThermomixActionsCommand
{
    /**
     * @var array
     */
    private $filters;

    /**
     * @var array
     */
    private $options;

    /**
     * FindThermomixActionsCommand constructor.
     * @param array $filters
     * @param array $options
     */
    public function __construct(array $filters = [], array $options = [])
    {
        $this->filters = $filters;
        $this->options = $options;
    }

    /**
     * @return array
     */
    public function filters(): array
    {
        return $this->filters;
    }

    /**
     * @return array
     */
    public function options() : array
    {
        return $this->options;
    }
}
