<?php

namespace RecipeBook\Application\Service\Api\Thermomix\Find\ThermomixAction;

final class FindThermomixActionsCommandHandler
{
    /**
     * @var ThermomixActionFinder
     */
    private $finder;

    /**
     * FindThermomixActionsCommandHandler constructor.
     * @param ThermomixActionFinder $finder
     */
    public function __construct(ThermomixActionFinder $finder)
    {
        $this->finder = $finder;
    }

    /**
     * @param FindThermomixActionsCommand $command
     * @return array
     */
    public function handle(FindThermomixActionsCommand $command) : array
    {
        return $this->finder->filterOrFail($command->filters(), $command->options());
    }
}
