<?php

namespace RecipeBook\Application\Service\Api\Thermomix\Count\ThermomixModel;

final class CountThermomixModelsCommand
{
    /**
     * @var array
     */
    private $filters;

    /**
     * CountRecipesCommand constructor.
     * @param array $filters
     */
    public function __construct(array $filters = [])
    {
        $this->filters = $filters;
    }

    /**
     * @return array
     */
    public function filters(): array
    {
        return $this->filters;
    }
}
