<?php

namespace RecipeBook\Application\Service\Api\Thermomix\Count\ThermomixModel;

final class CountThermomixModelsCommandHandler
{
    /**
     * @var ThermomixModelCounter
     */
    private $counter;

    /**
     * CountRecipesCommandHandler constructor.
     * @param ThermomixModelCounter $counter
     */
    public function __construct(ThermomixModelCounter $counter)
    {
        $this->counter = $counter;
    }

    /**
     * @param CountThermomixModelsCommand $command
     * @return int
     */
    public function handle(CountThermomixModelsCommand $command) : int
    {
        return $this->counter->count($command->filters());
    }
}
