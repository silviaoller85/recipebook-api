<?php

namespace RecipeBook\Application\Service\Api\Thermomix\Count\ThermomixModel;

use RecipeBook\Domain\Model\Thermomix\ThermomixModel\ThermomixModelFinder as DomainThermomixModelFinder;

final class ThermomixModelCounter
{
    /**
     * @var DomainThermomixModelFinder
     */
    private $finder;

    /**
     * RecipeFinder constructor.
     * @param DomainThermomixModelFinder $finder
     */
    public function __construct(DomainThermomixModelFinder $finder)
    {
        $this->finder = $finder;
    }

    /**
     * @param array $filters
     * @return int
     */
    public function count(array $filters = []) : int
    {
        return $this->finder->count($filters);
    }
}
