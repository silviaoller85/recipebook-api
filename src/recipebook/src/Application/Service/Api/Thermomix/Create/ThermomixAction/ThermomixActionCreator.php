<?php

namespace RecipeBook\Application\Service\Api\Thermomix\Create\ThermomixAction;

use RecipeBook\Domain\Common\ValueObject\Uuid;
use RecipeBook\Domain\Model\Thermomix\ThermomixAction\ThermomixAction;
use RecipeBook\Domain\Model\Thermomix\ThermomixAction\ThermomixActionRepositoryGatewayInterface;

final class ThermomixActionCreator
{
    /**
     * @var ThermomixActionRepositoryGatewayInterface
     */
    private $repository;

    /**
     * ThermomixActionCreator constructor.
     * @param ThermomixActionRepositoryGatewayInterface $repository
     */
    public function __construct(ThermomixActionRepositoryGatewayInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param Uuid $id
     * @param string $name
     * @param string|null $description
     * @return ThermomixAction
     * @throws \Exception
     */
    public function create(
        Uuid $id,
        string $name,
        string $description = null
    ) : ThermomixAction {
        $thermomixAction = ThermomixAction::create($id, $name, $description);

        $this->repository->persist($thermomixAction);

        return $thermomixAction;
    }
}
