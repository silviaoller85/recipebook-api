<?php

namespace RecipeBook\Application\Service\Api\Thermomix\Create\ThermomixModel;

use RecipeBook\Domain\Common\ValueObject\Uuid;
use RecipeBook\Domain\Model\Thermomix\ThermomixModel\ThermomixModel;
use RecipeBook\Domain\Model\Thermomix\ThermomixModel\ThermomixModelRepositoryGatewayInterface;

final class ThermomixModelCreator
{
    /**
     * @var ThermomixModelRepositoryGatewayInterface
     */
    private $repository;

    /**
     * ThermomixModelCreator constructor.
     * @param ThermomixModelRepositoryGatewayInterface $repository
     */
    public function __construct(ThermomixModelRepositoryGatewayInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param Uuid $id
     * @param string $name
     * @param string|null $description
     * @return ThermomixModel
     * @throws \Exception
     */
    public function create(
        Uuid $id,
        string $name,
        string $description = null
    ) : ThermomixModel {
        $thermomixModel = ThermomixModel::create($id, $name, $description);

        $this->repository->persist($thermomixModel);

        return $thermomixModel;
    }
}
