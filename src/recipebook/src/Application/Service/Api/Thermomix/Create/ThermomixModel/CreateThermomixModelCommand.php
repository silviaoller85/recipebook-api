<?php

namespace RecipeBook\Application\Service\Api\Thermomix\Create\ThermomixModel;

final class CreateThermomixModelCommand
{
    /**
     * @var string
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var null|string
     */
    private $description;

    /**
     * @param string $id
     * @param string $name
     * @param string|null $description
     */
    public function __construct(string $id, string $name, string $description = null)
    {
        $this->id = $id;
        $this->name = $name;
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function id(): string
    {
        return $this->id;
    }

    /**
     * @return null|string
     */
    public function name(): ?string
    {
        return $this->name;
    }

    /**
     * @return null|string
     */
    public function description(): ?string
    {
        return $this->description;
    }
}
