<?php

namespace RecipeBook\Application\Service\Api\Thermomix\Create\ThermomixAction;

use RecipeBook\Domain\Common\ValueObject\Uuid;
use RecipeBook\Domain\Model\Thermomix\ThermomixAction\ThermomixAction;

final class CreateThermomixActionCommandHandler
{
    /**
     * @var ThermomixActionCreator
     */
    private $creator;

    /**
     * CreateThermomixActionCommandHandler constructor.
     * @param ThermomixActionCreator $creator
     */
    public function __construct(ThermomixActionCreator $creator)
    {
        $this->creator = $creator;
    }

    /**
     * @param CreateThermomixActionCommand $command
     * @return ThermomixAction
     * @throws \Exception
     */
    public function handle(CreateThermomixActionCommand $command) : ThermomixAction
    {
        return $this->creator->create(new Uuid($command->id()), $command->name(), $command->description());
    }
}
