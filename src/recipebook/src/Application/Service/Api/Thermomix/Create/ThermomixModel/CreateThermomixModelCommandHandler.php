<?php

namespace RecipeBook\Application\Service\Api\Thermomix\Create\ThermomixModel;

use RecipeBook\Domain\Common\ValueObject\Uuid;
use RecipeBook\Domain\Model\Thermomix\ThermomixModel\ThermomixModel;

final class CreateThermomixModelCommandHandler
{
    /**
     * @var ThermomixModelCreator
     */
    private $creator;

    /**
     * CreateThermomixModelCommandHandler constructor.
     * @param ThermomixModelCreator $creator
     */
    public function __construct(ThermomixModelCreator $creator)
    {
        $this->creator = $creator;
    }

    /**
     * @param CreateThermomixModelCommand $command
     * @return ThermomixModel
     * @throws \Exception
     */
    public function handle(CreateThermomixModelCommand $command) : ThermomixModel
    {
        return $this->creator->create(new Uuid($command->id()), $command->name(), $command->description());
    }
}
