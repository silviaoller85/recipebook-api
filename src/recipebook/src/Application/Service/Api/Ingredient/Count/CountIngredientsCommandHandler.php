<?php

namespace RecipeBook\Application\Service\Api\Ingredient\Count;

final class CountIngredientsCommandHandler
{
    /**
     * @var IngredientCounter
     */
    private $counter;

    /**
     * CountIngredientsCommandHandler constructor.
     * @param IngredientCounter $counter
     */
    public function __construct(IngredientCounter $counter)
    {
        $this->counter = $counter;
    }

    /**
     * @param CountIngredientsCommand $command
     * @return int
     */
    public function handle(CountIngredientsCommand $command) : int
    {
        return $this->counter->count($command->filters());
    }
}
