<?php

namespace RecipeBook\Application\Service\Api\Ingredient\Count;

final class CountIngredientsCommand
{
    /**
     * @var array
     */
    private $filters;

    /**
     * CountIngredientsCommand constructor.
     * @param array $filters
     */
    public function __construct(array $filters = [])
    {
        $this->filters = $filters;
    }

    /**
     * @return array
     */
    public function filters(): array
    {
        return $this->filters;
    }
}
