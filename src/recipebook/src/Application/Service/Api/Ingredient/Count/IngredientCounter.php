<?php

namespace RecipeBook\Application\Service\Api\Ingredient\Count;

use RecipeBook\Domain\Model\Ingredient\IngredientFinder as DomainIngredientFinder;

final class IngredientCounter
{
    /**
     * @var DomainIngredientFinder
     */
    private $finder;

    /**
     * IngredientFinder constructor.
     * @param DomainIngredientFinder $finder
     */
    public function __construct(DomainIngredientFinder $finder)
    {
        $this->finder = $finder;
    }

    /**
     * @param array $filters
     * @return int
     */
    public function count(array $filters = []) : int
    {
        return $this->finder->count($filters);
    }
}
