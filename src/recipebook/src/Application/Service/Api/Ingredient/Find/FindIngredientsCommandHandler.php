<?php

namespace RecipeBook\Application\Service\Api\Ingredient\Find;

final class FindIngredientsCommandHandler
{
    /**
     * @var IngredientFinder
     */
    private $finder;

    /**
     * FindIngredientsCommandHandler constructor.
     * @param IngredientFinder $finder
     */
    public function __construct(IngredientFinder $finder)
    {
        $this->finder = $finder;
    }

    /**
     * @param FindIngredientsCommand $command
     * @return array
     */
    public function handle(FindIngredientsCommand $command) : array
    {
        return $this->finder->filterOrFail($command->filters(), $command->options());
    }
}
