<?php

namespace RecipeBook\Application\Service\Api\Ingredient\Find;

use RecipeBook\Application\Common\Transformer\PaginateResponse;
use RecipeBook\Domain\Model\Ingredient\Ingredient;

final class IngredientsResponse
{
    use PaginateResponse;

    /**
     * @param array $ingredients
     * @param int $currentPage
     * @param int $itemsPerPage
     * @param int $total
     * @return array
     */
    public function transform(array $ingredients = [], int $currentPage, int $itemsPerPage, int $total) : array
    {
        $items = [];
        array_map(function (Ingredient $ingredient) use (&$items) {
            $items[] = $ingredient->toArray();
        }, $ingredients);

        return $this->paginateTransform($items, $currentPage, $itemsPerPage, $total);
    }
}
