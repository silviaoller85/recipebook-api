<?php

namespace RecipeBook\Application\Service\Api\Ingredient\Find;

use RecipeBook\Domain\Model\Ingredient\IngredientFinder as DomainIngredientFinder;

final class IngredientFinder
{
    /**
     * @var DomainIngredientFinder
     */
    private $finder;

    /**
     * IngredientFinder constructor.
     * @param DomainIngredientFinder $finder
     */
    public function __construct(DomainIngredientFinder $finder)
    {
        $this->finder = $finder;
    }

    /**
     * @param array $filters
     * @param array $options
     * @return array
     */
    public function filterOrFail(array $filters = [], array $options = []) : array
    {
        return $this->finder->findBy($filters, $options);
    }
}
