<?php

namespace RecipeBook\Application\Service\Api\Ingredient\Get;

use RecipeBook\Domain\Model\Ingredient\Ingredient;

/**
 * Class IngredientResponse
 * @package RecipeBook\Application\Service\Api\Recipe\Get
 */
final class IngredientResponse
{
    /**
     * @param Ingredient $ingredient
     * @return array
     */
    public function transform(Ingredient $ingredient) : array
    {
        return $ingredient->toArray();
    }
}
