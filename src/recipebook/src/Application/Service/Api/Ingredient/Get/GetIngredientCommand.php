<?php

namespace RecipeBook\Application\Service\Api\Ingredient\Get;

/**
 * Class GetIngredientCommand
 * @package Recipe\Application\Service\Api\Ingredient\Get
 */
final class GetIngredientCommand
{
    /**
     * @var string
     */
    private $id;

    /**
     * GetIngredientCommand constructor.
     * @param string $id
     */
    public function __construct(string $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function id(): string
    {
        return $this->id;
    }
}
