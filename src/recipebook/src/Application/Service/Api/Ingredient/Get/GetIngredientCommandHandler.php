<?php

namespace RecipeBook\Application\Service\Api\Ingredient\Get;

use RecipeBook\Domain\Model\Ingredient\Ingredient;

/**
 * Class GetIngredientCommandHandler
 * @package RecipeBook\Application\Service\Api\Ingredient\Get
 */
final class GetIngredientCommandHandler
{
    /**
     * @var IngredientGetter
     */
    private $getter;

    /**
     * GetIngredientCommandHandler constructor.
     * @param IngredientGetter $getter
     */
    public function __construct(IngredientGetter $getter)
    {
        $this->getter = $getter;
    }

    /**
     * @param GetIngredientCommand $command
     * @return Ingredient
     */
    public function handle(GetIngredientCommand $command) : Ingredient
    {
        return $this->getter->getById($command->id());
    }
}
