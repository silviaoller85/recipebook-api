<?php

namespace RecipeBook\Application\Service\Api\Ingredient\Get;

use RecipeBook\Domain\Model\Ingredient\Ingredient;
use RecipeBook\Domain\Model\Ingredient\IngredientFinder as DomainIngredientFinder;

/**
 * Class IngredientGetter
 * @package RecipeBook\Application\Service\Api\Ingredient\Get
 */
final class IngredientGetter
{
    /**
     * @var DomainIngredientFinder
     */
    private $finder;

    /**
     * IngredientGetter constructor.
     * @param DomainIngredientFinder $finder
     */
    public function __construct(DomainIngredientFinder $finder)
    {
        $this->finder = $finder;
    }

    /**
     * @param string $id
     * @return Ingredient
     * @throws \RecipeBook\Domain\Model\Ingredient\IngredientNotFoundException
     */
    public function getById(string $id) : Ingredient
    {
        return $this->finder->getById($id);
    }
}
