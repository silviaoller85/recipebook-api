<?php

namespace RecipeBook\Application\Service\Api\Ingredient\Create;

use RecipeBook\Domain\Model\Unit\Unit;

final class CreateIngredientCommand
{
    /**
     * @var string
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var null|string
     */
    private $description;

    /**
     * @var Unit|null
     */
    private $unit;

    /**
     * CreateIngredientCommand constructor.
     * @param string $id
     * @param string $name
     * @param string|null $description
     * @param Unit|null $unit
     */
    public function __construct(string $id, string $name, string $description = null, Unit $unit = null)
    {
        $this->id = $id;
        $this->name = $name;
        $this->description = $description;
        $this->unit = $unit;
    }

    /**
     * @return string
     */
    public function id(): string
    {
        return $this->id;
    }

    /**
     * @return null|string
     */
    public function name(): ?string
    {
        return $this->name;
    }

    /**
     * @return null|string
     */
    public function description(): ?string
    {
        return $this->description;
    }

    /**
     * @return Unit|null
     */
    public function unit() : ?Unit
    {
        return $this->unit;
    }
}
