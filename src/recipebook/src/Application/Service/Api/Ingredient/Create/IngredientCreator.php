<?php

namespace RecipeBook\Application\Service\Api\Ingredient\Create;

use RecipeBook\Domain\Common\ValueObject\Uuid;
use RecipeBook\Domain\Model\Ingredient\Ingredient;
use RecipeBook\Domain\Model\Ingredient\IngredientRepositoryGatewayInterface;
use RecipeBook\Domain\Model\Unit\Unit;

final class IngredientCreator
{
    /**
     * @var IngredientRepositoryGatewayInterface
     */
    private $repository;

    /**
     * IngredientCreator constructor.
     * @param IngredientRepositoryGatewayInterface $repository
     */
    public function __construct(IngredientRepositoryGatewayInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param Uuid $id
     * @param string $name
     * @param string|null $description
     * @param Unit|null $unit
     * @return Ingredient
     * @throws \Exception
     */
    public function create(
        Uuid $id,
        string $name,
        string $description = null,
        Unit $unit = null
    ) : Ingredient {
        $ingredient = Ingredient::create($id, $name, $description, $unit);

        $this->repository->persist($ingredient);

        return $ingredient;
    }
}
