<?php

namespace RecipeBook\Application\Service\Api\Ingredient\Create;

use RecipeBook\Domain\Common\ValueObject\Uuid;
use RecipeBook\Domain\Model\Ingredient\Ingredient;

final class CreateIngredientCommandHandler
{
    /**
     * @var IngredientCreator
     */
    private $creator;

    /**
     * CreateIngredientCommandHandler constructor.
     * @param IngredientCreator $creator
     */
    public function __construct(IngredientCreator $creator)
    {
        $this->creator = $creator;
    }

    /**
     * @param CreateIngredientCommand $command
     * @return Ingredient
     * @throws \Exception
     */
    public function handle(CreateIngredientCommand $command) : Ingredient
    {
        return $this->creator->create(new Uuid($command->id()), $command->name(), $command->description(), $command->unit());
    }
}
