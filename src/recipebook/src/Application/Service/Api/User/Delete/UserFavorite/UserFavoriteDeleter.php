<?php

namespace RecipeBook\Application\Service\Api\User\Delete\UserFavorite;

use RecipeBook\Domain\Common\ValueObject\Uuid;
use RecipeBook\Domain\Model\Recipe\Recipe;
use RecipeBook\Domain\Model\User\User;
use RecipeBook\Domain\Model\User\UserFavorite;
use RecipeBook\Domain\Model\User\UserFavoriteRepositoryGatewayInterface;

final class UserFavoriteDeleter
{
    /**
     * @var UserFavoriteRepositoryGatewayInterface
     */
    private $repository;

    /**
     * RecipeDeleter constructor.
     * @param UserFavoriteRepositoryGatewayInterface $repository
     */
    public function __construct(UserFavoriteRepositoryGatewayInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param UserFavorite $userFavorite
     * @return bool
     */
    public function delete(UserFavorite $userFavorite) : bool
    {
        $this->repository->delete($userFavorite);

        return true;
    }
}
