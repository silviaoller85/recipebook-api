<?php

namespace RecipeBook\Application\Service\Api\User\Delete\UserFavorite;

final class DeleteUserFavoriteCommandHandler
{
    /**
     * @var UserFavoriteDeleter
     */
    private $deleter;

    /**
     * DeleteUserFavoriteCommandHandler constructor.
     * @param UserFavoriteDeleter $deleter
     */
    public function __construct(UserFavoriteDeleter $deleter)
    {
        $this->deleter = $deleter;
    }

    /**
     * @param DeleteUserFavoriteCommand $command
     * @return bool
     */
    public function handle(DeleteUserFavoriteCommand $command) : bool
    {
        return $this->deleter->delete($command->userFavorite());
    }
}
