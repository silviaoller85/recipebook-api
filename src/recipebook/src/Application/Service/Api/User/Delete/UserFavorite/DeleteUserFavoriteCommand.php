<?php

namespace RecipeBook\Application\Service\Api\User\Delete\UserFavorite;

use RecipeBook\Domain\Model\User\UserFavorite;

final class DeleteUserFavoriteCommand
{
    /**
     * @var UserFavorite
     */
    private $userFavorite;

    /**
     * @param UserFavorite $userFavorite
     */
    public function __construct(
        UserFavorite $userFavorite
    ) {
        $this->userFavorite = $userFavorite;
    }

    /**
     * @return UserFavorite
     */
    public function userFavorite(): UserFavorite
    {
        return $this->userFavorite;
    }
}
