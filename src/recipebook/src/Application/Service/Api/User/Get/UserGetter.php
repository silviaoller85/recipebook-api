<?php

namespace RecipeBook\Application\Service\Api\User\Get;

use RecipeBook\Domain\Model\User\User;
use RecipeBook\Domain\Model\User\UserFinder as DomainUserFinder;

/**
 * Class UserGetter
 * @package RecipeBook\Application\Service\Api\User\Get
 */
final class UserGetter
{
    /**
     * @var DomainUserFinder
     */
    private $finder;

    /**
     * UserGetter constructor.
     * @param DomainUserFinder $finder
     */
    public function __construct(DomainUserFinder $finder)
    {
        $this->finder = $finder;
    }

    /**
     * @param string $id
     * @return User
     * @throws \RecipeBook\Domain\Model\User\UserNotFoundException
     */
    public function getById(string $id) : User
    {
        return $this->finder->getById($id);
    }
}
