<?php

namespace RecipeBook\Application\Service\Api\User\Get;

use RecipeBook\Domain\Model\User\User;

/**
 * Class UserResponse
 * @package RecipeBook\Application\Service\Api\User\Get
 */
final class UserResponse
{
    /**
     * @param User $user
     * @return array
     */
    public function transform(User $user) : array
    {
        return $user->toArray();
    }
}
