<?php

namespace RecipeBook\Application\Service\Api\User\Get;

use RecipeBook\Domain\Model\User\User;

/**
 * Class GetUserCommandHandler
 * @package RecipeBook\Application\Service\Api\User\Get
 */
final class GetUserCommandHandler
{
    /**
     * @var UserGetter
     */
    private $getter;

    /**
     * GetUserCommandHandler constructor.
     * @param UserGetter $getter
     */
    public function __construct(UserGetter $getter)
    {
        $this->getter = $getter;
    }

    /**
     * @param GetUserCommand $command
     * @return User
     */
    public function handle(GetUserCommand $command) : User
    {
        return $this->getter->getById($command->id());
    }
}
