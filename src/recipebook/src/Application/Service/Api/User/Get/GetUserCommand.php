<?php

namespace RecipeBook\Application\Service\Api\User\Get;

/**
 * Class GetUserCommand
 * @package RecipeBook\Application\Service\Api\User\Get
 */
final class GetUserCommand
{
    /**
     * @var string
     */
    private $id;

    /**
     * GetUserCommand constructor.
     * @param string $id
     */
    public function __construct(string $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function id(): string
    {
        return $this->id;
    }
}
