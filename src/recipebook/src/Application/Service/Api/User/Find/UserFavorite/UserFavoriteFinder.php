<?php

namespace RecipeBook\Application\Service\Api\User\Find\UserFavorite;

use RecipeBook\Domain\Model\User\UserFavoriteFinder as DomainUserFavoriteFinder;

final class UserFavoriteFinder
{
    /**
     * @var DomainUserFavoriteFinder
     */
    private $finder;

    /**
     * RecipeFinder constructor.
     * @param DomainUserFavoriteFinder $finder
     */
    public function __construct(DomainUserFavoriteFinder $finder)
    {
        $this->finder = $finder;
    }

    /**
     * @param array $filters
     * @param array $options
     * @return array
     */
    public function filterOrFail(array $filters = [], array $options = []) : array
    {
        return $this->finder->findBy($filters, $options);
    }
}
