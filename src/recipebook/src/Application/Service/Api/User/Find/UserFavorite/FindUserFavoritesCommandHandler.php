<?php

namespace RecipeBook\Application\Service\Api\User\Find\UserFavorite;

final class FindUserFavoritesCommandHandler
{
    /**
     * @var UserFavoriteFinder
     */
    private $finder;

    /**
     * FindRecipesCommandHandler constructor.
     * @param UserFavoriteFinder $finder
     */
    public function __construct(UserFavoriteFinder $finder)
    {
        $this->finder = $finder;
    }

    /**
     * @param FindUserFavoritesCommand $command
     * @return array
     */
    public function handle(FindUserFavoritesCommand $command) : array
    {
        return $this->finder->filterOrFail($command->filters(), $command->options());
    }
}
