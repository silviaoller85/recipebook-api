<?php

namespace RecipeBook\Application\Service\Api\User\Find;

use RecipeBook\Domain\Model\User\UserFinder as DomainUserFinder;

final class UserFinder
{
    /**
     * @var DomainUserFinder
     */
    private $finder;

    /**
     * UserFinder constructor.
     * @param DomainUserFinder $finder
     */
    public function __construct(DomainUserFinder $finder)
    {
        $this->finder = $finder;
    }

    /**
     * @param array $filters
     * @param array $options
     * @return array
     */
    public function filterOrFail(array $filters = [], array $options = []) : array
    {
        return $this->finder->findBy($filters, $options);
    }
}
