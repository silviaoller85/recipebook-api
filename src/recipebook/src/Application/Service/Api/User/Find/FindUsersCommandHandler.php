<?php

namespace RecipeBook\Application\Service\Api\User\Find;

final class FindUsersCommandHandler
{
    /**
     * @var UserFinder
     */
    private $finder;

    /**
     * FindUsersCommandHandler constructor.
     * @param UserFinder $finder
     */
    public function __construct(UserFinder $finder)
    {
        $this->finder = $finder;
    }

    /**
     * @param FindUsersCommand $command
     * @return array
     */
    public function handle(FindUsersCommand $command) : array
    {
        return $this->finder->filterOrFail($command->filters(), $command->options());
    }
}
