<?php

namespace RecipeBook\Application\Service\Api\User\Find;

use RecipeBook\Application\Common\Transformer\PaginateResponse;
use RecipeBook\Domain\Model\User\User;

final class UsersResponse
{
    use PaginateResponse;

    /**
     * @param array $users
     * @param int $currentPage
     * @param int $itemsPerPage
     * @param int $total
     * @return array
     */
    public function transform(array $users = [], int $currentPage, int $itemsPerPage, int $total) : array
    {
        $items = [];
        array_map(function (User $user) use (&$items) {
            $items[] = $user->toArrayMinified();
        }, $users);

        return $this->paginateTransform($items, $currentPage, $itemsPerPage, $total);
    }
}
