<?php

namespace RecipeBook\Application\Service\Api\User\Create\UserFavorite;

use RecipeBook\Domain\Common\ValueObject\Uuid;
use RecipeBook\Domain\Model\User\UserFavorite;

final class CreateUserFavoriteCommandHandler
{
    /**
     * @var UserFavoriteCreator
     */
    private $creator;

    /**
     * CreateUserFavoriteCommandHandler constructor.
     * @param UserFavoriteCreator $creator
     */
    public function __construct(UserFavoriteCreator $creator)
    {
        $this->creator = $creator;
    }

    /**
     * @param CreateUserFavoriteCommand $command
     * @return UserFavorite
     * @throws \Exception
     */
    public function handle(CreateUserFavoriteCommand $command) : UserFavorite
    {
        return $this->creator->create(
            new Uuid($command->id()),
            $command->user(),
            $command->recipe()
        );
    }
}
