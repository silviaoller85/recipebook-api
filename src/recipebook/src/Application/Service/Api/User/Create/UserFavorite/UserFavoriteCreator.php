<?php

namespace RecipeBook\Application\Service\Api\User\Create\UserFavorite;

use RecipeBook\Domain\Common\ValueObject\Uuid;
use RecipeBook\Domain\Model\Recipe\Recipe;
use RecipeBook\Domain\Model\User\User;
use RecipeBook\Domain\Model\User\UserFavorite;
use RecipeBook\Domain\Model\User\UserFavoriteRepositoryGatewayInterface;

final class UserFavoriteCreator
{
    /**
     * @var UserFavoriteRepositoryGatewayInterface
     */
    private $repository;

    /**
     * RecipeCreator constructor.
     * @param UserFavoriteRepositoryGatewayInterface $repository
     */
    public function __construct(UserFavoriteRepositoryGatewayInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param Uuid $id
     * @param User $user
     * @param Recipe $recipe
     * @return UserFavorite
     * @throws \Exception
     */
    public function create(
        Uuid $id,
        User $user,
        Recipe $recipe
    ) : UserFavorite {

        $userFavorite = UserFavorite::create(
            $id,
            $user,
            $recipe
        );

        $this->repository->persist($userFavorite);

        return $userFavorite;
    }
}
