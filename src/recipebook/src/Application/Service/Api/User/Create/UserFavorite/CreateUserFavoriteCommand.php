<?php

namespace RecipeBook\Application\Service\Api\User\Create\UserFavorite;

use RecipeBook\Domain\Model\Recipe\Recipe;
use RecipeBook\Domain\Model\User\User;

final class CreateUserFavoriteCommand
{
    /**
     * @var string
     */
    private $id;

    /**
     * @var User
     */
    private $user;

    /**
     * @var Recipe
     */
    private $recipe;


    /**
     * @param string $id
     * @param User $user
     * @param Recipe $recipe
     */
    public function __construct(
        string $id,
        User $user,
        Recipe $recipe
    ) {
        $this->id = $id;
        $this->user = $user;
        $this->recipe = $recipe;
    }

    /**
     * @return string
     */
    public function id(): string
    {
        return $this->id;
    }

    /**
     * @return User
     */
    public function user(): User
    {
        return $this->user;
    }

    /**
     * @return Recipe
     */
    public function recipe(): Recipe
    {
        return $this->recipe;
    }
}
