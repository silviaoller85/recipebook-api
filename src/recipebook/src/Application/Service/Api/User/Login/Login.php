<?php

namespace RecipeBook\Application\Service\Api\User\Login;

use RecipeBook\Domain\Model\User\UserFinder as DomainUserFinder;

/**
 * Class Login
 * @package RecipeBook\Application\Service\Api\User\Login
 */
final class Login
{
    /**
     * @var DomainUserFinder
     */
    private $finder;

    /**
     * Login constructor.
     * @param DomainUserFinder $finder
     */
    public function __construct(DomainUserFinder $finder)
    {
        $this->finder = $finder;
    }

    /**
     * @param string $email
     * @param string $password
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     */
    public function credentials(string $email, string $password)
    {
        return $this->finder->credentials($email, $password);
    }
}
