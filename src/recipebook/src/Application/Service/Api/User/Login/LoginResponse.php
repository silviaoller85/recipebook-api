<?php

namespace RecipeBook\Application\Service\Api\User\Login;

use RecipeBook\Domain\Model\User\User;

/**
 * Class LoginResponse
 * @package RecipeBook\Application\Service\Api\User\Login
 */
final class LoginResponse
{
    /**
     * @param User $user
     * @param string $token
     * @return array
     */
    public function transform(User $user, string $token) : array
    {
        return [
            'data' => $user->toArrayLogin(),
            'token' => $token,
        ];
    }
}
