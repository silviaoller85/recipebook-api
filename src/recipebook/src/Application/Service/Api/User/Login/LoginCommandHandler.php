<?php

namespace RecipeBook\Application\Service\Api\User\Login;

/**
 * Class LoginCommandHandler
 * @package RecipeBook\Application\Service\Api\User\Login
 */
final class LoginCommandHandler
{
    /**
     * @var Login
     */
    private $login;

    /**
     * @param Login $login
     */
    public function __construct(Login $login)
    {
        $this->login = $login;
    }

    /**
     * @param LoginCommand $command
     *
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     * @throws \Exception
     */
    public function handle(LoginCommand $command)
    {
        return $this->login->credentials(
            $command->email(),
            $command->password()
        );
    }
}
