<?php

namespace RecipeBook\Application\Service\Api\User\Register;

use RecipeBook\Domain\Model\User\User;
use RecipeBook\Domain\Model\User\UserRepositoryGatewayInterface;

/**
 * Class Register
 * @package RecipeBook\Application\Service\Api\User\Register
 */
final class Register
{
    /**
     * @var UserRepositoryGatewayInterface
     */
    private $repository;

    /**
     * Register constructor.
     * @param UserRepositoryGatewayInterface $repository
     */
    public function __construct(UserRepositoryGatewayInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param string $id
     * @param string $name
     * @param string $email
     * @param string $password
     * @return User
     * @throws \Exception
     */
    public function create(
        string $id,
        string $name,
        string $email,
        string $password
    ) {
        $user = User::create($id, $name, $email, $password, true);

        $this->repository->persist($user);

        return $user;
    }
}
