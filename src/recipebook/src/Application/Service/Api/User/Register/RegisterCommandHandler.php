<?php

namespace RecipeBook\Application\Service\Api\User\Register;

/**
 * Class RegisterCommandHandler
 * @package RecipeBook\Application\Service\Api\User\Login
 */
final class RegisterCommandHandler
{
    /**
     * @var Register
     */
    private $register;

    /**
     * @param Register $register
     */
    public function __construct(Register $register)
    {
        $this->register = $register;
    }

    /**
     * @param RegisterCommand $command
     *
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     * @throws \Exception
     */
    public function handle(RegisterCommand $command)
    {
        return $this->register->create(
            $command->id(),
            $command->name(),
            $command->email(),
            $command->password()
        );
    }
}
