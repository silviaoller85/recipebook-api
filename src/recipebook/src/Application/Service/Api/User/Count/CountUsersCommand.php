<?php

namespace RecipeBook\Application\Service\Api\User\Count;

final class CountUsersCommand
{
    /**
     * @var array
     */
    private $filters;

    /**
     * CountUsersCommand constructor.
     * @param array $filters
     */
    public function __construct(array $filters = [])
    {
        $this->filters = $filters;
    }

    /**
     * @return array
     */
    public function filters(): array
    {
        return $this->filters;
    }
}
