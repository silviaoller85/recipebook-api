<?php

namespace RecipeBook\Application\Service\Api\User\Count;

use RecipeBook\Domain\Model\User\UserFinder as DomainUserFinder;

final class UserCounter
{
    /**
     * @var DomainUserFinder
     */
    private $finder;

    /**
     * UserFinder constructor.
     * @param DomainUserFinder $finder
     */
    public function __construct(DomainUserFinder $finder)
    {
        $this->finder = $finder;
    }

    /**
     * @param array $filters
     * @return int
     */
    public function count(array $filters = []) : int
    {
        return $this->finder->count($filters);
    }
}
