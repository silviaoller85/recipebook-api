<?php

namespace RecipeBook\Application\Service\Api\User\Count;

final class CountUsersCommandHandler
{
    /**
     * @var UserCounter
     */
    private $counter;

    /**
     * CountUsersCommandHandler constructor.
     * @param UserCounter $counter
     */
    public function __construct(UserCounter $counter)
    {
        $this->counter = $counter;
    }

    /**
     * @param CountUsersCommand $command
     * @return int
     */
    public function handle(CountUsersCommand $command) : int
    {
        return $this->counter->count($command->filters());
    }
}
