<?php

namespace RecipeBook\Application\Common\Transformer;

/**
 * Class ErrorResponseTransformer
 * @package RecipeBook\Application\Common\Transformer
 */
class ErrorResponseTransformer extends ResponseTransformer
{

    /**
     * ErrorResponseTransformer constructor.
     *
     * @param string $apiUri
     */
    public function __construct($apiUri)
    {
        parent::__construct($apiUri, 'errors');
    }


    /**
     * @param array $errors
     */
    public function write(array $errors)
    {
        $errorsCollection = [];

        do {
            $error = current($errors);

            if (is_string($error)) {
                $errorsCollection[] = ['message' => $error];
            }
        } while (next($errors));

        $this->setEmbedded($this->resource, $errorsCollection);
    }
}
