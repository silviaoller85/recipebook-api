<?php

namespace RecipeBook\Application\Common\Transformer;

/**
 * Trait PaginateResponse
 * @package RecipeBook\Application\Common\Transformer
 */
trait PaginateResponse
{
    /**
     * @param array $items
     * @param int $currentPage
     * @param int $itemsPerPage
     * @param int $total
     * @return array
     */
    public function paginateTransform(array $items = [], int $currentPage, int $itemsPerPage, int $total)
    {
        return [
            'items' => $items,
            'pagination' => $this->pagination($currentPage, $itemsPerPage, $total)
        ];
    }

    /**
     * @param int $currentPage
     * @param int $itemsPerPage
     * @param int $total
     * @return array
     */
    private function pagination(int $currentPage, int $itemsPerPage, int $total)
    {
        return [
            'page' => $currentPage,
            'itemsPerPage' => $itemsPerPage,
            'total' => $total,
            'pages' => ceil($total / $itemsPerPage)
        ];
    }
}
