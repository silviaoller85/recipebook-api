<?php

namespace RecipeBook\Application\Common\Transformer;

/**
 * Class ResponseTransformer
 * @package RecipeBook\Application\Common\Transformer
 */
abstract class ResponseTransformer
{

    /**
     * @var string
     */
    protected $resource;

    /**
     * @var string
     */
    protected $apiUri;

    /**
     * @var array
     */
    protected $data = [];


    /**
     * CustomerIssueResponseTransformer constructor.
     *
     * @param string $apiUri
     * @param string $resource
     */
    public function __construct(string $apiUri, string $resource)
    {
        $this->resource = $resource;

        $this->apiUri = $apiUri;
    }


    /**
     * @return array
     */
    public function read(): array
    {
        return $this->data;
    }

    /**
     * @param string $type
     * @param array  $data
     */
    protected function setEmbedded(string $type, array $data): void
    {
        $this->data[$type] = $data;
    }
}
