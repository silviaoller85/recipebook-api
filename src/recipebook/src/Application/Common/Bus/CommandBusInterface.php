<?php

namespace RecipeBook\Application\Common\Bus;

interface CommandBusInterface
{
    public function handle($command);
}
