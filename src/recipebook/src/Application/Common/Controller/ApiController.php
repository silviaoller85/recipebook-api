<?php

namespace RecipeBook\Application\Common\Controller;

use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\Facades\Validator;
use RecipeBook\Application\Common\Bus\CommandBus;
use RecipeBook\Domain\Model\User\User;

/**
 * Class ApiController
 * @package RecipeBook\Application\Common\Controller
 */
abstract class ApiController
{
    protected $commandBus;

    protected $resource = 'data';

    use ValidatesRequests;
    use ErrorJsonResponses;
    use JsonResponses;

    const HTTP_CREATED = 201;
    const HTTP_UPDATED = 200;
    const HTTP_DELETED = 200;
    const HTTP_UPLOADED = 200;
    const HTTP_OK = 200;
    const HTTP_NOT_FOUND = 404;
    const HTTP_ERROR = 500;

    /**
     * ApiController constructor.
     * @param CommandBus $commandBus
     */
    public function __construct(CommandBus $commandBus)
    {
        $this->commandBus = $commandBus;
    }

    /**
     * @return string
     */
    public function resource() : string
    {
        return $this->resource;
    }


    /**
     * @param array $data
     * @param array $rules
     * @throws \Exception
     */
    protected function validate(array $data, array $rules)
    {
        $res = Validator::make($data, $rules);

        if ($res->fails()) {
            throw new \Exception($res->errors()->first());
        }
    }

    /**
     * @return User|null
     */
    protected function user() : ?User
    {
        return auth()->user();
    }
}
