<?php

namespace RecipeBook\Application\Common\Controller;

use Illuminate\Http\JsonResponse;

/**
 * Trait JWTResponse
 * @package RecipeBook\Application\Common\Controller
 */
trait JWTResponse
{
    public function response($data, $status)
    {
        return JsonResponse::create(array_merge(
            [
                'access_token' => $data['token'],
                'token_type' => 'bearer',
                'expires_in' => auth()->factory()->getTTL() * 60
            ],
            [
                $this->resource() => $data['data']
            ]
        ), $status)
            ->header('Access-Control-Allow-Origin', '*')
            ->header('Access-Control-Allow-Methods', 'GET, POST, PATCH, PUT, DELETE, OPTIONS')
            ->header('Access-Control-Allow-Headers', '*');
    }
}
