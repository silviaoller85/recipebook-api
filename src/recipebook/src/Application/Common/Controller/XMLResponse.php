<?php

namespace RecipeBook\Application\Common\Controller;

use RecipeBook\Domain\Model\Recipe\Recipe;
use RecipeBook\Domain\Model\User\User;
use SimpleXMLElement;

class XMLResponse
{
    /**
     * @var string
     */
    private $url;

    /**
     * XMLResponse constructor.
     * @param string $url
     */
    public function __construct(string $url)
    {
        $this->url = $url;
    }

    /**
     * @param array $recipes
     * @param array $users
     */
    public function response(array $recipes = [], array $users = [])
    {
        $xml = new SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?><urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"></urlset>');

        array_map(function (Recipe $recipe) use (&$xml) {
            $child = $xml->addChild('url');
            $child->addChild('loc', $this->url .'/recipes/view/' . $recipe->slug());
        }, $recipes);

        array_map(function (User $user) use (&$xml) {
            $child = $xml->addChild('url');
            $child->addChild('loc', $this->url .'/users/' . $user->id());
        }, $users);

        Header('Content-type: text/xml');
        print($xml->asXML());
    }
}
