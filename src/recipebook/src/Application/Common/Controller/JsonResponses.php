<?php

namespace RecipeBook\Application\Common\Controller;

use Illuminate\Http\JsonResponse;

/**
 * Trait JsonResponses
 * @package RecipeBook\Application\Common\Controller
 */
trait JsonResponses
{
    /**
     * @param $data
     * @param $status
     * @return JsonResponse
     */
    public function response($data, $status)
    {
        return JsonResponse::create([$this->resource() => $data], $status)
            ->header('Access-Control-Allow-Origin', '*')
            ->header('Access-Control-Allow-Methods', 'GET, POST, PATCH, PUT, DELETE, OPTIONS')
            ->header('Access-Control-Allow-Headers', '*');
    }
}
