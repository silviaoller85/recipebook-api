<?php

namespace RecipeBook\Application\Common\Controller;

use Illuminate\Http\JsonResponse;
use RecipeBook\Application\Common\Transformer\ErrorResponseTransformer;
use Throwable;

/**
 * Trait ErrorJsonResponses
 * @package RecipeBook\Application\Common\Controller
 */
trait ErrorJsonResponses
{

    /**
     * @param Throwable $throwable
     * @param int       $statusCode
     * @return JsonResponse
     */
    public function errorResponse(Throwable $throwable, int $statusCode): JsonResponse
    {
        report($throwable);

        $errorResponseTransformer = $this->getErrorTransformer();

        //var_dump($throwable->getFile());

        $debug = filter_var(getenv('APP_DEBUG'), FILTER_VALIDATE_BOOLEAN);

        $message = $debug ? $throwable->getMessage() : 'Error. Contacta con el administrador.';

        $errorResponseTransformer->write([$message]);

        return JsonResponse::create($errorResponseTransformer->read(), $statusCode)
            ->header('Access-Control-Allow-Origin', '*')
            ->header('Access-Control-Allow-Methods', 'GET, POST, PATCH, PUT, DELETE, OPTIONS')
            ->header('Access-Control-Allow-Headers', '*');
    }


    /**
     * @return ErrorResponseTransformer
     */
    protected function getErrorTransformer(): ErrorResponseTransformer
    {
        return app(ErrorResponseTransformer::class);
    }
}
