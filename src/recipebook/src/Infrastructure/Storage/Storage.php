<?php

namespace RecipeBook\Infrastructure\Storage;

use RecipeBook\Domain\Model\File;

interface Storage
{
    public function __construct();
    public function upload(File $file, string $path);
    public function delete(string $path);
}
