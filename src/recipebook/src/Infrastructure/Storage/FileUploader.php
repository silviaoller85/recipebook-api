<?php

namespace RecipeBook\Infrastructure\Storage;

use Illuminate\Support\Facades\Log;
use RecipeBook\Domain\Model\File;
use RecipeBook\Domain\Model\Recipe\Recipe;
use RecipeBook\Domain\Model\Recipe\RecipeStep\RecipeStep;

class FileUploader
{
    /**
     * @var File
     */
    private $file;

    /**
     * @var null|Recipe
     */
    private $recipe;

    /**
     * @var null|RecipeStep
     */
    private $recipeStep;

    /**
     * @var Storage
     */
    private $storage;

    /**
     * FileUploader constructor.
     * @param Storage $storage
     */
    public function __construct(Storage $storage)
    {
        $this->storage = $storage;
    }

    /**
     * @param File $file
     * @param Recipe|null $recipe
     * @param RecipeStep|null $recipeStep
     * @return FileUploaded
     */
    public function upload(File $file, Recipe $recipe = null, RecipeStep $recipeStep = null) : FileUploaded
    {
        $this->file = $file;
        $this->recipe = $recipe;
        $this->recipeStep = $recipeStep;

        return $this->storage->upload($file, $this->path());
    }

    /**
     * @param string $path
     */
    public function delete(string $path)
    {
        $this->storage->delete($path);
    }

    /**
     * @return string
     */
    private function path() : string
    {
        $path = [
            'recipes',
            $this->recipe->id()->value()
        ];

        if ($this->recipeStep) {
            $path = array_merge(
                $path,
                [
                    'steps',
                    $this->recipeStep->id()->value()
                ]
            );
        }

        $path[] = time() . '.' . $this->file->extension();

        return implode('/', $path);
    }
}
