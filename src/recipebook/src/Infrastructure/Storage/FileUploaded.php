<?php

namespace RecipeBook\Infrastructure\Storage;

class FileUploaded
{
    /**
     * @var string
     */
    private $path;

    /**
     * @var string
     */
    private $url;

    /**
     * FileUploaded constructor.
     * @param string $path
     * @param string $url
     */
    private function __construct(string $path, string $url)
    {
        $this->path = $path;
        $this->url = $url;
    }

    /**
     * @param string $path
     * @param string $url
     * @return FileUploaded
     */
    public static function create(string $path, string $url) : self
    {
        return new self($path, $url);
    }

    /**
     * @return string
     */
    public function path() : string
    {
        return $this->path;
    }

    /**
     * @return string
     */
    public function url() : string
    {
        return $this->url;
    }
}
