<?php

namespace RecipeBook\Infrastructure\Storage;

use RecipeBook\Domain\Model\File;

class Local implements Storage
{
    const DISK = 'public';

    /**
     * @var \Illuminate\Support\Facades\Storage
     */
    private $storage;

    /**
     * Local constructor.
     */
    public function __construct()
    {
        $this->storage = \Illuminate\Support\Facades\Storage::disk(self::DISK);
    }

    /**
     * @param File $file
     * @param string $path
     * @return FileUploaded
     */
    public function upload(File $file, string $path) : FileUploaded
    {
        $content = file_get_contents($file->path());

        $this->storage->put($path, $content);
        $url = $this->storage->url($path);

        return FileUploaded::create($path, $url);
    }

    /**
     * @param string $path
     */
    public function delete(string $path)
    {
        $this->storage->delete($path);
    }
}
