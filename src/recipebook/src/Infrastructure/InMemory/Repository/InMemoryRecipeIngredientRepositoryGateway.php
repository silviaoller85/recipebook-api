<?php

namespace RecipeBook\Infrastructure\InMemory\Repository;

use RecipeBook\Domain\Model\Recipe\RecipeIngredient\RecipeIngredient;
use RecipeBook\Domain\Model\Recipe\RecipeIngredient\RecipeIngredientRepositoryGatewayInterface;
use RecipeBook\Infrastructure\InMemory\InMemory;

final class InMemoryRecipeIngredientRepositoryGateway extends InMemory implements RecipeIngredientRepositoryGatewayInterface
{

    /**
     * @param array $filters
     * @param array $options
     *
     * @return array
     * @throws \Exception
     */
    public function filter(array $filters, array $options = []): ?array
    {
        if ($filters && isset($filters['error'])) {
            return null;
        }

        return[
            $this->findOneBy($filters),
            $this->findOneBy($filters)
        ];
    }

    /**
     * @param $entity
     */
    public function persist($entity)
    {
        // TODO: Implement persist() method.
    }

    /**
     * @param $id
     *
     * @return null|mixed
     */
    public function find($id)
    {
        // TODO: Implement find() method.
    }

    /**
     * @param string $id
     * @return null|RecipeIngredient
     * @throws \Exception
     */
    public function findOneById(string $id): ?RecipeIngredient
    {
        if ($id == -999) {
            return null;
        }

        return self::recipeIngredient();
    }

    /**
     * @param array $criteria
     * @return void
     */
    public function findBy(array $criteria)
    {
        // TODO: Implement findBy() method.
    }

    public function count(array $filters = []) : int
    {
        return rand(1, 10);
    }

    /**
     * @param array $criteria
     * @return null|mixed
     * @throws \Exception
     */
    public function findOneBy(array $criteria)
    {
        if (isset($criteria['id']) && $criteria['id'] == -999) {
            return null;
        }

        return self::recipeIngredient();
    }

    public function delete($entity)
    {
        // TODO: Implement delete() method.
    }
}
