<?php

namespace RecipeBook\Infrastructure\InMemory\Repository;

use RecipeBook\Domain\Model\Thermomix\ThermomixModel\ThermomixModel;
use RecipeBook\Domain\Model\Thermomix\ThermomixModel\ThermomixModelRepositoryGatewayInterface;
use RecipeBook\Infrastructure\InMemory\InMemory;

final class InMemoryThermomixModelRepositoryGateway extends InMemory implements ThermomixModelRepositoryGatewayInterface
{

    /**
     * @param array $filters
     * @param array $options
     *
     * @return array
     * @throws \Exception
     */
    public function filter(array $filters, array $options = []): ?array
    {
        if ($filters && isset($filters['error'])) {
            return null;
        }

        return[
            $this->findOneBy($filters),
            $this->findOneBy($filters)
        ];
    }

    /**
     * @param $entity
     */
    public function persist($entity)
    {
        // TODO: Implement persist() method.
    }

    /**
     * @param $id
     *
     * @return null|mixed
     */
    public function find($id)
    {
        // TODO: Implement find() method.
    }

    /**
     * @param string $id
     * @return null|ThermomixModel
     * @throws \Exception
     */
    public function findOneById(string $id): ?ThermomixModel
    {
        if ($id == -999) {
            return null;
        }

        return self::thermomixModel();
    }

    /**
     * @param array $criteria
     * @return void
     */
    public function findBy(array $criteria)
    {
        // TODO: Implement findBy() method.
    }

    public function count(array $filters = []) : int
    {
        return rand(1, 10);
    }

    /**
     * @param array $criteria
     * @return null|mixed
     * @throws \Exception
     */
    public function findOneBy(array $criteria)
    {
        if (isset($criteria['id']) && $criteria['id'] == -999) {
            return null;
        }

        return self::thermomixModel();
    }
}
