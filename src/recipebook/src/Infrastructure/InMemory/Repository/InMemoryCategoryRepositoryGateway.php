<?php

namespace RecipeBook\Infrastructure\InMemory\Repository;

use RecipeBook\Domain\Model\Category\Category;
use RecipeBook\Domain\Model\Category\CategoryRepositoryGatewayInterface;
use RecipeBook\Infrastructure\InMemory\InMemory;

final class InMemoryCategoryRepositoryGateway extends InMemory implements CategoryRepositoryGatewayInterface
{

    /**
     * @param array $filters
     * @param array $options
     *
     * @return array
     * @throws \Exception
     */
    public function filter(array $filters, array $options = []): ?array
    {
        if ($filters && isset($filters['error'])) {
            return null;
        }

        return[
            $this->findOneBy($filters),
            $this->findOneBy($filters)
        ];
    }

    /**
     * @param $entity
     */
    public function persist($entity)
    {
        // TODO: Implement persist() method.
    }

    /**
     * @param $id
     *
     * @return null|mixed
     */
    public function find($id)
    {
        // TODO: Implement find() method.
    }

    /**
     * @param string $id
     * @return null|Category
     * @throws \Exception
     */
    public function findOneById(string $id): ?Category
    {
        if ($id == -999) {
            return null;
        }

        return self::category();
    }

    /**
     * @param array $criteria
     * @return void
     */
    public function findBy(array $criteria)
    {
        // TODO: Implement findBy() method.
    }

    public function count(array $filters = []) : int
    {
        return rand(1, 10);
    }

    /**
     * @param array $criteria
     * @return null|mixed
     * @throws \Exception
     */
    public function findOneBy(array $criteria)
    {
        if (isset($criteria['id']) && $criteria['id'] == -999) {
            return null;
        }

        return self::category();
    }
}
