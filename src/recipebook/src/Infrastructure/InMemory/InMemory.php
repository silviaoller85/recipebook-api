<?php

namespace RecipeBook\Infrastructure\InMemory;

use Faker\Factory;
use RecipeBook\Domain\Common\ValueObject\Uuid;
use RecipeBook\Domain\Model\Category\Category;
use RecipeBook\Domain\Model\Ingredient\Ingredient;
use RecipeBook\Domain\Model\Recipe\Recipe;
use RecipeBook\Domain\Model\Recipe\RecipeIngredient\RecipeIngredient;
use RecipeBook\Domain\Model\Recipe\RecipePhoto\RecipePhoto;
use RecipeBook\Domain\Model\Recipe\RecipeStep\RecipeStep;
use RecipeBook\Domain\Model\Recipe\RecipeStep\RecipeStepThermomixAction;
use RecipeBook\Domain\Model\Thermomix\ThermomixAction\ThermomixAction;
use RecipeBook\Domain\Model\Thermomix\ThermomixModel\ThermomixModel;
use RecipeBook\Domain\Model\Unit\Unit;
use RecipeBook\Domain\Model\User\User;
use RecipeBook\Domain\Model\User\UserFavorite;

abstract class InMemory
{
    /**
     * @return Category
     * @throws \Exception
     */
    public static function category() : Category
    {
        return new Category(
            Uuid::random(),
            Factory::create()->word,
            Factory::create()->slug
        );
    }

    /**
     * @return User
     * @throws \Exception
     */
    public static function user() : User
    {
        return new User(
            Uuid::random(),
            Factory::create()->word,
            Factory::create()->email,
            Factory::create()->password,
            Factory::create()->boolean
        );
    }

    /**
     * @return Unit
     * @throws \Exception
     */
    public static function unit() : Unit
    {
        return new Unit(
            Uuid::random(),
            Factory::create()->word,
            Factory::create()->text
        );
    }

    /**
     * @return Ingredient
     * @throws \Exception
     */
    public static function ingredient() : Ingredient
    {
        return new Ingredient(
            Uuid::random(),
            Factory::create()->word,
            Factory::create()->text,
            self::unit()
        );
    }

    /**
     * @return ThermomixModel
     * @throws \Exception
     */
    public static function thermomixModel() : ThermomixModel
    {
        return new ThermomixModel(
            Uuid::random(),
            Factory::create()->word,
            Factory::create()->text
        );
    }

    /**
     * @return ThermomixAction
     * @throws \Exception
     */
    public static function thermomixAction() : ThermomixAction
    {
        return new ThermomixAction(
            Uuid::random(),
            Factory::create()->word,
            Factory::create()->text
        );
    }

    /**
     * @return Recipe
     * @throws \Exception
     */
    public static function recipe() : Recipe
    {
        return new Recipe(
            Uuid::random(),
            self::user(),
            Factory::create()->word,
            Factory::create()->text,
            Factory::create()->numberBetween(120),
            Factory::create()->numberBetween(120),
            Factory::create()->numberBetween(10),
            true
        );
    }

    /**
     * @return RecipeIngredient
     * @throws \Exception
     */
    public static function recipeIngredient() : RecipeIngredient
    {
        return new RecipeIngredient(
            Uuid::random(),
            self::recipe(),
            self::ingredient(),
            Factory::create()->numberBetween(120),
            self::unit()
        );
    }

    /**
     * @return RecipeStep
     * @throws \Exception
     */
    public static function recipeStep() : RecipeStep
    {
        return new RecipeStep(
            Uuid::random(),
            self::recipe(),
            Factory::create()->numberBetween(20),
            Factory::create()->word,
            Factory::create()->text
        );
    }

    /**
     * @return RecipeStepThermomixAction
     * @throws \Exception
     */
    public static function recipeStepThermomixAction() : RecipeStepThermomixAction
    {
        return new RecipeStepThermomixAction(
            Uuid::random(),
            self::recipeStep(),
            self::thermomixAction(),
            Factory::create()->numberBetween(20)
        );
    }

    /**
     * @return UserFavorite
     * @throws \Exception
     */
    public static function userFavorite() : UserFavorite
    {
        return new UserFavorite(
            Uuid::random(),
            self::user(),
            self::recipe()
        );
    }

    /**
     * @return RecipePhoto
     * @throws \Exception
     */
    public static function recipePhoto() : RecipePhoto
    {
        return new RecipePhoto(
            Uuid::random(),
            self::recipe(),
            null,
            Factory::create()->text,
            Factory::create()->url,
            1
        );
    }
}
