<?php

namespace RecipeBook\Infrastructure\Doctrine;

use Doctrine\ORM\QueryBuilder;

/**
 * Trait PaginateQuery
 * @package RecipeBook\Infrastructure\Doctrine
 */
trait PaginateQuery
{
    /**
     * @param array $pagination
     * @param       $qb
     */
    private function preparePagination(array $pagination, QueryBuilder $qb): void
    {
        $pagination = array_merge(['page' => 0, 'itemsPerPage' => 999999,], $pagination);

        $pagination = array_map(
            function ($option) {
                return (int)$option;
            },
            $pagination
        );

        --$pagination['page'];


        $pagination['page'] = ($pagination['page'] < 1) ? 0 : $pagination['page'] * $pagination['itemsPerPage'];

        $qb->setFirstResult($pagination['page'])->setMaxResults($pagination['itemsPerPage']);
    }
}
