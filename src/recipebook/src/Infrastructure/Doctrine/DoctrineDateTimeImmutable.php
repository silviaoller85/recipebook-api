<?php

namespace RecipeBook\Infrastructure\Doctrine;

use DateTimeImmutable;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\DateTimeType;

class DoctrineDateTimeImmutable extends DateTimeType
{
    /**
     * {@inheritdoc}
     */
    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        if ($value === null || $value instanceof \DateTimeInterface) {
            return $value;
        }

        return DateTimeImmutable::createFromMutable(parent::convertToPHPValue($value, $platform));
    }
}
