<?php

namespace RecipeBook\Infrastructure\Doctrine\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use RecipeBook\Domain\Model\User\User;
use RecipeBook\Domain\Model\User\UserFavoriteRepositoryGatewayInterface;
use RecipeBook\Infrastructure\Doctrine\PaginateQuery;

class DoctrineUserFavoriteRepositoryGateway extends EntityRepository implements UserFavoriteRepositoryGatewayInterface
{
    use PaginateQuery;

    /**
     * @param array $filters
     * @param array $options
     *
     * @return array
     */
    public function filter(array $filters, array $options = []): ?array
    {
        $qb = $this->_createQuery($filters);

        if (array_key_exists('_pagination', $filters)) {
            $this->preparePagination($filters['_pagination'], $qb);
        }

        return $qb->getQuery()->getResult();
    }

    /**
     * @param string $id
     * @return User|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findOneById(string $id): ?User
    {
        $qb = $this->_createQuery();

        $qb->andWhere('userFavorite.id = :id')
            ->setParameter('id', $id);

        return $qb->getQuery()->getOneOrNullResult();
    }

    /**
     * @param array $filters
     *
     * @return int
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function count(array $filters): int
    {
        $qb = $this->_createQuery($filters);

        $qb->select('COUNT(DISTINCT(userFavorite.id))');

        $count = $qb->getQuery()->getSingleScalarResult();

        return $count;
    }

    /**
     * @param $entity
     */
    public function persist($entity)
    {
        $this->_em->persist($entity);
    }

    /**
     * @param $entity
     */
    public function delete($entity)
    {
        $this->_em->remove($entity);
        $this->_em->flush();
    }

    /**
     * @param array $filters
     * @return \Doctrine\ORM\QueryBuilder
     */
    private function _createQuery(array $filters = []) : QueryBuilder
    {
        $qb = $this->createQueryBuilder('userFavorite');

        if (array_key_exists('id', $filters)) {
            $qb->andWhere('userFavorite.id = :id')->setParameter('id', mb_strtolower($filters['id']));
        }

        if (array_key_exists('user', $filters)) {
            $qb->andWhere('userFavorite.user = :user')->setParameter('user', $filters['user']);
        }

        if (array_key_exists('recipe', $filters)) {
            $qb->andWhere('userFavorite.recipe = :recipe')->setParameter('recipe', $filters['recipe']);
        }

        return $qb;
    }
}
