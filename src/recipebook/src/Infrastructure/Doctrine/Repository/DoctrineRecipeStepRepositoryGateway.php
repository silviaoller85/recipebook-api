<?php

namespace RecipeBook\Infrastructure\Doctrine\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use RecipeBook\Domain\Model\Recipe\RecipeStep\RecipeStep;
use RecipeBook\Domain\Model\Recipe\RecipeStep\RecipeStepRepositoryGatewayInterface;
use RecipeBook\Infrastructure\Doctrine\PaginateQuery;

class DoctrineRecipeStepRepositoryGateway extends EntityRepository implements RecipeStepRepositoryGatewayInterface
{
    use PaginateQuery;

    /**
     * @param array $filters
     * @param array $options
     *
     * @return array
     */
    public function filter(array $filters, array $options = []): ?array
    {
        $qb = $this->_createQuery($filters);

        if (array_key_exists('_pagination', $filters)) {
            $this->preparePagination($filters['_pagination'], $qb);
        }

        return $qb->getQuery()->getResult();
    }

    /**
     * @param string $id
     * @return RecipeStep|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findOneById(string $id): ?RecipeStep
    {
        $qb = $this->_createQuery();

        $qb->andWhere('recipeStep.id = :id')
            ->setParameter('id', $id);

        return $qb->getQuery()->getOneOrNullResult();
    }

    /**
     * @param array $filters
     *
     * @return int
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function count(array $filters): int
    {
        $qb = $this->_createQuery($filters);

        $qb->select('COUNT(DISTINCT(recipeStep.id))');

        $count = $qb->getQuery()->getSingleScalarResult();

        return $count;
    }

    /**
     * @param $entity
     */
    public function persist($entity)
    {
        $this->_em->persist($entity);
    }

    /**
     * @param $entity
     */
    public function delete($entity)
    {
        $this->_em->remove($entity);
        $this->_em->flush();
    }

    /**
     * @param array $filters
     * @return \Doctrine\ORM\QueryBuilder
     */
    private function _createQuery(array $filters = []) : QueryBuilder
    {
        $qb = $this->createQueryBuilder('recipeStep');

        if (array_key_exists('id', $filters)) {
            $qb->andWhere('recipeStep.id = :id')->setParameter('id', mb_strtolower($filters['id']));
        }

        if (array_key_exists('name', $filters)) {
            $qb->andWhere('recipeStep.name LIKE LOWER(:name)')->setParameter('name', '%'. mb_strtolower($filters['name']).'%');
        }

        return $qb;
    }
}
