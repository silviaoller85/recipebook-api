<?php

namespace RecipeBook\Infrastructure\Doctrine\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use RecipeBook\Domain\Model\Recipe\Recipe;
use RecipeBook\Domain\Model\Recipe\RecipeRepositoryGatewayInterface;
use RecipeBook\Infrastructure\Doctrine\PaginateQuery;

class DoctrineRecipeRepositoryGateway extends EntityRepository implements RecipeRepositoryGatewayInterface
{
    use PaginateQuery;

    /**
     * @param array $filters
     * @param array $options
     *
     * @return array
     */
    public function filter(array $filters, array $options = []): ?array
    {
        $qb = $this->_createQuery($filters);

        if (array_key_exists('orderBy', $options)) {
            if (array_key_exists('field', $options['orderBy'])) {
                $qb->orderBy('recipe.'.$options['orderBy']['field'], $options['orderBy']['order']);
            } else {
                foreach ($options['orderBy'] as $orderBy) {
                    $qb->addOrderBy('recipe.'.$orderBy['field'], $orderBy['order']);
                }
            }
        }

        if (array_key_exists('_pagination', $filters)) {
            $this->preparePagination($filters['_pagination'], $qb);
        }

        return $qb->getQuery()->getResult();
    }

    /**
     * @param string $id
     * @return Recipe|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findOneById(string $id): ?Recipe
    {
        $qb = $this->_createQuery();

        $qb->andWhere('recipe.id = :id')
            ->setParameter('id', $id);

        return $qb->getQuery()->getOneOrNullResult();
    }

    /**
     * @param array $filters
     *
     * @return int
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function count(array $filters): int
    {
        $qb = $this->_createQuery($filters);

        $qb->select('COUNT(DISTINCT(recipe.id))');

        $count = $qb->getQuery()->getSingleScalarResult();

        return $count;
    }

    /**
     * @param $entity
     */
    public function persist($entity)
    {
        $this->_em->persist($entity);
    }

    /**
     * @param $entity
     */
    public function delete($entity)
    {
        $this->_em->remove($entity);
        $this->_em->flush();
    }

    /**
     * @param array $filters
     * @return \Doctrine\ORM\QueryBuilder
     */
    private function _createQuery(array $filters = []) : QueryBuilder
    {
        $qb = $this->createQueryBuilder('recipe');

        if (array_key_exists('id', $filters)) {
            $qb->andWhere('recipe.id = :id')->setParameter('id', mb_strtolower($filters['id']));
        }

        if (array_key_exists('name', $filters)) {
            $qb->andWhere('recipe.name LIKE LOWER(:name)')->setParameter('name', '%'. mb_strtolower($filters['name']).'%');
        }

        if (array_key_exists('slug', $filters)) {
            $qb->andWhere('recipe.slug = :slug')->setParameter('slug', mb_strtolower($filters['slug']));
        }

        if (array_key_exists('userId', $filters)) {
            $qb->leftJoin('recipe.user', 'user');
            $qb->andWhere('user.id = :userId')->setParameter('userId', $filters['userId']);
        }

        if (array_key_exists('categoriesId', $filters)) {
            $qb->leftJoin('recipe.categories', 'categories');
            $qb->andWhere("categories.id IN ('".implode("','", $filters['categoriesId'])."')");
        }

        if (array_key_exists('isThermomix', $filters)) {
            $isThermomix = filter_var($filters['isThermomix'], FILTER_VALIDATE_BOOLEAN);
            if ($isThermomix) {
                $qb->andWhere('recipe.isThermomix = :isThermomix')->setParameter('isThermomix', $isThermomix);
            }
        }


        return $qb;
    }
}
