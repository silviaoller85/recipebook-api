<?php

namespace RecipeBook\Infrastructure\Doctrine\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use RecipeBook\Domain\Model\Thermomix\ThermomixModel\ThermomixModel;
use RecipeBook\Domain\Model\Thermomix\ThermomixModel\ThermomixModelRepositoryGatewayInterface;
use RecipeBook\Infrastructure\Doctrine\PaginateQuery;

class DoctrineThermomixModelRepositoryGateway extends EntityRepository implements ThermomixModelRepositoryGatewayInterface
{
    use PaginateQuery;

    /**
     * @param array $filters
     * @param array $options
     *
     * @return array
     */
    public function filter(array $filters, array $options = []): ?array
    {
        $qb = $this->_createQuery($filters);

        if (array_key_exists('orderBy', $options)) {
            if (array_key_exists('field', $options['orderBy'])) {
                $qb->orderBy('thermomixModel.'.$options['orderBy']['field'], $options['orderBy']['order']);
            } else {
                foreach ($options['orderBy'] as $orderBy) {
                    $qb->addOrderBy('thermomixModel.'.$orderBy['field'], $orderBy['order']);
                }
            }
        }

        if (array_key_exists('_pagination', $filters)) {
            $this->preparePagination($filters['_pagination'], $qb);
        }

        return $qb->getQuery()->getResult();
    }

    /**
     * @param string $id
     * @return ThermomixModel|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findOneById(string $id): ?ThermomixModel
    {
        $qb = $this->_createQuery();

        $qb->andWhere('thermomixModel.id = :id')
            ->setParameter('id', $id);

        return $qb->getQuery()->getOneOrNullResult();
    }

    /**
     * @param array $filters
     *
     * @return int
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function count(array $filters): int
    {
        $qb = $this->_createQuery($filters);

        $qb->select('COUNT(DISTINCT(thermomixModel.id))');

        $count = $qb->getQuery()->getSingleScalarResult();

        return $count;
    }

    /**
     * @param $entity
     */
    public function persist($entity)
    {
        $this->_em->persist($entity);
    }

    /**
     * @param array $filters
     * @return \Doctrine\ORM\QueryBuilder
     */
    private function _createQuery(array $filters = []) : QueryBuilder
    {
        $qb = $this->createQueryBuilder('thermomixModel');

        if (array_key_exists('id', $filters)) {
            $qb->andWhere('thermomixModel.id = :id')->setParameter('id', mb_strtolower($filters['id']));
        }

        if (array_key_exists('name', $filters)) {
            $qb->andWhere('thermomixModel.name LIKE LOWER(:name)')->setParameter('name', '%'. mb_strtolower($filters['name']).'%');
        }

        return $qb;
    }
}
