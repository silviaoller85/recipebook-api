<?php

namespace RecipeBook\Infrastructure\Doctrine\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use RecipeBook\Domain\Model\Ingredient\Ingredient;
use RecipeBook\Domain\Model\Ingredient\IngredientRepositoryGatewayInterface;
use RecipeBook\Infrastructure\Doctrine\PaginateQuery;

class DoctrineIngredientRepositoryGateway extends EntityRepository implements IngredientRepositoryGatewayInterface
{
    use PaginateQuery;

    /**
     * @param array $filters
     * @param array $options
     *
     * @return array
     */
    public function filter(array $filters, array $options = []): ?array
    {
        $qb = $this->_createQuery($filters);

        if (array_key_exists('orderBy', $options)) {
            if (array_key_exists('field', $options['orderBy'])) {
                $qb->orderBy('ingredient.'.$options['orderBy']['field'], $options['orderBy']['order']);
            } else {
                foreach ($options['orderBy'] as $orderBy) {
                    $qb->addOrderBy('ingredient.'.$orderBy['field'], $orderBy['order']);
                }
            }
        }

        if (array_key_exists('_pagination', $filters)) {
            $this->preparePagination($filters['_pagination'], $qb);
        }

        return $qb->getQuery()->getResult();
    }

    /**
     * @param string $id
     * @return Ingredient|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findOneById(string $id): ?Ingredient
    {
        $qb = $this->_createQuery();

        $qb->andWhere('ingredient.id = :id')
            ->setParameter('id', $id);

        return $qb->getQuery()->getOneOrNullResult();
    }

    /**
     * @param array $filters
     *
     * @return int
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function count(array $filters): int
    {
        $qb = $this->_createQuery($filters);

        $qb->select('COUNT(DISTINCT(ingredient.id))');

        $count = $qb->getQuery()->getSingleScalarResult();

        return $count;
    }

    /**
     * @param $entity
     */
    public function persist($entity)
    {
        $this->_em->persist($entity);
    }

    /**
     * @param array $filters
     * @return \Doctrine\ORM\QueryBuilder
     */
    private function _createQuery(array $filters = []) : QueryBuilder
    {
        $qb = $this->createQueryBuilder('ingredient');

        if (array_key_exists('id', $filters)) {
            $qb->andWhere('ingredient.id = :id')->setParameter('id', mb_strtolower($filters['id']));
        }

        if (array_key_exists('name', $filters)) {
            $qb->andWhere('ingredient.name LIKE LOWER(:name)')->setParameter('name', '%'. mb_strtolower($filters['name']).'%');
        }

        return $qb;
    }
}
