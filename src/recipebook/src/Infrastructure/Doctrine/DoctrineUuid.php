<?php

namespace RecipeBook\Infrastructure\Doctrine;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\StringType;
use RecipeBook\Domain\Common\ValueObject\Uuid;

class DoctrineUuid extends StringType
{
    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        return parent::convertToPHPValue(Uuid::fromString($value), $platform);
    }
}
