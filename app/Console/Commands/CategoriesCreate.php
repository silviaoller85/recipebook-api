<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use RecipeBook\Application\Common\Bus\CommandBus;
use RecipeBook\Application\Service\Api\Category\Create\CreateCategoryCommand;
use RecipeBook\Application\Service\Api\Category\Find\FindCategoriesCommand;
use RecipeBook\Domain\Common\ValueObject\Uuid;

class CategoriesCreate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'categories:create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creating categories and checking before if exists';


    const CATEGORIES = [
        'Pasta',
        'Arròs',
        'Postres',
        'Amanides',
        'Carns',
        'Peix',
        'Verdures',
        'Begudes',
        'Vegà',
        'Pizzes',
        'Llegums'
    ];

    /**
     * @var CommandBus
     */
    private $commandBus;

    /**
     * Create a new command instance.
     *
     * @param CommandBus $commandBus
     */
    public function __construct(CommandBus $commandBus)
    {
        parent::__construct();

        $this->commandBus = $commandBus;
    }

    /**
     * Execute the console command.
     * @return mixed
     * @throws \Exception
     */
    public function handle()
    {
        foreach (self::CATEGORIES as $category) {
            $exists = $this->commandBus->handle(new FindCategoriesCommand(['name' => $category]));
            if (!$exists) {
                $this->commandBus->handle(new CreateCategoryCommand(
                    Uuid::random(),
                    $category
                ));

                $this->info($category . ' created');
            }
        }
    }
}
