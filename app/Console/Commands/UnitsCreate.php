<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use RecipeBook\Application\Common\Bus\CommandBus;
use RecipeBook\Application\Service\Api\Unit\Create\CreateUnitCommand;
use RecipeBook\Application\Service\Api\Unit\Find\FindUnitsCommand;
use RecipeBook\Domain\Common\ValueObject\Uuid;

class UnitsCreate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'units:create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creating units and checking before if exists';

    const UNITS = [
        'unitat',
        'gram',
        'cartró',
        'cassó',
        'cullerada',
        'culleradeta',
        'filet',
        'fulla',
        'llauna',
        'tall',
        'unça',
        'paquet',
        'peça',
        'grapat',
        'branca',
        'llesca',
        'rodanxa',
        'sobre',
        'terrina',
        'tassa',
        'got',
        'ració',
        'bot'
    ];

    /**
     * @var CommandBus
     */
    private $commandBus;

    /**
     * Create a new command instance.
     *
     * @param CommandBus $commandBus
     */
    public function __construct(CommandBus $commandBus)
    {
        parent::__construct();

        $this->commandBus = $commandBus;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Exception
     */
    public function handle()
    {
        foreach (self::UNITS as $unit) {
            $exists = $this->commandBus->handle(new FindUnitsCommand(['name' => $unit]));
            if (!$exists) {
                $this->commandBus->handle(new CreateUnitCommand(
                    Uuid::random(),
                    $unit
                ));

                $this->info($unit . ' created');
            }
        }
    }
}
