<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use RecipeBook\Application\Common\Bus\CommandBus;
use RecipeBook\Application\Service\Api\Ingredient\Create\CreateIngredientCommand;
use RecipeBook\Application\Service\Api\Ingredient\Find\FindIngredientsCommand;
use RecipeBook\Domain\Common\ValueObject\Uuid;

class ImportIngredients extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ingredients:import';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import ingredients from openfoodsfacts.org';

    /**
     * @var CommandBus
     */
    private $commandBus;

    /**
     * Create a new command instance.
     *
     * @param CommandBus $commandBus
     */
    public function __construct(CommandBus $commandBus)
    {
        $this->commandBus = $commandBus;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Exception
     */
    public function handle()
    {
        $data = json_decode(file_get_contents('https://es.openfoodfacts.org/ingredients.json'));
        $ingredients = $this->commandBus->handle(new FindIngredientsCommand());

        try {
            foreach ($data->tags as $ingredient) {
                $name = ucfirst(str_replace('-', ' ', $ingredient->name));

                if ($name && strlen($name) <= 200) {
                    $exists = self::exists($ingredients, $name);

                    if (!$exists) {
                        $this->commandBus->handle(new CreateIngredientCommand(
                            Uuid::random(),
                            $name
                        ));

                        $this->info($name . ' creat');
                    }
                }
            }
        } catch (\Exception $exception) {
            $this->error($exception->getMessage());
        }
    }

    private static function exists(array $ingredients, $name)
    {
        foreach ($ingredients as $item) {
            if ($item->name() == $name) {
                return true;
            }
        }

        return false;
    }
}
