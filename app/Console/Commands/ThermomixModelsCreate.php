<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use RecipeBook\Application\Common\Bus\CommandBus;
use RecipeBook\Application\Service\Api\Thermomix\Create\ThermomixModel\CreateThermomixModelCommand;
use RecipeBook\Application\Service\Api\Thermomix\Find\ThermomixModel\FindThermomixModelsCommand;
use RecipeBook\Domain\Common\ValueObject\Uuid;

class ThermomixModelsCreate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'thermomixModels:create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creating thermomixModels and checking before if exists';

    const ThermomixModelS = [
        'TM31',
        'TM5',
        'TM6'
    ];

    /**
     * @var CommandBus
     */
    private $commandBus;

    /**
     * Create a new command instance.
     *
     * @param CommandBus $commandBus
     */
    public function __construct(CommandBus $commandBus)
    {
        parent::__construct();

        $this->commandBus = $commandBus;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Exception
     */
    public function handle()
    {
        foreach (self::ThermomixModelS as $thermomixModel) {
            $exists = $this->commandBus->handle(new FindThermomixModelsCommand(['name' => $thermomixModel]));
            if (!$exists) {
                $this->commandBus->handle(new CreateThermomixModelCommand(
                    Uuid::random(),
                    $thermomixModel
                ));

                $this->info($thermomixModel . ' created');
            }
        }
    }
}
