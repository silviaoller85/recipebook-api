<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use RecipeBook\Application\Common\Bus\CommandBus;
use RecipeBook\Application\Service\Api\Ingredient\Create\CreateIngredientCommand;
use RecipeBook\Application\Service\Api\Ingredient\Find\FindIngredientsCommand;
use RecipeBook\Application\Service\Api\Unit\Find\FindUnitsCommand;
use RecipeBook\Domain\Common\ValueObject\Uuid;

class IngredientsCreate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ingredients:create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creating ingredients and checking before if exists';

    const INGREDIENTS = [
        'Sal' => 'gram',
        'Arròs' => 'gram'
    ];

    /**
     * @var CommandBus
     */
    private $commandBus;

    /**
     * Create a new command instance.
     *
     * @param CommandBus $commandBus
     */
    public function __construct(CommandBus $commandBus)
    {
        parent::__construct();

        $this->commandBus = $commandBus;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Exception
     */
    public function handle()
    {
        foreach (self::INGREDIENTS as $ingredient => $unit) {
            $exists = $this->commandBus->handle(new FindIngredientsCommand(['name' => $ingredient]));
            if (!$exists) {
                $units = $this->commandBus->handle(new FindUnitsCommand(['name' => $unit]));

                if ($units) {
                    $this->commandBus->handle(new CreateIngredientCommand(
                        Uuid::random(),
                        $ingredient,
                        null,
                        $units[0]
                    ));

                    $this->info($ingredient . ' created');
                } else {
                    $this->info($ingredient . ' ERROR');
                }
            }
        }
    }
}
