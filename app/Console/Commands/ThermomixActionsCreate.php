<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use RecipeBook\Application\Common\Bus\CommandBus;
use RecipeBook\Application\Service\Api\Thermomix\Create\ThermomixAction\CreateThermomixActionCommand;
use RecipeBook\Application\Service\Api\Thermomix\Find\ThermomixAction\FindThermomixActionsCommand;
use RecipeBook\Application\Service\Api\Thermomix\Find\ThermomixModel\FindThermomixModelsCommand;
use RecipeBook\Domain\Common\ValueObject\Uuid;

class ThermomixActionsCreate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'thermomixActions:create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creating thermomixActions and checking before if exists';

    const THERMOMIXACTIONS = [
        'minutos' => [
            'TM31',
            'TM5',
            'TM6'
        ],
        'segundos' => [
            'TM31',
            'TM5',
            'TM6'
        ],
        '37º' => [
            'TM31',
            'TM5',
            'TM6'
        ],
        '50º' => [
            'TM31',
            'TM5',
            'TM6'
        ],
        '60º' => [
            'TM31',
            'TM5',
            'TM6'
        ],
        '70º' => [
            'TM31',
            'TM5',
            'TM6'
        ],
        '80º' => [
            'TM31',
            'TM5',
            'TM6'
        ],
        '90º' => [
            'TM31',
            'TM5',
            'TM6'
        ],
        '100º' => [
            'TM31',
            'TM5',
            'TM6'
        ],
        'temperatura Varoma' => [
            'TM31',
            'TM5',
            'TM6'
        ],
        'velocidad 1' => [
            'TM31',
            'TM5',
            'TM6'
        ],
        'velocidad 2' => [
            'TM31',
            'TM5',
            'TM6'
        ],
        'velocidad 3' => [
            'TM31',
            'TM5',
            'TM6'
        ],
        'velocidad 4' => [
            'TM31',
            'TM5',
            'TM6'
        ],
        'velocidad 5' => [
            'TM31',
            'TM5',
            'TM6'
        ],
        'velocidad 6' => [
            'TM31',
            'TM5',
            'TM6'
        ],
        'velocidad 7' => [
            'TM31',
            'TM5',
            'TM6'
        ],
        'velocidad 8' => [
            'TM31',
            'TM5',
            'TM6'
        ],
        'velocidad 9' => [
            'TM31',
            'TM5',
            'TM6'
        ],
        'velocidad 10' => [
            'TM31',
            'TM5',
            'TM6'
        ],
        'velocidad Turbo' => [
            'TM31',
            'TM5',
            'TM6'
        ],
        'velocidad cuchara' => [
            'TM31',
            'TM5',
            'TM6'
        ],
        'velocidad Espiga' => [
            'TM31',
            'TM5',
            'TM6'
        ],
        'giro a la izquierda' => [
            'TM31',
            'TM5',
            'TM6'
        ]
    ];

    /**
     * @var CommandBus
     */
    private $commandBus;

    /**
     * Create a new command instance.
     *
     * @param CommandBus $commandBus
     */
    public function __construct(CommandBus $commandBus)
    {
        parent::__construct();

        $this->commandBus = $commandBus;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Exception
     */
    public function handle()
    {
        foreach (self::THERMOMIXACTIONS as $thermomixAction => $models) {
            $exists = $this->commandBus->handle(new FindThermomixActionsCommand(['name' => $thermomixAction]));
            if (!$exists) {
                $object = $this->commandBus->handle(new CreateThermomixActionCommand(
                    Uuid::random(),
                    $thermomixAction
                ));

                foreach ($models as $model) {
                    $objModels = $this->commandBus->handle(new FindThermomixModelsCommand(['name' => $model]));

                    if ($objModels) {
                        $object->addModel($objModels[0]);
                    }
                }

                $this->info($thermomixAction . ' created');
            }
        }
    }
}
