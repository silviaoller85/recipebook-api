<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class PersistenceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $driver = config('persistence.config.driver');

        $config = config("persistence.config.repository-gateways");

        foreach ($config as $gatewayInterface => $gatewayConcretions) {
            $this->app->singleton($gatewayInterface, $gatewayConcretions[$driver]);
        }
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
