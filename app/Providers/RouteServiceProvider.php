<?php

namespace App\Providers;

use App\Http\Controllers\Sitemap;
use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        //

        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapApiRoutes();

        $this->mapWebRoutes();

        //
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        Route::middleware('web')
             ->namespace($this->namespace)
             ->group(base_path('routes/web.php'));
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::group([
            'prefix'     => 'api/v1/',
            'middleware' => 'api',
        ], function () {
            Route::prefix('users')->group(base_path('routes/api/users.php'));
            Route::prefix('categories')->group(base_path('routes/api/categories.php'));
            Route::prefix('ingredients')->group(base_path('routes/api/ingredients.php'));
            Route::prefix('units')->group(base_path('routes/api/units.php'));
            Route::prefix('thermomix')->group(base_path('routes/api/thermomix.php'));
        });
    }
}
