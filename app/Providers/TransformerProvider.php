<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use RecipeBook\Application\Common\Transformer\ErrorResponseTransformer;

class TransformerProvider extends ServiceProvider
{
    const BASE_URI = '/api/v1';

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            ErrorResponseTransformer::class,
            function () {
                return new ErrorResponseTransformer(self::BASE_URI);
            }
        );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
