<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use RecipeBook\Application\Common\Controller\XMLResponse;

class XMLResponseProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            XMLResponse::class,
            function () {
                return new XMLResponse(getenv('APP_PUBLIC_URL'));
            }
        );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
