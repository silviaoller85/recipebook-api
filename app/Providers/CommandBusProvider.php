<?php

namespace App\Providers;

use Doctrine\ORM\EntityManager;
use Illuminate\Container\Container;
use Illuminate\Support\ServiceProvider;
use Joselfonseca\LaravelTactician\Locator\LaravelLocator;
use League\Event\Emitter;
use League\Tactician\CommandEvents\EventMiddleware;
use League\Tactician\Doctrine\ORM\TransactionMiddleware;
use League\Tactician\Handler\CommandHandlerMiddleware;
use League\Tactician\Plugins\LockingMiddleware;
use RecipeBook\Application\Common\Bus\CommandBus;

class CommandBusProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $config = config('laravel-tactician');

        $this->app->singleton(
            CommandBus::class,
            function (Container $container) use ($config) {
                /** @var LaravelLocator $locator */
                $locator = $container->make($config['locator']);


                $locator->addHandlers($config['handle-map']);

                return new CommandBus(
                    [
                        new LockingMiddleware,
                        new TransactionMiddleware($container->make(EntityManager::class)),
                        new EventMiddleware($container->make(Emitter::class)),
                        new CommandHandlerMiddleware(
                            $container->make($config['extractor']),
                            $locator,
                            $container->make($config['inflector'])
                        ),
                    ]
                );
            }
        );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
