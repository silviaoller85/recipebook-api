<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use RecipeBook\Application\Common\Transformer\ErrorResponseTransformer;
use RecipeBook\Infrastructure\Storage\Local;
use RecipeBook\Infrastructure\Storage\Storage;

class StorageProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            Storage::class,
            function () {
                return new Local();
            }
        );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
