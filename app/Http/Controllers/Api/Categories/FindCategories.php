<?php

namespace App\Http\Controllers\Api\Categories;

use Illuminate\Http\Request;
use RecipeBook\Application\Common\Bus\CommandBus;
use RecipeBook\Application\Common\Controller\ApiController;
use RecipeBook\Application\Service\Api\Category\Count\CountCategoriesCommand;
use RecipeBook\Application\Service\Api\Category\Find\CategoriesResponse;
use RecipeBook\Application\Service\Api\Category\Find\FindCategoriesCommand;
use RecipeBook\Domain\Model\Category\CategoriesNotFoundException;

class FindCategories extends ApiController
{
    /**
     * @var CategoriesResponse
     */
    private $response;

    /**
     * FindCategories constructor.
     * @param CommandBus $commandBus
     * @param CategoriesResponse $response
     */
    public function __construct(CommandBus $commandBus, CategoriesResponse $response)
    {
        parent::__construct($commandBus);
        $this->response = $response;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke(Request $request)
    {
        try {
            $filters = $request->except('options') ?: [];
            $options = $request->get('options') ?: [];

            $filters['_pagination'] = array_merge([
                'page'           => 1,
                'itemsPerPage' => 999999,
            ], $request->input('_pagination', []));

            return $this->response(
                $this->response->transform(
                    $this->commandBus->handle(new FindCategoriesCommand($filters, $options)),
                    $filters['_pagination']['page'],
                    $filters['_pagination']['itemsPerPage'],
                    $this->commandBus->handle(new CountCategoriesCommand($filters))
                ),
                self::HTTP_OK
            );
        } catch (CategoriesNotFoundException $exception) {
            return $this->errorResponse($exception, self::HTTP_NOT_FOUND);
        } catch (\Throwable $exception) {
            return $this->errorResponse($exception, self::HTTP_ERROR);
        }
    }
}
