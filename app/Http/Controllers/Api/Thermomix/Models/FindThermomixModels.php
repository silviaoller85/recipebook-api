<?php

namespace App\Http\Controllers\Api\Thermomix\Models;

use Illuminate\Http\Request;
use RecipeBook\Application\Common\Bus\CommandBus;
use RecipeBook\Application\Common\Controller\ApiController;
use RecipeBook\Application\Service\Api\Thermomix\Count\ThermomixModel\CountThermomixModelsCommand;
use RecipeBook\Application\Service\Api\Thermomix\Find\ThermomixModel\FindThermomixModelsCommand;
use RecipeBook\Application\Service\Api\Thermomix\Find\ThermomixModel\ThermomixModelsResponse;
use RecipeBook\Domain\Model\Unit\UnitsNotFoundException;

class FindThermomixModels extends ApiController
{
    /**
     * @var ThermomixModelsResponse
     */
    private $response;

    /**
     * FindUnits constructor.
     * @param CommandBus $commandBus
     * @param ThermomixModelsResponse $response
     */
    public function __construct(CommandBus $commandBus, ThermomixModelsResponse $response)
    {
        parent::__construct($commandBus);
        $this->response = $response;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke(Request $request)
    {
        try {
            $filters = $request->except('options') ?: [];
            $options = $request->get('options') ?: [];

            $filters['_pagination'] = array_merge([
                'page'           => 1,
                'itemsPerPage' => 999999,
            ], $request->input('_pagination', []));

            return $this->response(
                $this->response->transform(
                    $this->commandBus->handle(new FindThermomixModelsCommand($filters, $options)),
                    $filters['_pagination']['page'],
                    $filters['_pagination']['itemsPerPage'],
                    $this->commandBus->handle(new CountThermomixModelsCommand($filters))
                ),
                self::HTTP_OK
            );
        } catch (UnitsNotFoundException $exception) {
            return $this->errorResponse($exception, self::HTTP_NOT_FOUND);
        } catch (\Throwable $exception) {
            return $this->errorResponse($exception, self::HTTP_ERROR);
        }
    }
}
