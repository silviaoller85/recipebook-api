<?php

namespace App\Http\Controllers\Api\Units;

use Illuminate\Http\Request;
use RecipeBook\Application\Common\Bus\CommandBus;
use RecipeBook\Application\Common\Controller\ApiController;
use RecipeBook\Application\Service\Api\Unit\Count\CountUnitsCommand;
use RecipeBook\Application\Service\Api\Unit\Find\UnitsResponse;
use RecipeBook\Application\Service\Api\Unit\Find\FindUnitsCommand;
use RecipeBook\Domain\Model\Unit\UnitsNotFoundException;

class FindUnits extends ApiController
{
    /**
     * @var UnitsResponse
     */
    private $response;

    /**
     * FindUnits constructor.
     * @param CommandBus $commandBus
     * @param UnitsResponse $response
     */
    public function __construct(CommandBus $commandBus, UnitsResponse $response)
    {
        parent::__construct($commandBus);
        $this->response = $response;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke(Request $request)
    {
        try {
            $filters = $request->except('options') ?: [];
            $options = $request->get('options') ?: [];

            $filters['_pagination'] = array_merge([
                'page'           => 1,
                'itemsPerPage' => 999999,
            ], $request->input('_pagination', []));

            return $this->response(
                $this->response->transform(
                    $this->commandBus->handle(new FindUnitsCommand($filters, $options)),
                    $filters['_pagination']['page'],
                    $filters['_pagination']['itemsPerPage'],
                    $this->commandBus->handle(new CountUnitsCommand($filters))
                ),
                self::HTTP_OK
            );
        } catch (UnitsNotFoundException $exception) {
            return $this->errorResponse($exception, self::HTTP_NOT_FOUND);
        } catch (\Throwable $exception) {
            return $this->errorResponse($exception, self::HTTP_ERROR);
        }
    }
}
