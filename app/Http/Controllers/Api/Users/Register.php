<?php

namespace App\Http\Controllers\Api\Users;

use Illuminate\Http\Request;
use RecipeBook\Application\Common\Controller\ApiController;
use RecipeBook\Application\Service\Api\User\Register\RegisterCommand;
use RecipeBook\Domain\Common\ValueObject\Uuid;

class Register extends ApiController
{
    public function __invoke(Request $request)
    {
        try {
            $this->validate($request->toArray(), [
                'name' => ['required', 'string', 'max:255'],
                'email' => ['required', 'string', 'email', 'max:255'],
                'password' => ['required', 'string', 'min:8', 'confirmed'],
            ]);

            $user = $this->commandBus->handle(new RegisterCommand(
                Uuid::random(),
                $request->get('name'),
                $request->get('email'),
                $request->get('password')
            ));

            return $this->response($user->toArray(), self::HTTP_OK);
        } catch (\Throwable $exception) {
            return $this->errorResponse($exception, self::HTTP_ERROR);
        }
    }
}
