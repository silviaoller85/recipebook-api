<?php

namespace App\Http\Controllers\Api\Users\UserFavorites;

use Illuminate\Http\Request;
use RecipeBook\Application\Common\Controller\ApiController;
use RecipeBook\Application\Service\Api\Recipe\Get\GetRecipeCommand;
use RecipeBook\Application\Service\Api\User\Delete\UserFavorite\DeleteUserFavoriteCommand;
use RecipeBook\Application\Service\Api\User\Find\UserFavorite\FindUserFavoritesCommand;
use RecipeBook\Domain\Model\User\UserFavoritesNotFoundException;

class RemoveUserFavorite extends ApiController
{
    /**
     * @param Request $request
     * @param string|null $recipeId
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke(Request $request, string $recipeId)
    {
        try {
            $user = $this->user();
            $recipe = $this->commandBus->handle(new GetRecipeCommand($recipeId));

            $userFavorites = $this->commandBus->handle(new FindUserFavoritesCommand([
                'user' => $user,
                'recipe' => $recipe
            ]));

            $this->commandBus->handle(new DeleteUserFavoriteCommand($userFavorites[0]));

            return $this->response([
                'id' => $recipeId
            ], self::HTTP_DELETED);
        } catch (UserFavoritesNotFoundException $exception) {
            return $this->errorResponse($exception, self::HTTP_NOT_FOUND);
        } catch (\Throwable $exception) {
            return $this->errorResponse($exception, self::HTTP_ERROR);
        }
    }
}
