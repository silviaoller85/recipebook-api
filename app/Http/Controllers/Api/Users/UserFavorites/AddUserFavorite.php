<?php

namespace App\Http\Controllers\Api\Users\UserFavorites;

use Illuminate\Http\Request;
use RecipeBook\Application\Common\Controller\ApiController;
use RecipeBook\Application\Service\Api\Recipe\Get\GetRecipeCommand;
use RecipeBook\Application\Service\Api\User\Create\UserFavorite\CreateUserFavoriteCommand;
use RecipeBook\Domain\Common\ValueObject\Uuid;
use RecipeBook\Domain\Model\Recipe\RecipeNotFoundException;

class AddUserFavorite extends ApiController
{
    /**
     * @param Request $request
     * @param string|null $recipeId
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke(Request $request, string $recipeId)
    {
        try {
            $user = $this->user();
            $recipe = $this->commandBus->handle(new GetRecipeCommand($recipeId));

            $favorite = $this->commandBus->handle(new CreateUserFavoriteCommand(
                Uuid::random(),
                $user,
                $recipe
            ));

            return $this->response([
                'id' => $favorite->id()->value()
            ], self::HTTP_CREATED);
        } catch (RecipeNotFoundException $exception) {
            return $this->errorResponse($exception, self::HTTP_NOT_FOUND);
        } catch (\Throwable $exception) {
            return $this->errorResponse($exception, self::HTTP_ERROR);
        }
    }
}
