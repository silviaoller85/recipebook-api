<?php

namespace App\Http\Controllers\Api\Users;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use RecipeBook\Application\Common\Bus\CommandBus;
use RecipeBook\Application\Common\Controller\ApiController;
use RecipeBook\Application\Service\Api\User\Get\UserResponse;
use RecipeBook\Application\Service\Api\User\Get\GetUserCommand;
use RecipeBook\Domain\Model\User\UserNotFoundException;

/**
 * Class GetUser
 * @package App\Http\Controllers\Api\Users
 */
class GetUser extends ApiController
{
    /**
     * @var UserResponse
     */
    private $response;

    /**
     * GetUser constructor.
     * @param CommandBus $commandBus
     * @param UserResponse $response
     */
    public function __construct(CommandBus $commandBus, UserResponse $response)
    {
        $this->response = $response;
        parent::__construct($commandBus);
    }

    /**
     * @param Request $request
     * @param string $id
     * @return JsonResponse
     */
    public function __invoke(Request $request, string $id = null)
    {
        if (is_null($id)) {
            $id = $this->user()->id();
        }

        try {
            $user = $this->commandBus->handle(new GetUserCommand($id));

            return $this->response($this->response->transform($user), self::HTTP_OK);
        } catch (UserNotFoundException $exception) {
            return $this->errorResponse($exception, self::HTTP_NOT_FOUND);
        } catch (\Throwable $exception) {
            return $this->errorResponse($exception, self::HTTP_ERROR);
        }
    }
}
