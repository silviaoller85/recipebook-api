<?php

namespace App\Http\Controllers\Api\Users;

use Illuminate\Http\Request;
use RecipeBook\Application\Common\Bus\CommandBus;
use RecipeBook\Application\Common\Controller\ApiController;
use RecipeBook\Application\Common\Controller\JWTResponse;
use RecipeBook\Application\Service\Api\User\Login\LoginCommand;
use RecipeBook\Application\Service\Api\User\Login\LoginResponse;
use RecipeBook\Domain\Model\User\UserNotFoundException;

class Login extends ApiController
{
    use JWTResponse;

    /**
     * @var LoginResponse
     */
    private $response;

    /**
     * Login constructor.
     * @param CommandBus $commandBus
     * @param LoginResponse $response
     */
    public function __construct(CommandBus $commandBus, LoginResponse $response)
    {
        parent::__construct($commandBus);
        $this->response = $response;
    }

    /**
     * @param Request $request
     * @return Login|\Illuminate\Http\JsonResponse
     */
    public function __invoke(Request $request)
    {
        try {
            $parameters = false;
            $content = $request->getContent();

            if (strlen($content)) {
                $parameters = json_decode($content, true);
            }

            if (!$parameters) {
                $parameters = $request->toArray();
            }

            $this->validate($parameters, [
                'email' => 'email|required',
                'password' => 'required',
            ]);

            $user = $this->commandBus->handle(new LoginCommand(
                $parameters['email'],
                $parameters['password']
            ));

            if (! $token = auth()->attempt(request(['email', 'password']))) {
                return response()->json(['error' => 'Unauthorized'], 401);
            }

            return $this->response($this->response->transform($user, $token), self::HTTP_OK);
        } catch (UserNotFoundException $exception) {
            return $this->errorResponse($exception, self::HTTP_NOT_FOUND);
        } catch (\Throwable $exception) {
            return $this->errorResponse($exception, self::HTTP_ERROR);
        }
    }
}
