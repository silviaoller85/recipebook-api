<?php

namespace App\Http\Controllers\Api\Users\Recipes;

use Illuminate\Http\Request;
use RecipeBook\Application\Common\Controller\ApiController;
use RecipeBook\Application\Service\Api\Category\Get\GetCategoryCommand;
use RecipeBook\Application\Service\Api\Recipe\Create\CreateRecipeCommand;
use RecipeBook\Application\Service\Api\Recipe\Update\Category\UpdateRecipeCategoryCommand;
use RecipeBook\Application\Service\Api\Recipe\Update\ThermomixModel\UpdateRecipeThermomixModelCommand;
use RecipeBook\Application\Service\Api\Thermomix\Get\ThermomixModel\GetThermomixModelCommand;
use RecipeBook\Application\Service\Api\User\Get\GetUserCommand;
use RecipeBook\Domain\Common\ValueObject\Uuid;
use RecipeBook\Domain\Model\Category\CategoryNotFoundException;

class CreateRecipe extends ApiController
{
    /**
     * @param Request $request
     * @param string|null $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke(Request $request, string $id = null)
    {
        if (is_null($id)) {
            $user = $this->user();
        } else {
            $user = $this->commandBus->handle(new GetUserCommand($id));
        }

        try {
            $this->validate($request->toArray(), [
                'name' => 'required',
                'preparationTime' => 'required|numeric',
                'cookingTime' => 'required|numeric',
                'serves' => 'required|integer'
            ]);

            $recipe = $this->commandBus->handle(new CreateRecipeCommand(
                Uuid::random(),
                $request->get('name'),
                $request->get('description'),
                $user,
                $request->get('preparationTime'),
                $request->get('cookingTime'),
                $request->get('serves'),
                filter_var($request->get('isThermomix'), FILTER_VALIDATE_BOOLEAN)
            ));

            $categoriesId = $request->get('categoriesId', []);
            if ($categoriesId && is_array($categoriesId)) {
                $categories = [];
                foreach ($categoriesId as $category) {
                    $categories[] = $this->commandBus->handle(new GetCategoryCommand($category));
                }
                $this->commandBus->handle(new UpdateRecipeCategoryCommand($recipe, $categories));
            }

            $thermomixModelsId = $request->get('thermomixModelsId', []);
            if ($thermomixModelsId && is_array($thermomixModelsId)) {
                $thermomixModels = [];
                foreach ($thermomixModelsId as $thermomixModel) {
                    $thermomixModels[] = $this->commandBus->handle(new GetThermomixModelCommand($thermomixModel));
                }
                $this->commandBus->handle(new UpdateRecipeThermomixModelCommand($recipe, $thermomixModels));
            }

            return $this->response([
                'id' => $recipe->id()->value()
            ], self::HTTP_CREATED);
        } catch (CategoryNotFoundException $exception) {
            return $this->errorResponse($exception, self::HTTP_NOT_FOUND);
        } catch (\Throwable $exception) {
            return $this->errorResponse($exception, self::HTTP_ERROR);
        }
    }
}
