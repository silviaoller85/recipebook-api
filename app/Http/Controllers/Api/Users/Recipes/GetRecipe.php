<?php

namespace App\Http\Controllers\Api\Users\Recipes;

use Illuminate\Http\Request;
use RecipeBook\Application\Common\Bus\CommandBus;
use RecipeBook\Application\Common\Controller\ApiController;
use RecipeBook\Application\Service\Api\Recipe\Get\GetRecipeCommand;
use RecipeBook\Application\Service\Api\Recipe\Get\RecipeResponse;
use RecipeBook\Application\Service\Api\User\Get\GetUserCommand;
use RecipeBook\Domain\Model\Recipe\RecipeNotFoundException;
use RecipeBook\Domain\Model\User\UserNotFoundException;

class GetRecipe extends ApiController
{
    /**
     * @var RecipeResponse
     */
    private $response;

    /**
     * GetUser constructor.
     * @param CommandBus $commandBus
     * @param RecipeResponse $response
     */
    public function __construct(CommandBus $commandBus, RecipeResponse $response)
    {
        $this->response = $response;
        parent::__construct($commandBus);
    }

    /**
     * @param Request $request
     * @param string|null $userId
     * @param string|null $recipeId
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke(Request $request, string $userId = null, string $recipeId = null)
    {
        if (is_null($recipeId)) {
            $recipeId = $userId;
            $user = $this->user();
        } else {
            $user = $this->commandBus->handle(new GetUserCommand($userId));
        }

        try {
            $recipe = $this->commandBus->handle(new GetRecipeCommand($recipeId, $user));

            return $this->response($this->response->transform($recipe), self::HTTP_OK);
        } catch (UserNotFoundException $exception) {
            return $this->errorResponse($exception, self::HTTP_NOT_FOUND);
        } catch (RecipeNotFoundException $exception) {
            return $this->errorResponse($exception, self::HTTP_NOT_FOUND);
        } catch (\Throwable $exception) {
            return $this->errorResponse($exception, self::HTTP_ERROR);
        }
    }
}
