<?php

namespace App\Http\Controllers\Api\Users\Recipes\RecipeSteps;

use Illuminate\Http\Request;
use RecipeBook\Application\Common\Controller\ApiController;
use RecipeBook\Application\Service\Api\Recipe\Create\RecipeStep\CreateRecipeStepCommand;
use RecipeBook\Application\Service\Api\Recipe\Get\GetRecipeCommand;
use RecipeBook\Application\Service\Api\User\Get\GetUserCommand;
use RecipeBook\Domain\Common\ValueObject\Uuid;
use RecipeBook\Domain\Model\Recipe\RecipeNotFoundException;

class CreateRecipeStep extends ApiController
{
    /**
     * @param Request $request
     * @param string $userId
     * @param string|null $recipeId
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke(Request $request, string $userId, string $recipeId = null)
    {
        if (is_null($recipeId)) {
            $recipeId = $userId;
            $user = $this->user();
        } else {
            $user = $this->commandBus->handle(new GetUserCommand($userId));
        }

        try {
            $this->validate($request->toArray(), [
                'description' => 'required',
            ]);

            $recipe = $this->commandBus->handle(new GetRecipeCommand($recipeId, $user));

            $recipeStep = $this->commandBus->handle(new CreateRecipeStepCommand(
                Uuid::random(),
                $recipe,
                $request->get('number'),
                $request->get('name'),
                $request->get('description')
            ));

            return $this->response([
                'id' => $recipeStep->id()->value()
            ], self::HTTP_CREATED);
        } catch (RecipeNotFoundException $exception) {
            return $this->errorResponse($exception, self::HTTP_NOT_FOUND);
        } catch (\Throwable $exception) {
            return $this->errorResponse($exception, self::HTTP_ERROR);
        }
    }
}
