<?php

namespace App\Http\Controllers\Api\Users\Recipes\RecipeSteps;

use Illuminate\Http\Request;
use RecipeBook\Application\Common\Controller\ApiController;
use RecipeBook\Application\Service\Api\Recipe\Update\RecipeStep\UpdateRecipeStepCommand;
use RecipeBook\Application\Service\Api\Recipe\Get\GetRecipeCommand;
use RecipeBook\Application\Service\Api\Recipe\Get\RecipeStep\GetRecipeStepCommand;
use RecipeBook\Application\Service\Api\User\Get\GetUserCommand;
use RecipeBook\Domain\Model\Recipe\RecipeStepNotFoundException;

class UpdateRecipeStep extends ApiController
{
    /**
     * @param Request $request
     * @param string $userId
     * @param string $recipeId
     * @param string|null $recipeStepId
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke(Request $request, string $userId, string $recipeId, string $recipeStepId = null)
    {
        if (is_null($recipeStepId)) {
            $recipeStepId = $recipeId;
            $recipeId = $userId;
            $user = $this->user();
        } else {
            $user = $this->commandBus->handle(new GetUserCommand($userId));
        }

        try {
            $this->validate($request->toArray(), [
                'description' => 'required',
            ]);

            $recipe = $this->commandBus->handle(new GetRecipeCommand($recipeId, $user));
            $recipeStep = $this->commandBus->handle(new GetRecipeStepCommand($recipeStepId, $recipe));

            $this->commandBus->handle(new UpdateRecipeStepCommand(
                $recipeStepId,
                $request->get('number'),
                $request->get('name'),
                $request->get('description'),
                $recipe
            ));

            return $this->response([
                'id' => $recipeStepId
            ], self::HTTP_UPDATED);
        } catch (RecipeStepNotFoundException $exception) {
            return $this->errorResponse($exception, self::HTTP_NOT_FOUND);
        } catch (\Throwable $exception) {
            return $this->errorResponse($exception, self::HTTP_ERROR);
        }
    }
}
