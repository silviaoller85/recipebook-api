<?php

namespace App\Http\Controllers\Api\Users\Recipes\RecipeSteps;

use Illuminate\Http\Request;
use RecipeBook\Application\Common\Controller\ApiController;
use RecipeBook\Application\Service\Api\Recipe\Get\GetRecipeCommand;
use RecipeBook\Application\Service\Api\Recipe\Get\RecipeStep\GetRecipeStepCommand;
use RecipeBook\Application\Service\Api\Recipe\Update\RecipeStep\UpdateRecipeStepCommand;
use RecipeBook\Application\Service\Api\User\Get\GetUserCommand;
use RecipeBook\Domain\Model\Recipe\RecipeNotFoundException;
use RecipeBook\Domain\Model\Recipe\RecipeStepNotFoundException;

class RefreshRecipeSteps extends ApiController
{
    /**
     * @param Request $request
     * @param string $userId
     * @param string|null $recipeId
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke(Request $request, string $userId, string $recipeId = null)
    {
        if (is_null($recipeId)) {
            $recipeId = $userId;
            $user = $this->user();
        } else {
            $user = $this->commandBus->handle(new GetUserCommand($userId));
        }

        try {
            $recipe = $this->commandBus->handle(new GetRecipeCommand($recipeId, $user));
            foreach ($request->get('steps') as $number => $step) {
                $step = $this->commandBus->handle(new GetRecipeStepCommand($step['id'], $recipe));
                $this->commandBus->handle(new UpdateRecipeStepCommand(
                    $step->id()->value(),
                    $number + 1,
                    $step->name(),
                    $step->description(),
                    $recipe
                ));
            }

            return $this->response([
                'id' => $recipeId
            ], self::HTTP_UPDATED);
        } catch (RecipeNotFoundException $exception) {
            return $this->errorResponse($exception, self::HTTP_NOT_FOUND);
        } catch (RecipeStepNotFoundException $exception) {
            return $this->errorResponse($exception, self::HTTP_NOT_FOUND);
        } catch (\Throwable $exception) {
            return $this->errorResponse($exception, self::HTTP_ERROR);
        }
    }
}
