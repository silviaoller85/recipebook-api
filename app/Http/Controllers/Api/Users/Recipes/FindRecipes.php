<?php

namespace App\Http\Controllers\Api\Users\Recipes;

use Illuminate\Http\Request;
use RecipeBook\Application\Common\Bus\CommandBus;
use RecipeBook\Application\Common\Controller\ApiController;
use RecipeBook\Application\Service\Api\Recipe\Count\CountRecipesCommand;
use RecipeBook\Application\Service\Api\Recipe\Find\FindRecipesCommand;
use RecipeBook\Application\Service\Api\Recipe\Find\RecipesResponse;
use RecipeBook\Domain\Model\Recipe\RecipesNotFoundException;
use RecipeBook\Domain\Model\User\UserNotFoundException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;

class FindRecipes extends ApiController
{
    /**
     * @var RecipesResponse
     */
    private $response;

    /**
     * GetUser constructor.
     * @param CommandBus $commandBus
     * @param RecipesResponse $response
     */
    public function __construct(CommandBus $commandBus, RecipesResponse $response)
    {
        $this->response = $response;
        parent::__construct($commandBus);
    }

    /**
     * @param Request $request
     * @param string|null $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke(Request $request, string $id = null)
    {
        try {
            if (!$request->get('searching') && !$this->user() && !$id) {
                return response()->json(['error' => 'TOKEN_EXPIRED'], 500);
            }

            if (is_null($id) && $this->user()) {
                $id = $this->user()->id();
            }

            $filters = $request->except('options') ?: [];
            $options = $request->get('options') ?: [];

            if (!$request->get('searching')) {
                $filters['userId'] = $id;
            }

            $filters['_pagination'] = array_merge([
                'page'          => 1,
                'itemsPerPage'  => 3,
            ], $request->input('_pagination', []));

            return $this->response(
                $this->response->transform(
                    $this->commandBus->handle(new FindRecipesCommand($filters, $options)),
                    $filters['_pagination']['page'],
                    $filters['_pagination']['itemsPerPage'],
                    $this->commandBus->handle(new CountRecipesCommand($filters))
                ),
                self::HTTP_OK
            );
        } catch (UserNotFoundException $exception) {
            return $this->errorResponse($exception, self::HTTP_NOT_FOUND);
        } catch (RecipesNotFoundException $exception) {
            return $this->errorResponse($exception, self::HTTP_NOT_FOUND);
        } catch (\Throwable $exception) {
            return $this->errorResponse($exception, self::HTTP_ERROR);
        }
    }
}
