<?php

namespace App\Http\Controllers\Api\Users\Recipes\RecipePhotos;

use Illuminate\Http\Request;
use RecipeBook\Application\Common\Controller\ApiController;
use RecipeBook\Application\Service\Api\Recipe\Create\RecipePhoto\CreateRecipePhotoCommand;
use RecipeBook\Application\Service\Api\Recipe\Delete\RecipePhoto\DeleteRecipePhotoCommand;
use RecipeBook\Application\Service\Api\Recipe\Get\GetRecipeCommand;
use RecipeBook\Domain\Common\ValueObject\Uuid;
use RecipeBook\Domain\Model\File;
use RecipeBook\Domain\Model\Recipe\RecipeNotFoundException;

class AddRecipePhoto extends ApiController
{
    /**
     * @param Request $request
     * @param string|null $recipeId
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke(Request $request, string $recipeId)
    {
        try {
            $user = $this->user();
            $recipe = $this->commandBus->handle(new GetRecipeCommand($recipeId));

            foreach ($recipe->photos() as $photo) {
                $this->commandBus->handle(new DeleteRecipePhotoCommand($photo));
            }

            $photo = $request->file('photo');
            $photo = File::createFromArray([
                'path' => $photo->getRealPath(),
                'filename' => $photo->getClientOriginalName(),
                'extension' => $photo->clientExtension(),
                'size' => $photo->getSize(),
                'mime' => $photo->getMimeType()
            ]);

            $favorite = $this->commandBus->handle(new CreateRecipePhotoCommand(
                Uuid::random(),
                $user,
                $recipe,
                null,
                $photo
            ));

            return $this->response([
                'id' => $favorite->id()->value()
            ], self::HTTP_CREATED);
        } catch (RecipeNotFoundException $exception) {
            return $this->errorResponse($exception, self::HTTP_NOT_FOUND);
        } catch (\Throwable $exception) {
            return $this->errorResponse($exception, self::HTTP_ERROR);
        }
    }
}
