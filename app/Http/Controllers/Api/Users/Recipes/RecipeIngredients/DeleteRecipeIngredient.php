<?php

namespace App\Http\Controllers\Api\Users\Recipes\RecipeIngredients;

use Illuminate\Http\Request;
use RecipeBook\Application\Common\Controller\ApiController;
use RecipeBook\Application\Service\Api\Recipe\Delete\RecipeIngredient\DeleteRecipeIngredientCommand;
use RecipeBook\Application\Service\Api\Recipe\Get\GetRecipeCommand;
use RecipeBook\Application\Service\Api\Recipe\Get\RecipeIngredient\GetRecipeIngredientCommand;
use RecipeBook\Application\Service\Api\User\Get\GetUserCommand;
use RecipeBook\Domain\Model\Recipe\RecipeIngredientNotFoundException;

class DeleteRecipeIngredient extends ApiController
{
    /**
     * @param Request $request
     * @param string $userId
     * @param string $recipeId
     * @param string|null $recipeIngredientId
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke(Request $request, string $userId, string $recipeId, string $recipeIngredientId = null)
    {
        if (is_null($recipeIngredientId)) {
            $recipeIngredientId = $recipeId;
            $recipeId = $userId;
            $user = $this->user();
        } else {
            $user = $this->commandBus->handle(new GetUserCommand($userId));
        }

        try {
            $recipe = $this->commandBus->handle(new GetRecipeCommand($recipeId, $user));
            $recipeIngredient = $this->commandBus->handle(new GetRecipeIngredientCommand($recipeIngredientId, $recipe));

            $this->commandBus->handle(new DeleteRecipeIngredientCommand($recipeIngredient));

            return $this->response([
                'id' => $recipeIngredientId
            ], self::HTTP_DELETED);
        } catch (RecipeIngredientNotFoundException $exception) {
            return $this->errorResponse($exception, self::HTTP_NOT_FOUND);
        } catch (\Throwable $exception) {
            return $this->errorResponse($exception, self::HTTP_ERROR);
        }
    }
}
