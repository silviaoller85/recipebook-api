<?php

namespace App\Http\Controllers\Api\Users\Recipes\RecipeIngredients;

use Illuminate\Http\Request;
use RecipeBook\Application\Common\Controller\ApiController;
use RecipeBook\Application\Service\Api\Ingredient\Get\GetIngredientCommand;
use RecipeBook\Application\Service\Api\Recipe\Get\GetRecipeCommand;
use RecipeBook\Application\Service\Api\Recipe\Update\RecipeIngredient\UpdateRecipeIngredientCommand;
use RecipeBook\Application\Service\Api\Unit\Get\GetUnitCommand;
use RecipeBook\Application\Service\Api\User\Get\GetUserCommand;
use RecipeBook\Domain\Model\Ingredient\IngredientNotFoundException;
use RecipeBook\Domain\Model\Recipe\RecipeNotFoundException;
use RecipeBook\Domain\Model\Unit\UnitNotFoundException;

class UpdateRecipeIngredient extends ApiController
{
    /**
     * @param Request $request
     * @param string $userId
     * @param string|null $recipeId
     * @param string|null $recipeIngredientId
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke(Request $request, string $userId, string $recipeId, string $recipeIngredientId = null)
    {
        if (is_null($recipeIngredientId)) {
            $recipeIngredientId = $recipeId;
            $recipeId = $userId;
            $user = $this->user();
        } else {
            $user = $this->commandBus->handle(new GetUserCommand($userId));
        }

        try {
            $this->validate($request->toArray(), [
                'ingredientId' => 'required',
                'value' => 'required',
                'unitId' => 'required',
            ]);

            $recipe = $this->commandBus->handle(new GetRecipeCommand($recipeId, $user));
            $ingredient = $this->commandBus->handle(new GetIngredientCommand($request->get('ingredientId')));
            $unit = $this->commandBus->handle(new GetUnitCommand($request->get('unitId')));

            $recipeIngredient = $this->commandBus->handle(new UpdateRecipeIngredientCommand(
                $recipeIngredientId,
                $recipe,
                $ingredient,
                $request->get('value'),
                $unit
            ));

            return $this->response([
                'id' => $recipeIngredient->id()->value()
            ], self::HTTP_CREATED);
        } catch (RecipeNotFoundException $exception) {
            return $this->errorResponse($exception, self::HTTP_NOT_FOUND);
        } catch (IngredientNotFoundException $exception) {
            return $this->errorResponse($exception, self::HTTP_NOT_FOUND);
        } catch (UnitNotFoundException $exception) {
            return $this->errorResponse($exception, self::HTTP_NOT_FOUND);
        } catch (\Throwable $exception) {
            return $this->errorResponse($exception, self::HTTP_ERROR);
        }
    }
}
