<?php

namespace App\Http\Controllers\Api\Users\Recipes;

use Illuminate\Http\Request;
use RecipeBook\Application\Common\Controller\ApiController;
use RecipeBook\Application\Service\Api\Recipe\Delete\DeleteRecipeCommand;
use RecipeBook\Application\Service\Api\Recipe\Get\GetRecipeCommand;
use RecipeBook\Application\Service\Api\User\Get\GetUserCommand;
use RecipeBook\Domain\Model\Recipe\RecipeNotFoundException;

class DeleteRecipe extends ApiController
{
    /**
     * @param Request $request
     * @param string|null $userId
     * @param string|null $recipeId
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke(Request $request, string $userId = null, string $recipeId = null)
    {
        if (is_null($recipeId)) {
            $recipeId = $userId;
            $user = $this->user();
        } else {
            $user = $this->commandBus->handle(new GetUserCommand($userId));
        }

        try {
            $recipe = $this->commandBus->handle(new GetRecipeCommand($recipeId, $user));

            $this->commandBus->handle(new DeleteRecipeCommand($recipe));

            return $this->response([
                'id' => $recipeId
            ], self::HTTP_DELETED);
        } catch (RecipeNotFoundException $exception) {
            return $this->errorResponse($exception, self::HTTP_NOT_FOUND);
        } catch (\Throwable $exception) {
            return $this->errorResponse($exception, self::HTTP_ERROR);
        }
    }
}
