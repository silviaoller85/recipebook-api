<?php

namespace App\Http\Controllers\Api\Users\Recipes;

use Illuminate\Http\Request;
use RecipeBook\Application\Common\Bus\CommandBus;
use RecipeBook\Application\Common\Controller\ApiController;
use RecipeBook\Application\Service\Api\Recipe\Find\FindRecipesCommand;
use RecipeBook\Application\Service\Api\Recipe\Get\RecipeResponse;
use RecipeBook\Domain\Model\Recipe\RecipesNotFoundException;

class ViewRecipe extends ApiController
{
    /**
     * @var RecipeResponse
     */
    private $response;

    /**
     * GetUser constructor.
     * @param CommandBus $commandBus
     * @param RecipeResponse $response
     */
    public function __construct(CommandBus $commandBus, RecipeResponse $response)
    {
        $this->response = $response;
        parent::__construct($commandBus);
    }

    /**
     * @param Request $request
     * @param string|null $userId
     * @param string|null $recipeId
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke(Request $request, string $recipe = null)
    {
        try {
            $recipes = $this->commandBus->handle(new FindRecipesCommand([
                'slug' => $recipe
            ]));

            $recipe = $recipes[0];

            return $this->response($this->response->transform($recipe), self::HTTP_OK);
        } catch (RecipesNotFoundException $exception) {
            return $this->errorResponse($exception, self::HTTP_NOT_FOUND);
        } catch (\Throwable $exception) {
            return $this->errorResponse($exception, self::HTTP_ERROR);
        }
    }
}
