<?php

namespace App\Http\Controllers\Api\Users;

use Illuminate\Http\Request;
use RecipeBook\Application\Common\Bus\CommandBus;
use RecipeBook\Application\Common\Controller\ApiController;
use RecipeBook\Application\Service\Api\User\Count\CountUsersCommand;
use RecipeBook\Application\Service\Api\User\Find\UsersResponse;
use RecipeBook\Application\Service\Api\User\Find\FindUsersCommand;
use RecipeBook\Domain\Model\User\UsersNotFoundException;

class FindUsers extends ApiController
{
    /**
     * @var UsersResponse
     */
    private $response;

    /**
     * FindUsers constructor.
     * @param CommandBus $commandBus
     * @param UsersResponse $response
     */
    public function __construct(CommandBus $commandBus, UsersResponse $response)
    {
        parent::__construct($commandBus);
        $this->response = $response;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke(Request $request)
    {
        try {
            $filters = $request->except('options') ?: [];
            $options = $request->get('options') ?: [];

            $filters['_pagination'] = array_merge([
                'page'           => 1,
                'itemsPerPage' => 999999,
            ], $request->input('_pagination', []));

            return $this->response(
                $this->response->transform(
                    $this->commandBus->handle(new FindUsersCommand($filters, $options)),
                    $filters['_pagination']['page'],
                    $filters['_pagination']['itemsPerPage'],
                    $this->commandBus->handle(new CountUsersCommand($filters))
                ),
                self::HTTP_OK
            );
        } catch (UsersNotFoundException $exception) {
            return $this->errorResponse($exception, self::HTTP_NOT_FOUND);
        } catch (\Throwable $exception) {
            return $this->errorResponse($exception, self::HTTP_ERROR);
        }
    }
}
