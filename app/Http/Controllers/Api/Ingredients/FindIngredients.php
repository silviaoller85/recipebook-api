<?php

namespace App\Http\Controllers\Api\Ingredients;

use Illuminate\Http\Request;
use RecipeBook\Application\Common\Bus\CommandBus;
use RecipeBook\Application\Common\Controller\ApiController;
use RecipeBook\Application\Service\Api\Ingredient\Count\CountIngredientsCommand;
use RecipeBook\Application\Service\Api\Ingredient\Find\IngredientsResponse;
use RecipeBook\Application\Service\Api\Ingredient\Find\FindIngredientsCommand;
use RecipeBook\Domain\Model\Ingredient\IngredientsNotFoundException;

class FindIngredients extends ApiController
{
    /**
     * @var IngredientsResponse
     */
    private $response;

    /**
     * FindIngredients constructor.
     * @param CommandBus $commandBus
     * @param IngredientsResponse $response
     */
    public function __construct(CommandBus $commandBus, IngredientsResponse $response)
    {
        parent::__construct($commandBus);
        $this->response = $response;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke(Request $request)
    {
        try {
            $filters = $request->except('options') ?: [];
            $options = $request->get('options') ?: [];

            $filters['_pagination'] = array_merge([
                'page'           => 1,
                'itemsPerPage' => 999999,
            ], $request->input('_pagination', []));

            return $this->response(
                $this->response->transform(
                    $this->commandBus->handle(new FindIngredientsCommand($filters, $options)),
                    $filters['_pagination']['page'],
                    $filters['_pagination']['itemsPerPage'],
                    $this->commandBus->handle(new CountIngredientsCommand($filters))
                ),
                self::HTTP_OK
            );
        } catch (IngredientsNotFoundException $exception) {
            return $this->errorResponse($exception, self::HTTP_NOT_FOUND);
        } catch (\Throwable $exception) {
            return $this->errorResponse($exception, self::HTTP_ERROR);
        }
    }
}
