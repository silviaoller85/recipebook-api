<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use RecipeBook\Application\Common\Bus\CommandBus;
use RecipeBook\Application\Common\Controller\ApiController;
use RecipeBook\Application\Common\Controller\XMLResponse;
use RecipeBook\Application\Service\Api\Recipe\Find\FindRecipesCommand;
use RecipeBook\Application\Service\Api\User\Find\FindUsersCommand;

class Sitemap extends ApiController
{
    /**
     * @var XMLResponse
     */
    private $xmlResponse;

    /**
     * Sitemap constructor.
     * @param CommandBus $commandBus
     * @param XMLResponse $xmlResponse
     */
    public function __construct(CommandBus $commandBus, XMLResponse $xmlResponse)
    {
        $this->xmlResponse = $xmlResponse;
        parent::__construct($commandBus);
    }

    public function __invoke(Request $request)
    {
        $filters = [
            'searching' => true,
            '_pagination' => [
                'page'          => 1,
                'itemsPerPage'  => 9999999,
            ]
        ];
        $options = [
            'orderBy' => [
                'field' => 'name',
                'order' => 'ASC'
            ]
        ];

        $recipes = $this->commandBus->handle(new FindRecipesCommand($filters, $options));
        $users = $this->commandBus->handle(new FindUsersCommand($filters, $options));

        return $this->xmlResponse->response($recipes, $users);
    }
}
