<?php

namespace App\Http\Middleware;

use Closure;

class ParametersToArray
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $inputs = [];
        foreach ($request->all() as $key => $input) {
            if (is_string($input) && is_array(json_decode($input, true))) {
                $inputs[$key] = json_decode($input, true);
            } else {
                $inputs[$key] = $input;
            }
        }
        $request->replace($inputs);

        return $next($request);
    }
}
