<?php

Route::group([
    'middleware' => ['api', 'jwt.verify'],
], function ($router) {
    Route::name('ingredients.find')->get('/', \App\Http\Controllers\Api\Ingredients\FindIngredients::class);
});
