<?php

Route::group([
    'middleware' => ['api', 'jwt.verify'],
    'prefix' => 'models',
], function ($router) {
    Route::name('thermomix.models.find')->get('/', \App\Http\Controllers\Api\Thermomix\Models\FindThermomixModels::class);
});
