<?php

Route::group([
    'middleware' => ['api'],
], function ($router) {
    Route::name('categories.find')->get('/', \App\Http\Controllers\Api\Categories\FindCategories::class);
});
