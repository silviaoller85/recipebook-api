<?php

Route::name('users.register')->post('register', \App\Http\Controllers\Api\Users\Register::class);
Route::name('users.login')->post('login', \App\Http\Controllers\Api\Users\Login::class);


Route::group([
    'middleware' => ['api'],
    'prefix' => 'profile'
], function ($router) {
    Route::name('users.me')->get('me/{id?}', \App\Http\Controllers\Api\Users\GetUser::class);

    Route::group(['prefix' => 'recipes'], function(){
        Route::name('users.recipes.view')->get('view/{slug}', \App\Http\Controllers\Api\Users\Recipes\ViewRecipe::class);
        Route::name('users.recipes.find')->get('{id?}', \App\Http\Controllers\Api\Users\Recipes\FindRecipes::class);

        Route::group([
            'middleware' => ['jwt.verify']
        ], function ($router)
        {
            Route::name('users.recipes.get')->get('get/{id}', \App\Http\Controllers\Api\Users\Recipes\GetRecipe::class);
            Route::name('users.recipes.create')->post('create', \App\Http\Controllers\Api\Users\Recipes\CreateRecipe::class);
            Route::name('users.recipes.update')->post('update/{id}', \App\Http\Controllers\Api\Users\Recipes\UpdateRecipe::class);
            Route::name('users.recipes.delete')->post('delete/{id}', \App\Http\Controllers\Api\Users\Recipes\DeleteRecipe::class);
            Route::name('users.recipes.upload')->post('upload/{id}', \App\Http\Controllers\Api\Users\Recipes\RecipePhotos\AddRecipePhoto::class);

            Route::group(['prefix' => '{id}/ingredients'], function() {
                Route::name('users.recipes.ingredients.create')->post('create', \App\Http\Controllers\Api\Users\Recipes\RecipeIngredients\CreateRecipeIngredient::class);
                Route::name('users.recipes.ingredients.update')->post('update/{ingredientId}', \App\Http\Controllers\Api\Users\Recipes\RecipeIngredients\UpdateRecipeIngredient::class);
                Route::name('users.recipes.ingredients.delete')->post('/delete/{ingredientId}', \App\Http\Controllers\Api\Users\Recipes\RecipeIngredients\DeleteRecipeIngredient::class);
            });

            Route::group(['prefix' => '{id}/steps'], function() {
                Route::name('users.recipes.steps.create')->post('create', \App\Http\Controllers\Api\Users\Recipes\RecipeSteps\CreateRecipeStep::class);
                Route::name('users.recipes.steps.delete')->post('delete/{stepId}', \App\Http\Controllers\Api\Users\Recipes\RecipeSteps\DeleteRecipeStep::class);
                Route::name('users.recipes.steps.update')->post('update/{stepId}', \App\Http\Controllers\Api\Users\Recipes\RecipeSteps\UpdateRecipeStep::class);
                Route::name('users.recipes.steps.refresh')->post('refresh', \App\Http\Controllers\Api\Users\Recipes\RecipeSteps\RefreshRecipeSteps::class);
            });
        });
    });


    Route::group([
        'middleware' => ['jwt.verify']
    ], function ($router) {
        Route::group(['prefix' => 'favorites'], function () {
            Route::name('users.favorites.add')->post('add/{id}', \App\Http\Controllers\Api\Users\UserFavorites\AddUserFavorite::class);
            Route::name('users.favorites.remove')->post('remove/{id}', \App\Http\Controllers\Api\Users\UserFavorites\RemoveUserFavorite::class);
        });
    });
});

Route::group([
    'middleware' => ['api'],
], function($router) {
    Route::get('/', \App\Http\Controllers\Api\Users\FindUsers::class);
});
