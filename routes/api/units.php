<?php

Route::group([
    'middleware' => ['api', 'jwt.verify'],
], function ($router) {
    Route::name('units.find')->get('/', \App\Http\Controllers\Api\Units\FindUnits::class);
});
